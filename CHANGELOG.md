<a name="1.1.2"></a>
# [1.1.2](https://github.com/mindslab-ai/brain-stt/compare/v1.1.1...v1.1.2) (2017-12-22)

### Bug Fixes

* **build**: libmaum 없는 경우에 자동 빌드되도록 처리
* **build**: 빌드타입 지정, RELEASE, DEBUG

### Enhancements

* **proto**: tts 관련 정의 m2u로 다시 옮김

### Features

None

### Breaking Changes

None

### Remark

* **sttd:** s3evaluation, stt-trainer repo 분리

<a name="1.1.1"></a>
## [1.1.1](https://github.com/mindslab-ai/brain-stt/compare/v1.1.0...v1,1,1) (2017-12-08)

### Bug Fixes

* **sttd:** 결과 시간 보정
* **sttd:** 멀티 스레드에서 랜덤하게 result 나오지 않는 현상 수정
* **sttd:** child process 여러 번 실행되는 현상 수정
* **sttd:** 영어 마지막 결과 리턴 안되는 현상 수정

### Enhancements

* **sttd:** CPU, GPU 사용 옵션 추가
* **sttd:** external ip setting추가
* **sample:** 벤치마크 프로그램 개발
* **sample:** python program version 출력 추가

### Breaking Changes

None

### Remark

* **sttd:** s3evaluation, stt-trainer repo 분리

<a name="1.0"></a>
# [1.1.0](https://github.com/mindslab-ai/brain-stt/compare/760dde5...v1.1.0) (2017-11-05)
<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

### Bug Fixes

* **sttd:** GPU 메모리 릭 수정, 멀티 GPU지원

### Features

* **proto:** protoBuf
* **config:** 설정 파일
* **sttd:** stt 실행 서버
* **s3evaluation:** stt 평가 서버
* **stt-trainer:** stt 학습 서버

### Enhancements

* **StreamRecognize:** 동작 방식을 ETRI EPD옵션 적용한 방식으로 변경
* **sample:** sample grpc client 추가

### Breaking Changes

None

### Remark

None
