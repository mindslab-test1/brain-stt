# stt-proxy

## 개요

  stt-proxy 프로세스는 기존 brain-stt와 cnn-stt의 grpc proto파일이
  다르므로 brain-stt와 연동하던 기존 프로그램이 brain-stt 인터페이스를
  수정하지 않고 cnn-stt로 연동할 수 있도록 메시지를 중개해 주는
  프로세스입니다.

  또한 여러개의 STT 서버 주소 및 모델 정보를 database에 저장하고
  있으므로 stt client는 stt proxy의 주소만 알고 있다면 별도로 real
  server의 주소를 알고 있을 필요가 없는 장점이 있습니다.

  table에는 stt client가 보내는 모델명 (table에 ID컬럼)에 맞는 실제
  stt정보가 매핑되어 있습니다.


## BUILD

  $ go build


## 사용법
```
  Usage of stt-proxy:
    -cfg string
         Proxy config file (default "stt-proxy.cfg")
    -port int
         The server port (default 9801)
```


## 테이블 샘플
```
  ID                              PROTO       ADDR                MODEL         LANG        SAMPLERATE  DESC            
  ------------------------------  ----------  ------------------  ------------  ----------  ----------  ----------------
  02                              0           10.122.64.152:9802  cnn-kor-8000                          CNN STT baseline
  05                              0           10.122.64.152:9801  voicebot_poc  kor         8000        ETRI DNN 모델 
  09                              0           10.122.64.152:9801  hcard         kor         8000        ETRI DNN 모델 
  13                              0           10.122.64.152:9801  HW_1113-kor-  kor         8000        ETRI DNN        
  14                              2           172.17.0.2:15003    cnn9806                               CNN baseline    
  17                              2           10.122.64.152:9802  cnn152                                CNN STT baseline
  base_unilstm                    0           10.122.64.152:9801  base_unilstm  kor         8000        ETRI DNN 모델 
  car                             0           10.122.64.152:9801  car           kor         8000        ETRI DNN 모델 
  default                         0           10.122.64.152:9801  voicebot      kor         8000        ETRI DNN 모델 
  maum_intro_v2                   0           10.122.64.152:9801  maum_intro_v  kor         8000        ETRI DNN 모델 
  ulstm_eng                       0           182.162.19.10:9801  base_unilstm  eng         8000        English baseline
  voicebot                        0           10.122.64.152:9801  voicebot      kor         8000        ETRI DNN 모델
```


## 메타 데이터 세팅

  stt client는 기존에 model, samplerate, lang 값을 설정해서 보냈지만
  stt-proxy를 이용하는 경우에 기존 model값에 위 테이블의 ID에 해당하는
  값만 넘겨주시면 됩니다.

## 2021-10-19 IBK 음성봇 설치 관련 기록
---
### go build 문제 발생
아래와 같이 go build할 경우 아래와 같은 문제 발생 \
```
cmd/go: "fatal: git fetch-pack: expected shallow list" when retrieving earlier version of already installed package on CentOS 7"
```
구글 서치 결과 CentOS 7에 기본 설치된 git version 문제로 판단. [링크 참고](https://github.com/golang/go/issues/38373)

+ git 삭제 후 최신버전 설치
```
sudo yum -y erase git
sudo yum -y install https://repo.ius.io/ius-release-el7.rpm
sudo yum -y install git222
```
+ **(주의)** git 삭제 시 dependency가 있는 golang도 같이 삭제됨
+ golang 설치
```
sudo yum install golang
```
+ 설치 후 버전 확인
```
[minds@localhost ~]$ git version
git version 2.22.5
[minds@localhost ~]$ go version
go version go1.15.14 linux/amd64
```
+ 설치 후 go build 실행하면 아래와 같은 에러 발생
```
17:34 $ go build
go: github.com/mattn/go-sqlite3@v2.0.3+incompatible: reading github.com/mattn/go-sqlite3/go.mod at revision v2.0.3: unknown revision v2.0.3
```
+ go.mod의 패키지가 이전 버전의 go와 연관되어 맞지 않음. go mod init으로 go.mod파일을 새로 생성
```
17:37 $ mv go.mod go.mod.0
✔ ~/git/brain-stt/src/stt-proxy [canary|✚ 2…2] 
17:38 $ go mod init mindslab/stt-proxy
go: creating new go.mod: module mindslab/stt-proxy
```
+ 생성된 go.mod파일 마지막에 아래를 추가
```
replace maum => ./maum
```
+ go build 실행

