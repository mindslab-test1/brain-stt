package main

import (
	"database/sql"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"sort"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/viper"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	pb2 "maum/brain/stt"
	pb3 "maum/brain/w2l"
)

type sttServerInfo struct {
	// DNN:0, LSTM:1, CNN:2
	Proto      int
	Addr       string
	Model      string
	Lang       string
	Samplerate string
}

type myServer struct {
	pb2.SpeechToTextServiceServer
}

type mockRealServer struct {
	pb2.SttRealServiceServer
}

type modelServer struct {
	pb2.SttModelResolverServer
}

var (
	cfgFile            = flag.String("cfg", "stt-proxy.cfg", "Proxy config file")
	ip                 = flag.String("ip", "127.0.0.1", "The server ip")
	port               = flag.Int("port", 9801, "The server port")
	dbConnectionString string
	dbType             string
	sttMap             = make(map[string]*sttServerInfo)
	mapMutex           = &sync.Mutex{}
)

func (s *modelServer) Find(ctx context.Context, in *pb2.Model) (*pb2.ServerStatus, error) {
	var ss pb2.ServerStatus
	mapMutex.Lock()
	if info, ok := sttMap[in.Model]; ok {
		log.Printf("[DEBUG] Find (addr: %s, model: %s, lang: %s, samplerate: %s)", info.Addr, info.Model, info.Lang, info.Samplerate)
		mapMutex.Unlock()
		// DNN or LSTM STT
		if info.Proto != 2 {
			conn, err := grpc.Dial(info.Addr, grpc.WithInsecure())
			if err != nil {
				log.Printf("[ERROR] Connection Failed (addr: %s, model: %s, lang: %s, samplerate: %s)", info.Addr, info.Model, info.Lang, info.Samplerate)
				return nil, status.Errorf(codes.Unavailable, "Connection failed")
			}
			defer conn.Close()
			c := pb2.NewSttModelResolverClient(conn)
			return c.Find(context.Background(), in)
		} else {
			// CNN STT
			ss.Running = true
			ss.ServerAddress = fmt.Sprintf("%s:%d", *ip, *port)
			return &ss, nil
		}
	} else {
		log.Printf("[ERROR] Find() - No such model: %s", in.Model)
		ss.Running = false
		mapMutex.Unlock()
		return &ss, status.Errorf(codes.NotFound, "No such model")
	}
}

func (s *modelServer) Stop(ctx context.Context, in *pb2.Model) (*pb2.ServerStatus, error) {
	var ss pb2.ServerStatus
	mapMutex.Lock()
	if info, ok := sttMap[in.Model]; ok {
		log.Printf("[DEBUG] Stop (addr: %s, model: %s, lang: %s, samplerate: %s)", info.Addr, info.Model, info.Lang, info.Samplerate)
		mapMutex.Unlock()
		// DNN or LSTM STT
		if info.Proto != 2 {
			conn, err := grpc.Dial(info.Addr, grpc.WithInsecure())
			if err != nil {
				return nil, err
			}
			defer conn.Close()
			c := pb2.NewSttModelResolverClient(conn)
			return c.Stop(context.Background(), in)
		} else {
			// CNN STT
			ss.Running = true
			ss.ServerAddress = fmt.Sprintf("%s:%d", *ip, *port)
			return &ss, nil
		}
	} else {
		log.Printf("[ERROR] Stop() - No such model: %s", in.Model)
		ss.Running = false
		mapMutex.Unlock()
		return &ss, status.Errorf(codes.NotFound, "No such model")
	}
}

func (s *modelServer) Restart(ctx context.Context, in *pb2.Model) (*pb2.ServerStatus, error) {
	var ss pb2.ServerStatus
	mapMutex.Lock()
	if info, ok := sttMap[in.Model]; ok {
		log.Printf("[DEBUG] Restart (addr: %s, model: %s, lang: %s, samplerate: %s)", info.Addr, info.Model, info.Lang, info.Samplerate)
		mapMutex.Unlock()
		// DNN or LSTM STT
		if info.Proto != 2 {
			conn, err := grpc.Dial(info.Addr, grpc.WithInsecure())
			if err != nil {
				return nil, err
			}
			defer conn.Close()
			c := pb2.NewSttModelResolverClient(conn)
			return c.Restart(context.Background(), in)
		} else {
			// CNN STT
			ss.Running = true
			ss.ServerAddress = fmt.Sprintf("%s:%d", *ip, *port)
			return &ss, nil
		}
	} else {
		log.Printf("[ERROR] Restart() - No such model: %s", in.Model)
		ss.Running = false
		mapMutex.Unlock()
		return &ss, status.Errorf(codes.NotFound, "No such model")
	}
}

func (s *modelServer) Ping(ctx context.Context, in *pb2.Model) (*pb2.ServerStatus, error) {
	log.Printf("ping")
	var ss pb2.ServerStatus
	mapMutex.Lock()
	if info, ok := sttMap[in.Model]; ok {
		log.Printf("[DEBUG] Ping (addr: %s, model: %s, lang: %s, samplerate: %s)", info.Addr, info.Model, info.Lang, info.Samplerate)
		mapMutex.Unlock()
		// DNN or LSTM STT
		if info.Proto != 2 {
			conn, err := grpc.Dial(info.Addr, grpc.WithInsecure())
			if err != nil {
				return nil, err
			}
			defer conn.Close()
			c := pb2.NewSttModelResolverClient(conn)
			return c.Ping(context.Background(), in)
		} else {
			// CNN STT
			ss.Running = true
			ss.ServerAddress = fmt.Sprintf("%s:%d", *ip, *port)
			return &ss, nil
		}
	} else {
		log.Printf("[ERROR] Ping() - No such model: %s", in.Model)
		ss.Running = false
		mapMutex.Unlock()
		return &ss, status.Errorf(codes.NotFound, "No such model")
	}
}

func (s *mockRealServer) Ping(ctx context.Context, in *pb2.Model) (*pb2.ServerStatus, error) {
	var ss pb2.ServerStatus
	mapMutex.Lock()
	if info, ok := sttMap[in.Model]; ok {
		log.Printf("[DEBUG] Ping (addr: %s, model: %s, lang: %s, samplerate: %s)", info.Addr, info.Model, info.Lang, info.Samplerate)
		mapMutex.Unlock()
		// DNN or LSTM STT
		if info.Proto != 2 {
			conn, err := grpc.Dial(info.Addr, grpc.WithInsecure())
			if err != nil {
				return nil, err
			}
			defer conn.Close()
			c := pb2.NewSttRealServiceClient(conn)
			return c.Ping(context.Background(), in)
		} else {
			// CNN STT
			conn, err := grpc.Dial(info.Addr, grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(time.Second))
			if err != nil {
				log.Printf("[ERROR] Ping() - Connection timeout")
				return nil, err
			}
			defer conn.Close()
			ss.Running = true
			ss.ServerAddress = fmt.Sprintf("%s:%d", *ip, *port)
			return &ss, nil
		}
	} else {
		log.Printf("[ERROR] Ping() - No such model: %s", in.Model)
		ss.Running = false
		mapMutex.Unlock()
		return &ss, status.Errorf(codes.NotFound, "No such model")
	}
}

func streamRecognizeCNN(info *sttServerInfo, stream pb2.SpeechToTextService_StreamRecognizeServer) error {
	realConn, err := grpc.Dial(info.Addr, grpc.WithInsecure())
	defer realConn.Close()

	c := pb3.NewSpeechToTextClient(realConn)
	// md := metadata.Pairs(
	// 	"in.model", Model,
	// 	"in.lang", Lang,
	// 	"in.samplerate", SampleRate,
	// )
	ctx, cancel := context.WithTimeout(context.Background(), 86400*time.Second)
	defer cancel()
	// ctx := context.NewOutgoingContext(context.Background())
	substream, err := c.StreamRecognize(ctx)
	if err != nil {
		log.Printf("[ERROR] %v", err)
		return err
	}

	waitc := make(chan struct{})
	go func() {
		for {
			in, err := substream.Recv()
			if err == io.EOF {
				close(waitc)
				return
			}
			if err != nil {
				log.Printf("[ERROR] Fail to message: %v", err)
				close(waitc)
				return
			}
			result := &pb2.Segment{}
			result.Start = in.Start * 100 / 8000
			result.End = in.End * 100 / 8000
			result.Txt = in.Txt
			err = stream.Send(result)
		}
	}()

	for {
		in, err := stream.Recv()
		if err == io.EOF {
			substream.CloseSend()
			<-waitc
			log.Printf("[INFO] End of StreamRecognize")
			return nil
		}
		if err != nil {
			substream.CloseSend()
			<-waitc
			log.Printf("[ERROR] Unexpected end of StreamRecognize")
			return err
		}
		audio := &pb3.Speech{}
		audio.Bin = in.Bin
		substream.Send(audio)
	}
}

func streamRecognizeDNN(info *sttServerInfo, stream pb2.SpeechToTextService_StreamRecognizeServer, md metadata.MD) error {
	realConn, err := grpc.Dial(info.Addr, grpc.WithInsecure())
	defer realConn.Close()

	c := pb2.NewSpeechToTextServiceClient(realConn)
	// md := metadata.Pairs(
	// 	"in.model", Model,
	// 	"in.lang", Lang,
	// 	"in.samplerate", SampleRate,
	// )
	ctx, cancel := context.WithTimeout(context.Background(), 86400*time.Second)
	defer cancel()

	md["in.model"] = []string{info.Model}
	ctx = metadata.NewOutgoingContext(ctx, md)
	// ctx := context.NewOutgoingContext(context.Background())
	substream, err := c.StreamRecognize(ctx)
	if err != nil {
		log.Printf("[ERROR] %v", err)
		return err
	}

	waitc := make(chan struct{})
	go func() {
		for {
			in, err := substream.Recv()
			if err == io.EOF {
				close(waitc)
				return
			}
			if err != nil {
				log.Printf("[ERROR] Fail to message: %v", err)
				close(waitc)
				return
			}
			err = stream.Send(in)
		}
	}()

	for {
		in, err := stream.Recv()
		if err == io.EOF {
			substream.CloseSend()
			<-waitc
			log.Printf("[INFO] End of StreamRecognize")
			return nil
		}
		if err != nil {
			substream.CloseSend()
			<-waitc
			log.Printf("[ERROR] Unexpected end of StreamRecognize")
			return err
		}
		substream.Send(in)
	}
}

func (s *myServer) StreamRecognize(stream pb2.SpeechToTextService_StreamRecognizeServer) error {
	sttID := "default"
	var info *sttServerInfo
	md, ok := metadata.FromIncomingContext(stream.Context())
	if ok {
		if model, ok := md["in.model"]; ok {
			mapMutex.Lock()
			// metadata의 value 타입은 []string
			if info, ok = sttMap[model[0]]; ok {
				sttID = model[0]
				mapMutex.Unlock()
			} else {
				log.Printf("[ERROR] No such model: %s", model)
				err := status.Error(codes.NotFound, "no such model")
				mapMutex.Unlock()
				return err
			}
		}
	}
	if info == nil {
		mapMutex.Lock()
		info = sttMap[sttID]
		mapMutex.Unlock()
	}
	log.Printf("[INFO] Got request, STT ID: %s, PROTO: %d", sttID, info.Proto)

	if info.Proto != 2 {
		return streamRecognizeDNN(info, stream, md)
	}
	return streamRecognizeCNN(info, stream)
}

func readConfig(filename string, defaults map[string]interface{}) (*viper.Viper, error) {
	v := viper.New()
	for key, value := range defaults {
		v.SetDefault(key, value)
	}
	v.SetConfigFile(filename)
	v.SetConfigType("toml")
	v.AutomaticEnv()
	err := v.ReadInConfig()
	return v, err
}

func setDbConnection(v1 *viper.Viper) {
	dbType = v1.GetString("database.type")
	if dbType == "oracle" {
		dbType = "goracle"
		// username@[//]host[:port][/service_name][:server][/instance_name]
		dbConnectionString = fmt.Sprintf("%s/%s@%s:%d/%s",
			v1.GetString("database.user"),
			v1.GetString("database.passwd"),
			v1.GetString("database.host"),
			v1.GetInt("database.port"),
			v1.GetString("database.name"))
	} else if dbType == "mysql" {
		// "aicc:ggoggoma@tcp(aicc-bqa.cjw9kegbaf8s.ap-northeast-2.rds.amazonaws.com:3306)/happycall"
		dbConnectionString = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
			v1.GetString("database.user"),
			v1.GetString("database.passwd"),
			v1.GetString("database.host"),
			v1.GetInt("database.port"),
			v1.GetString("database.name"))
	} else if dbType == "sqlite3" {
		dbConnectionString = v1.GetString("database.path")
	}

	log.Printf("[INFO] dbConnection: %s", dbConnectionString)
}

func getSttServerInfo() {
	mapMutex.Lock()
	defer mapMutex.Unlock()
	sttMap["default"] = &sttServerInfo{0, "127.0.0.1:9801", "baseline", "kor", "8000"}
	db, err := sql.Open(dbType, dbConnectionString)
	if err != nil {
		log.Fatalf("[ERROR] %v", err)
	}
	defer db.Close()

	rows, err := db.Query(`
		SELECT
			ID, PROTO, ADDR, MODEL, LANG, SAMPLERATE
		FROM
			STT_PROXY_INFO`)
	if err != nil {
		log.Fatalf("[ERROR] %v", err)
	}

	for rows.Next() {
		var sttID string
		var info sttServerInfo
		err = rows.Scan(&sttID, &info.Proto, &info.Addr, &info.Model, &info.Lang, &info.Samplerate)
		if err != nil {
			log.Fatalf("sql error: %v", err)
		}
		sttMap[sttID] = &info
	}
	log.Printf("----------------------------- STT SERVER INFO ----------------------------------")
	var keys []string
	for k := range sttMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		val := sttMap[k]
		log.Printf("[INFO] ID: %s,\tAddr: %s,\tModel:%s", k, val.Addr, val.Model)
	}
	log.Printf("--------------------------------------------------------------------------------")
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
	flag.Parse()
	v1, err := readConfig(*cfgFile, map[string]interface{}{})
	if err != nil {
		log.Fatalf("[ERROR] %v", err)
	}
	setDbConnection(v1)
	getSttServerInfo()

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", *ip, *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	log.Printf("[INFO] Server started, Listen port is %d", *port)

	grpcServer2 := grpc.NewServer()
	pb2.RegisterSpeechToTextServiceServer(grpcServer2, &myServer{})
	pb2.RegisterSttRealServiceServer(grpcServer2, &mockRealServer{})
	pb2.RegisterSttModelResolverServer(grpcServer2, &modelServer{})

	grpcServer2.Serve(lis)
}
