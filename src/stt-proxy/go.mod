module mindslab/stt-proxy

go 1.15

replace maum => ./maum

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/spf13/viper v1.9.0
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f
	google.golang.org/grpc v1.41.0
	maum v0.0.0-00010101000000-000000000000
)
