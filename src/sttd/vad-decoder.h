#ifndef VAD_DECODER_H
#define VAD_DECODER_H

#include "Laser.h"
#include "decoder.h"
#include "dnnvad.h"
#include "stt-master.h"

class VadDecoder : public Decoder
{
 public:
  // Constructor & Destructor
  VadDecoder(SttStream *stream);
  virtual ~VadDecoder();

  // Overide
  virtual void StepFeatFrame(int &t, int min_frame_count);
  virtual void StepFeatFrameLast(int &t, int min_frame_count);
  virtual bool Initialize();

 private:
  // ETRI 소스 비교를 위하여 Naming Rule은 무시하고 원본 소스를 그대로 사용
  unsigned int nlookahead;
  unsigned int noutput;

  unsigned int wait_count; // feature buffer가 채워지지 못한 경우를 count
  unsigned int partial_count; // partial 결과를 출력하기 위한 count
  unsigned int reset_count; // reset 을 위한 count
  // unsigned int ndata; // featbuf에 들어있는 데이터의 길이 num of floats

  char* resultbuf;
  unsigned int max_result_len;
  int has_partial_result;

  unsigned int t_1st; // am 계산 time
  unsigned int t_dec; // decoder time
  unsigned int t_dec_prev; // previous decoder time for notoken case
  float *dnnout;
  float *dnnout2;
  int *dnnIndex;
  DNNVAD *pVAD;
  int prev_vad;
  unsigned int min_reset_period;
  int numOutNode;

  // char notoken[MAXLENDATA];
  SttMaster *master_;
};

extern "C" {
  int getSARecResultSLFExt( Laser *a_recP, int a_t, char *a_slfFn, char *a_WB );
  int getSARecResultSLF( Laser *a_recP, int a_t, char *a_slfFn );
  int getDLNetChunkSize();

  int stepSARecFrame1st(Laser *a_recP, int a_t, int a_dim, float *a_Ot );
  int stepSARecFrame2ndCom(Laser *a_recP, int a_t, int a_dim, int frame, int );
  int stepSARecFrame2nd(Laser *a_recP, int a_t, int a_dim, float *a_post);
  int stepSARecFrame2ndExt(Laser *a_recP, int a_t, int a_dim, float *a_post, float *a_post2 );
  int getSARecDNNNumOutNode(Laser *a_recP);
  int *getSARecDNNFrameOrder(Laser *a_recP, int nframes);
  float *GetDNNPosterior(Laser *a_recP);
  float *GetDNNPosteriorExt(Laser *a_recP, int isProb);
  int stepSARecFrameExt(Laser *a_laserP, int a_t, int featureDim, float *a_Ot);
}

#endif /* VAD_DECODER_H */
