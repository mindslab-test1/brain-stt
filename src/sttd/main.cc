#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <grpc++/grpc++.h>

#include "stt-service-impl.h"
#include "stt-real-impl.h"

#include <libmaum/common/config.h>
#include <csignal>

#include <libmaum/common/util.h>
#include <gitversion/version.h>
#include "remote-logger.h"
#include "sttd-variable.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using namespace std;

volatile std::sig_atomic_t g_signal_status;
grpc::Server *g_server = NULL;

void HandleSignal(int signal) {
  // CALL DESTRUCTOR
  LOGGER()->info("handling {}", signal);
  g_signal_status = signal;
  if (g_server) {
    auto deadline = std::chrono::system_clock::now() + std::chrono::milliseconds(800);
    g_server->Shutdown(deadline);
  }
  //exit(0);
}

void read_file(std::string dname, std::string fname, std::string& data) {
  int rc;
  char buf[1024];
  std::string path = dname + "/" + fname;
  int fd = open(path.c_str(), O_RDONLY);
  if (fd > 0) {
    while ((rc = read(fd, buf, sizeof(buf))) > 0) {
      data.append(buf, rc);
    }
    close(fd);
  }
}

void RunServer() {
  auto &c = libmaum::Config::Instance();
  auto logger = LOGGER();

  std::string server_address("0.0.0.0:");
  server_address += c.Get("brain-stt.sttd.front.port");
  int grpc_timeout = atoi(c.Get("brain-stt.sttd.front.timeout").c_str());
  c.DumpPid();

  SttServiceImpl stt_service;
  SttModelResolverImpl stt_model_resolver;
  ServerBuilder builder;

  grpc::SslServerCredentialsOptions sslOps;
  if (g_var.grpc_use_ssl) {
    std::string key, cert, root_ca;
    const char* env_maum_root = std::getenv("MAUM_ROOT");
    read_file(env_maum_root, "etc/server.key", key);
    read_file(env_maum_root, "etc/server.crt", cert);
    //read_file(env_maum_root, "etc/ca.crt", root_ca);

    grpc::SslServerCredentialsOptions::PemKeyCertPair keycert = {key, cert};

    //sslOps.pem_root_certs = root_ca;
    sslOps.pem_key_cert_pairs.push_back(keycert);
  }

  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_IDLE_MS, grpc_timeout);
  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_AGE_MS, g_var.grpc_max_conn_age_ms);
  builder.AddChannelArgument(GRPC_ARG_MAX_CONNECTION_AGE_GRACE_MS, 0 * 1000);
  //builder.AddChannelArgument(GRPC_ARG_KEEPALIVE_TIME_MS, 200);
  //builder.AddChannelArgument(GRPC_ARG_KEEPALIVE_TIMEOUT_MS, 200);
  builder.AddChannelArgument(GRPC_ARG_SERVER_HANDSHAKE_TIMEOUT_MS, g_var.grpc_handshake_timeout_ms);
  //builder.AddChannelArgument(GRPC_ARG_KEEPALIVE_PERMIT_WITHOUT_CALLS, 0 /* true */);
  if (g_var.grpc_use_ssl) {
    builder.AddListeningPort(server_address, grpc::SslServerCredentials(sslOps));
  } else {
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  }

  builder.RegisterService(&stt_service);
  builder.RegisterService(&stt_model_resolver);

  unique_ptr<Server> server(builder.BuildAndStart());
  if (server) {
    logger->info("{} listening on {}",
                 stt_service.name(), server_address);
    g_server = server.get();
    server->Wait();
  }
  logger->info("gRPC server shutdown");
}


char *g_program_dir = nullptr;
extern void ChildHandler(int sig);

void help(char *argv) {
  printf("sttd [--version] [--help]\n");
}

void process_option(int argc, char *argv[]) {
  bool do_exit = false;
  int c;
  while (1) {
    static const struct option long_options[] = {
      {"version", no_argument, 0, 'v'},
      {"help", no_argument, 0, 'h'},
      {NULL, no_argument, NULL, 0 }
    };

    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long(argc, argv, "vh?", long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
      case 0:
        break;
      case 'v':printf("brain-stt.version %s\n", version::VERSION_STRING);
        // cout << version::IS_STABLE_VERSION << endl;
        // cout << version::GIT_COMMIT_ID << endl;
        // cout << version::GIT_COMMITS_SINCE_TAG << endl;
        do_exit = true;
        break;
      case 'h':
        help(argv[0]);
        do_exit = true;
        break;
      case '?':
        help(argv[0]);
        do_exit = true;
        break;
      default:
        help(argv[0]);
        do_exit = true;
    }
  }

  if (do_exit)
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
  process_option(argc, argv);

  auto &c = libmaum::Config::Init(argc, argv, "brain-stt.conf");

  std::signal(SIGINT, HandleSignal);
  //std::signal(SIGSEGV, HandleSignal);
  //std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);
  
  auto program = c.GetProgramPath();
  auto pos = program.find_last_of("/");
  assert(pos != string::npos);
  g_program_dir = strdup(program.substr(0, pos).c_str());

  signal(SIGCHLD, ChildHandler);

  g_var.Initialize();

  auto logger = LOGGER();
  logger->flush_on(spdlog::level::trace);
  logger->set_level(spdlog::level::from_str(g_var.log_stt_level));
  logger->info("start stt main server");

  g_var.Verify();
  g_var.PrintConfig();

  REMOTE_LOGGER()->Start();

  // STT 이력 테이블 정리 Thread
  std::unique_ptr<SttHistory> history;
  if (!g_var.history_db.empty()) {
    history.reset(new SttHistory(g_var.history_db));
    history->Start(g_var.history_rollover_hour, g_var.history_max_days);
  }

  EnableCoreDump();
  RunServer();
  return 0;
}
