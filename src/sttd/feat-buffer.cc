#include <string.h>
#include "feat-buffer.h"

FeatBuffer::FeatBuffer()
    : featdim_(0),
      max_frame_count_(0),
      buf_(nullptr),
      rd_ptr_(nullptr),
      wr_ptr_(nullptr) {
}

FeatBuffer::~FeatBuffer() {
  if (buf_)
    delete [] buf_;
}

bool FeatBuffer::Create(int featdim /* 차원벡터크기 */,
            int max_frame_count /* 최대 프레임 크기 */) {
  featdim_ = featdim;
  max_frame_count_ = max_frame_count;
  buf_ = new float[featdim_ * max_frame_count_];
  rd_ptr_ = buf_;
  wr_ptr_ = buf_;
  return (buf_ != nullptr);
}

void FeatBuffer::Reset() {
  int len = wr_ptr_ - rd_ptr_;
  memmove((char *)buf_, (char *)rd_ptr_, len * sizeof(float));
  rd_ptr_ = buf_;
  wr_ptr_ = buf_ + len;
}

float *FeatBuffer::GetBuffer() {
  return wr_ptr_;
}

void FeatBuffer::MoveWritePtr(int len) {
  wr_ptr_ += len;
}

int FeatBuffer::GetFrameCount(int min_frame_count) {
  int nf;
  nf = (wr_ptr_ - rd_ptr_) / featdim_;
  return nf / min_frame_count * min_frame_count;
}

float *FeatBuffer::GetFeatBuffer(int min_frame_count) {
  float *feat = rd_ptr_;
  rd_ptr_ += featdim_ * min_frame_count;
  return feat;
}
