#include "sttd-variable.h"
#include <iostream>
#include <grpc++/grpc++.h>

#include "stt-real-impl.h"

#include <libmaum/common/config.h>
#include <csignal>
#include <sys/prctl.h>
#include <getopt.h>
#include <sys/resource.h>
#include <libmaum/common/util.h>
#include "sent-boundary.h"
#include "remote-logger.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;
using maum::common::LangCode;

using namespace std;

volatile std::sig_atomic_t g_signal_status;
grpc::Server *g_server = NULL;

void HandleSignal(int signal) {
  // CALL DESTRUCTOR
  REMOTE_LOGGER()->info("handling {}", signal);
  g_signal_status = signal;
  if (g_server) {
    auto deadline = std::chrono::system_clock::now() + std::chrono::milliseconds(500);
    g_server->Shutdown(deadline);
  }
  // usleep(100 * 1000);
  // exit(0);
}

/**
 * 새로운 child process의 메인 함수
 *
 * Classifier 시스템을 초기화하고 정상적으로 동작하도록 한다.
 *
 * @param name 도메인 이름
 * @param path DB 경로
 * @param sample_rate STT 처리 속도
 * @param endpoint 응답 엔드포인트
 * @param port 서버 포트
 */
void RunSttChild(const string &name, const string &lang,
                 int32_t sample_rate,
                 const string &endpoint, int port,
                 int gpu_idx) {
  std::string server_address("0.0.0.0:");
  server_address += std::to_string(port);

  LangCode lang_code = LangCode::kor;  // for avoid -Wmaybe-uninitialized
  maum::common::LangCode_Parse(lang, &lang_code);

  if (lang_code == LangCode::eng) {
    REMOTE_LOGGER()->info("start open_knowledge()");
    auto &c = libmaum::Config::Instance();
    auto p = c.Get("brain-stt.resource.tools");
    open_knowledge(p.c_str());
    REMOTE_LOGGER()->info("end open_knowledge()");
  }

  SttRealService::Service *service;
  service = new SttRealServiceImpl(name, lang_code, sample_rate, endpoint, gpu_idx);

  ServerBuilder builder;
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  builder.RegisterService(service);
  unique_ptr<Server> server(builder.BuildAndStart());
  if (server) {
    REMOTE_LOGGER()->info("new child stt server {} {} {} listening on {}",
                          name, lang, sample_rate, port);
    g_server = server.get();
    server->Wait();
  }

  if (lang_code == LangCode::eng) {
    REMOTE_LOGGER()->info("call close_knowledge()");
    close_knowledge();
  }
  REMOTE_LOGGER()->info("gRPC server shutdown ({}-{}-{})", name, lang, sample_rate);
}

void Usage(const char *prname) {
  std::cout
      << prname << " [-n name] [-l lang] [-e endpoint] [-p port] [-r sample-rate] [-g gpu_idx]"
      << std::endl
      << "-n --name : stt model name" << endl
      << "-l --lang : language (kor, eng)" << endl
      << "-e --endpoint : export_ip + \":\" + port" << endl
      << "-r --rate : sample rate (16000 or 8000)" << endl
      << "-p --port : port" << endl
      << "-g --gpu  : gpu" << endl;

  exit(0);
}

extern void ChildHandler(int sig);

int main(int argc, char *argv[]) {
  auto &c = libmaum::Config::Init(argc, argv, "brain-stt.conf");

  std::signal(SIGINT, HandleSignal);
//  std::signal(SIGSEGV, HandleSignal);
//  std::signal(SIGBUS, HandleSignal);
  std::signal(SIGTERM, HandleSignal);

  char *base = basename(argv[0]);

  char *name = nullptr;
  char *endpoint = nullptr;
  char *port = nullptr;
  char *lang = nullptr;
  char *sample_rate = nullptr;
  char *gpu_num = nullptr;

  while (true) {
    int optindex = 0;
    static option fork_option[] = {
        {"name", required_argument, 0, 'n'},
        {"lang", required_argument, 0, 'l'},
        {"samplerate", required_argument, 0, 'r'},
        {"endpoint", required_argument, 0, 'e'},
        {"port", required_argument, 0, 'p'},
        {"gpu", required_argument, 0, 'g'},
        {"help", no_argument, 0, 'h'}
    };
    int ch = getopt_long(argc, argv, "n:l:r:e:p:g:h", fork_option, &optindex);
    if (ch == -1) {
      break;
    }

    switch (ch) {
      case 'n': {
        name = optarg;
        break;
      }
      case 'l': {
        lang = optarg;
        break;
      }
      case 'r': {
        sample_rate = optarg;
        break;
      }
      case 'e': {
        endpoint = optarg;
        break;
      }
      case 'p': {
        port = optarg;
        break;
      }
      case 'h': {
        Usage(base);
        break;
      }
      case 'g': {
        gpu_num = optarg;
        break;
      }
      default: {
        Usage(base);
        break;
      }
    }
  }

  string process_name("brain-stt-");
  string model_name;
  model_name += lang[0];
  model_name += sample_rate[0];
  model_name += '-';
  model_name += name;
  process_name += model_name;
  c.SetProgramName("brain-sttd");
  c.SetLoggerName(process_name);

  g_var.Initialize();

  REMOTE_LOGGER()->Connect(model_name);
  REMOTE_LOGGER()->debug("start stt child server {}", process_name);
  int s = ::prctl(PR_SET_NAME, process_name.c_str(), NULL, NULL, NULL);
  snprintf(argv[0], strlen(argv[0]), "stt %s %c %c g%d %s",
           name, lang[0], sample_rate[0], std::stoi(gpu_num), port);
  REMOTE_LOGGER()->debug("[prctl status] {}", s);
  EnableCoreDump();
  SetNoFile(4096);
  RunSttChild(name, lang, stoi(sample_rate), endpoint, stoi(port), stoi(gpu_num));
  return 0;
}
