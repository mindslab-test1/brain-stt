#include "sttd-variable.h"
#include "vad-decoder.h"
#include "stt-stream.h"
#include "remote-logger.h"
#include <libmaum/common/util.h>
#include <libmaum/common/fileutils.h>

VadDecoder::VadDecoder(SttStream *stream) : Decoder(stream) {
  master_ = stream_->p();
}

VadDecoder::~VadDecoder() {
  if (dnnIndex) {
    free(dnnIndex);
    REMOTE_LOGGER()->debug("END: Free SARecDNNFrameOrder");
  }
  if (pVAD) {
    freeDNNVAD(pVAD);
    REMOTE_LOGGER()->debug("END: Free DNNVAD");
  }
}

bool VadDecoder::Initialize() {
  // ETRI엔진 변수
  wait_count = 0;
  partial_count = 0; // partial 결과를 출력하기 위한 count
  reset_count = 0; // reset 을 위한 count
  reset_period = master_->vad_reset_period;
  if (reset_period > 10000 /* 100초 */) {
    reset_period = 10000;
  }
  // unsigned int ndata; // featbuf에 들어있는 데이터의 길이 num of floats

  max_result_len = 8192;
  has_partial_result = 0;

  t_1st = 0; // am 계산 time
  t_dec = 0; // decoder time
  t_dec_prev = 0; // previous decoder time for notoken case

  min_reset_period = master_->vad_min_reset_period;

  dnnIndex = getSARecDNNFrameOrder(pLaser, nminibatch);
  pVAD = NULL;
  prev_vad = 0;
  numOutNode = getSARecDNNNumOutNode(pLaser);

  pVAD = createDNNVAD(master_->epdstat_info.c_str(), numOutNode);
  if (pVAD == NULL) {
    return false;
  }

  // std::string vadcfg_fname = master_->vad_cfg;
  // if (vadcfg_fname.front() != '/') {
  //   vadcfg_fname = ModelDname + "/" + vadcfg_fname;
  // }

  if (IsFile(master_->vad_cfg.c_str(), 0)) {
    REMOTE_LOGGER()->info("Load dnnvad.cfg...");
    readOptionDNNVAD(pVAD, master_->vad_cfg.c_str());
  } else {
    REMOTE_LOGGER()->info("Not found dnnvad.cfg...");
  }

  resetDNNVAD(pVAD);

  return true;
}

void VadDecoder::StepFeatFrame(int &interim_local, int min_frame_count) {
  // // Local Variables...
  // Laser *pLaser = stream_->laser();
  int ndata = featdim * nminibatch;

  // REMOTE_LOGGER()->error("{}, {}, {}", featdim, nminibatch, min_reset_period);

  while (feat_buf_->GetFrameCount(min_frame_count) > 0) {
    stepSARecFrame1st(pLaser, t_1st++ * nminibatch,
                      ndata, feat_buf_->GetFeatBuffer(min_frame_count));
    dnnout = GetDNNPosterior(pLaser);
    dnnout2 = GetDNNPosteriorExt(pLaser, 1);
    int *vadout = doDNNVAD(pVAD, dnnout, nminibatch, 0, dnnIndex);
    // REMOTE_LOGGER()->error("min_frame_count : {} {}", min_frame_count, min_reset_period);

    auto make_result = [&]() {
      if (!PartialBackTracking(t_dec)) {
        REMOTE_LOGGER()->error("PartialBackTracking failed");
        // return;
      } else {
        REMOTE_LOGGER()->debug("PartialBackTracking success");
      }

      t_dec_prev = t_dec;
      //reallocSLaser( laser() );
      //resetSLaser( laser() );
      partial_count = 0;
      reset_count = 0;
      stream_->ResetStream(t_dec);
      interim_local = 0;
    };

    for (int n = 0; n < min_frame_count; n++) {
      float *p = &(dnnout[dnnIndex[n]]);
      float *p2 = &(dnnout2[dnnIndex[n]]);
      if ((prev_vad == 1) && (vadout[n] == 0) && (reset_count > min_reset_period)) {
        REMOTE_LOGGER()->debug("Do PartialBackTracking");
        make_result();
      }
      stepSARecFrame2ndExt(pLaser, t_dec++, ndata, p, p2);
      interim_local++;
      prev_vad = vadout[n];
    }
    reset_count += nminibatch;
    partial_count += nminibatch;

    if (reset_count >= reset_period) {
      REMOTE_LOGGER()->info("reset_period is exceeded...!");
      make_result();
    }
    wait_count = 0;
  }
}

void VadDecoder::StepFeatFrameLast(int &t, int min_frame_count) {
  int N;
  int nf;
  int ndata;

  while ((nf = feat_buf_->GetFrameCount(1)) > 0) {
    ndata = featdim * nf;
    stepSARecFrame1st(pLaser, t_1st++ * nminibatch,
                      featdim * nminibatch, feat_buf_->GetFeatBuffer(min_frame_count));
    N = nminibatch < (unsigned int)nf ? nminibatch : nf;
    dnnout = GetDNNPosterior(pLaser);
    dnnout2 = GetDNNPosteriorExt(pLaser, 1);
    // REMOTE_LOGGER()->error("min_frame_count : {} {}", min_frame_count, min_reset_period);

    for (int n = 0; n < N; n++) {
      float *p = &(dnnout[dnnIndex[n]]);
      float *p2 = &(dnnout2[dnnIndex[n]]);
      stepSARecFrame2ndExt(pLaser, t_dec++, ndata, p, p2);
    }
    reset_count += N;
    partial_count += nminibatch;
  }

  if (reset_count > 0) {
    REMOTE_LOGGER()->error("Do PartialBackTracking Last");
    if (!PartialBackTracking(t_dec)) {
      REMOTE_LOGGER()->error("PartialBackTracking failed");
      // return;
    }
    REMOTE_LOGGER()->error("PartialBackTracking success");

    t_dec_prev = t_dec;
  }
}
