#ifndef REMOTE_LOGGER_H
#define REMOTE_LOGGER_H

#include <thread>
#include <mutex>
#include "spdlog/spdlog.h"
#include "zmq.h"

class RemoteLogger {
 public:
  RemoteLogger();
  ~RemoteLogger();

  template<typename... Args>
  void log(spdlog::level::level_enum lvl, const char *buf, const Args &... args);
  template<typename... Args>
  void trace(const char *buf, const Args &... args);
  template<typename... Args>
  void debug(const char *buf, const Args &...  args);
  template<typename... Args>
  void info(const char *buf, const Args &...  args);
  template<typename... Args>
  void warn(const char *buf, const Args &...  args);
  template<typename... Args>
  void error(const char *buf, const Args &...  args);
  template<typename... Args>
  void critical(const char *buf, const Args &...  args);

  void Connect(std::string name);
  void Start();
  void Run();

 private:
  void *pub_ctx_;
  void *publisher_;
  void *sub_ctx_;
  void *subscriber_;
  std::thread thrd_;

  // Logger Name
  std::string name_;

  std::mutex send_lock_;

  bool is_done_;
};

#define __SEND_LOG_SIZE 1024

template<typename... Args>
inline void RemoteLogger::log(spdlog::level::level_enum lvl, const char *buf, const Args &... args) {
  std::string msg = fmt::format(buf, args...);

  std::unique_lock<std::mutex> lock(send_lock_);

  // Set Log Level
  zmq_send(publisher_, &lvl, sizeof(int), ZMQ_SNDMORE);

  // Set Logger Name
  zmq_send(publisher_, name_.c_str(), name_.size(), ZMQ_SNDMORE);

  // Send Log Message
  auto q = msg.size() / __SEND_LOG_SIZE;
  for (auto i = 0; i < q; i++) {
    std::string part = msg.substr(__SEND_LOG_SIZE * i, __SEND_LOG_SIZE);
    zmq_send(publisher_, part.c_str(), part.size(), ZMQ_SNDMORE);
  }

  std::string last_part = msg.substr(__SEND_LOG_SIZE * q);
  zmq_send(publisher_, last_part.c_str(), last_part.size(), 0);
}

template<typename... Args>
inline void RemoteLogger::critical(const char *buf, const Args &... args) {
  log(spdlog::level::critical, buf, args...);
}

template<typename... Args>
inline void RemoteLogger::error(const char *buf, const Args &... args) {
  log(spdlog::level::err, buf, args...);
}

template<typename... Args>
inline void RemoteLogger::warn(const char *buf, const Args &... args) {
  log(spdlog::level::warn, buf, args...);
}

template<typename... Args>
inline void RemoteLogger::info(const char *buf, const Args &... args) {
  log(spdlog::level::info, buf, args...);
}

template<typename... Args>
inline void RemoteLogger::debug(const char *buf, const Args &... args) {
  log(spdlog::level::debug, buf, args...);
}

template<typename... Args>
inline void RemoteLogger::trace(const char *buf, const Args &... args) {
  log(spdlog::level::trace, buf, args...);
}

template<class T>
class Singleton {
 public:
  static T* getInstance() {
    return &(getSingleton());
  }

  static T& getSingleton() {
    static T instance;
    return instance;
  }

  ~Singleton() {};

 protected:
  Singleton(const Singleton& sig);
  Singleton& operator = (const Singleton& sig);
  Singleton() {};
};

#define REMOTE_LOGGER() Singleton<RemoteLogger>::getInstance()

#endif /* REMOTE_LOGGER_H */
