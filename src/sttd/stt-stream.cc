#include <iostream>

#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <libmaum/common/encoding.h>

#include "ETRIPP.h"
#include "sent-boundary.h"
#include "stt-stream.h"
#include "remote-logger.h"
#include "sttd-variable.h"
#include "vad-decoder.h"

// 1 Frame은 10ms 내에 들어가는 오디오 데이타를 의미, 1 Sample은 16bit(2Byte)
#define SAMPLES_PER_FRAME_8K  80
#define SAMPLES_PER_FRAME_16K 160

using namespace std;

const int kRealtimePeriod = 50;
const int kResetPeriod = 12000; // 120sec
// const int kResetPeriod = 30000; // 300sec

std::mutex SttStream::segment_lock_;

SttStream::SttStream(SttMaster *master,
                     SttSlave *slave,
                     int32_t seq,
                     int32_t second,
                     int32_t pause_ms,
                     int32_t epd_timeout)
    : parent_(master),
      slave_(slave),
      str_seq_(seq),
      second_(second),
      pause_ms_(pause_ms) {
  slave_->Reset(seq);
  size_t sz = size_t(p()->mfcc_size_ * (p()->mini_batch_ + p()->nlookahead_) * 150);
  output_ = new float[sz];
  REMOTE_LOGGER()->debug("output_ count = {}", sz);
  next_reset_time_ = kResetPeriod;

  if (!feat_buf_.Create(p()->mfcc_size_, 120 * 100 /* 120 sec */)) {
    REMOTE_LOGGER()->error("Failed to allocate feat buffer !!!");
  }

  frame_samples_ = (slave_->sample_rate_ == 8000) ? SAMPLES_PER_FRAME_8K : SAMPLES_PER_FRAME_16K;

  // EPD없을 경우 최대 인식 시간, 10ms 단위
  if (epd_timeout > 0) {
    max_epd_time_ = epd_timeout;
    REMOTE_LOGGER()->info("max epd time (from meta) is {}", max_epd_time_);
  } else {
    max_epd_time_ = g_var.max_epd_time;
    REMOTE_LOGGER()->info("max epd time (from conf) is {}", max_epd_time_);
  }
  if (p()->use_vad) {
    REMOTE_LOGGER()->info("use dnn-vad decoder, SampleRate: {}", slave_->sample_rate_);
    decoder_.reset(new VadDecoder(this));
    decoder_->SampleRate = slave_->sample_rate_;
    decoder_->ModelDname = slave_->model_dirname_;
    if (decoder_->Initialize() == false) {
      REMOTE_LOGGER()->error("STT Decoder Initialize() Failed...");
    }
  }
}


SttStream::~SttStream() {
  if (slave_) {
    slave_->Clear();
  }
  if (output_) {
    delete[] output_;
  }

  SttMaster::stream_cnt_--;
}

void SttStream::EnableRealTime() {
  next_reset_time_ = kRealtimePeriod;
  is_real_time_ = true;
  is_epd_ = true;
}

void SttStream::EnableInterim(bool on) {
  need_interim_result_ = on;
}

bool SttStream::HasResult() {
  if (!is_real_time_)
    return (results_.size() > 0);

  return (!fragments_.empty());
}

bool SttStream::HasInterimResult() {
  if (!is_real_time_)
    return (interim_results_.size() > 0);

  return (!interim_fragments_.empty());
}

string SttStream::GetResult() {
  string result;
  for (auto s : results_)
    result += s;
  return result;
}

string SttStream::GetInterimResult() {
  string result;
  for (auto s : interim_results_)
    result += s;
  return result;
}

string SttStream::GetRawMlf(bool is_interim) {
  string result;
  if (is_interim) {
    for (auto s : interim_mlf_partials_)
      result += s;
  } else {
    for (auto s : mlf_partials_)
      result += s;
  }
  return result;
}

void SttStream::StepFrame(size_t n_read, int16_t *rbuf, int &t) {
  int mfcc_size = p()->mfcc_size_;
  int ret;
  int nf; // ?? length of frame?
  last_offset_ += n_read;

  // FIXME : Laser 객체를 재사용하고 gpu가 2개인 경우 t == 0 부분에서 비정상 종료
  //         현재는 메모리릭 때문에 Laser 를 재사용하지 않으므로 당분간은 재연되지 않음
  if (t == 0) {
    size_t sz = size_t(
        p()->mfcc_size_  * (s()->epd_len_front_margin_ + (p()->mini_batch_ + p()->nlookahead_) + 1));
    float *output2_ = new float[sz];
    REMOTE_LOGGER()->debug("buffer count = {}", sz);
    ret = stepFrameLFrontEnd(front(), n_read, rbuf, &out_len_, output2_);
    REMOTE_LOGGER()->debug("FIRST step frame read {}, returns {}, out_len_ = {}",
                    n_read, g_var.FSTATUS_ENUM_MAP[ret], out_len_);
    int i;
    for (i = 0; i < 15; i++)
      memcpy(output_ + (i * mfcc_size), output2_, sizeof(float) * mfcc_size);
    memcpy(output_ + (i * mfcc_size), output2_, sizeof(float) * out_len_);
    out_len_ = out_len_ + i * mfcc_size;
    delete[] output2_;
  } else {
    ret = stepFrameLFrontEnd(front(), n_read, rbuf, &out_len_, output_);
  }
  // REMOTE_LOGGER()->debug("step frame read {}, lfrontend returns {}, out_len_ = {}",
  //                 n_read, fstatus_map_[ret], out_len_);

  if (out_len_ > 0) {
    nf = out_len_ / mfcc_size;
    // 2016.02.29 updated
    // bug fix : minibatch of remained Ot
    if (nf < p()->mini_batch_) {
      memcpy(output_ + (nf * mfcc_size),
             p()->silence_,
             sizeof(float) * (mfcc_size * (p()->mini_batch_ + p()->nlookahead_ - nf)));
    }

    for (int k = 0; k < nf; k++) {
      // logger->debug() << t + k;
      stepSARecFrameExt(laser(), time_local_ + k, p()->feature_dim_,
                        output_ + (k * mfcc_size));
    }
    t += nf;
    time_local_ += nf;
    // RESET 시간이 지나면 MLF를 생성한다.
    if (time_local_ > next_reset_time_) {
      REMOTE_LOGGER()->debug(" result (t:{} frames), time_local: {}, nf: {}, reset:{}",
                      t, time_local_, nf, kResetPeriod);
      if (!PartialBackTracking(time_local_, partial_cnt_))
        return;
      if (is_real_time_) {
        // do not reset laser here
      } else {
        if (start_offset_ == -1)
          start_offset_ = (last_offset_ / frame_samples_) - time_local_;
        time_local_ = 0;
        reallocSLaser(laser());
        resetSLaser(laser());
      }

      partial_cnt_++;
    }
  }
}

bool SttStream::AddData(const string &data) {
  size_t mul;
  if (is_epd_) {
    mul = frame_samples_ * 2;
  } else {
    mul = (decltype(data.size())(p()->read_size_)) * 2;
  }
  buffer_.append(data);

  size_t buff_size = buffer_.size();
  size_t feed_size = buff_size / mul * mul;
  size_t done = 0;

  while (done < feed_size) {
    // 버퍼에 남아있는 데이터를 읽어들인다.
    int16_t * rbuf = (int16_t *)(buffer_.data()+ done);
    // 읽은 만큼 음성인식을 수행한다.
    if (is_epd_)
      StepFrameEPD(mul / 2, rbuf, frames_);
    else
      StepFrame(mul / 2, rbuf, frames_);
    done += mul;
  }
  if (buff_size - feed_size) {
    // 처리하고 남은 바이트를 buffer_에 저장해 둔다.
    buffer_ = buffer_.substr(feed_size);
  } else {
    buffer_.clear();
  }

  // REMOTE_LOGGER()->trace("add data final buffer : size() {} ", buffer_.size());
  return true;
}

void SttStream::StepFeatFrame(int &t, int min_frame_count) {
  int nf = feat_buf_.GetFrameCount(min_frame_count);
  for (int i = 0; i < nf; i++) {
    int rc = stepSARecFrameExt(laser(), time_local_, p()->feature_dim_, feat_buf_.GetFeatBuffer());
    if (rc != 0) {
      REMOTE_LOGGER()->error("stepSARecFrameExt() return {}", rc);
    }
    t++;
    time_local_++;
    if (need_interim_result_)
      interim_time_local_++;
  }
}

void SttStream::StepFrameEPD(size_t n_read, int16_t *rbuf, int &t) {
  // 벡터 차원
  int epdstat;

  epdstat = stepFrameLFrontEnd(front(), n_read, rbuf, &out_len_, output_);
  last_offset_ += n_read;

  if (out_len_ > 0) {
    memcpy(feat_buf_.GetBuffer(), output_, out_len_ * sizeof(float));
    feat_buf_.MoveWritePtr(out_len_);
    if (p()->use_vad) {
      decoder_->StepFeatFrame(interim_time_local_, p()->mini_batch_);
    } else {
      StepFeatFrame(t, p()->mini_batch_);
    }

    if (!p()->use_vad && (epdstat == detected || time_local_ >= max_epd_time_) ) {
      StepFeatFrame(t, 1);

      REMOTE_LOGGER()->debug(" result (t:{} frames), time_local: {}", t, time_local_);

      if (!PartialBackTracking(time_local_, partial_cnt_)) {
        REMOTE_LOGGER()->error("PartialBackTracking failed");
        // return;
      }

      start_offset_ = (last_offset_ / frame_samples_) - time_local_ - feat_buf_.GetFrameCount(1);
      time_local_ = 0;

      reallocSLaser(laser());
      resetSLaser(laser());
      feat_buf_.Reset();

      partial_cnt_++;

      if (need_interim_result_) {
        interim_time_local_ = 0;
      }
    } else if (need_interim_result_ && interim_time_local_ > 20) {
      REMOTE_LOGGER()->debug("interim_time_local_ is {}", interim_time_local_);
      if (!PartialBackTrackingInterim(time_local_, partial_cnt_)) {
        REMOTE_LOGGER()->error("PartialBackTracking failed");
      }
      //start_offset_ = (last_offset_ / frame_samples_) - time_local_;
      interim_time_local_ = 0;
    }
  } else {
    // out_len_ > 0 이 아닌 경우 시작 시간 보정
    if (p()->use_vad)
      start_offset_ += n_read / frame_samples_;
  }
}

bool SttStream::FinishEPD() {
  if (!buffer_.empty()) {
    REMOTE_LOGGER()->debug("remained buffer size is {}", buffer_.size());
    StepFrameEPD(buffer_.size()/2, (short *)buffer_.data(), frames_);
    buffer_.clear();
  }

  if (p()->use_vad) {
    decoder_->StepFeatFrameLast(frames_, p()->mini_batch_);
  } else {
    StepFeatFrame(frames_, 1);

    PartialBackTracking(time_local_, partial_cnt_);

    start_offset_ = (last_offset_ / frame_samples_) - time_local_;
    time_local_ = 0;
    feat_buf_.Reset();

    partial_cnt_++;
  }
  return true;
}

bool SttStream::Finish() {
  auto logger = REMOTE_LOGGER();
  if (buffer_.size()) {
    StepFrame((int)(buffer_.size()/2), (int16_t *)buffer_.data(), frames_);
    buffer_.clear();
  }

  // flush internal buffer (static + zero padding -> dynamic feat)
  int ret = stepFrameLFrontEnd(front(), 0, NULL, &out_len_, output_);
  logger->trace("final step ret = {}, outlen = {}", ret, out_len_);
  int mfcc_size = p()->mfcc_size_;
  logger->trace("out_len_ = {}", out_len_);
  int nf = out_len_ / mfcc_size;
  if (nf < kResetPeriod) {
    for (int k = 0; k < nf; k++) {
      logger->trace("final: frames {}", frames_ + k);
      stepSARecFrameExt(laser(), time_local_ + k,
                        p()->feature_dim_, output_ + (k * mfcc_size));
    }
    frames_ += nf;
    time_local_ += nf;

    logger->debug(" result ({} frames )", frames_);

    PartialBackTracking(time_local_, partial_cnt_);
    // t_local = 0;
    // cnt_local++;

    reallocSLaser(laser());
    resetSLaser(laser());
  } else {
    //
    // global CMS condition
    //
    logger->trace("length of frame = {}", nf);
    start_offset_ = (last_offset_ / frame_samples_) - time_local_;
    time_local_ = 0;
    partial_cnt_ = 0;
    int k = 0;
    for (k = 0; k < nf; k++) {
      stepSARecFrameExt(laser(), time_local_, p()->feature_dim_,
                        output_ + (k * mfcc_size));
      logger->trace("frames {}", frames_ + k);

      time_local_++;
      if (time_local_ > next_reset_time_) {
        logger->trace(" result ( {} frames )", k);

        if (!PartialBackTracking(time_local_, partial_cnt_))
          continue;
        if (is_real_time_) {
          next_reset_time_ += kRealtimePeriod;
        } else {
          // 한번 더 부른다??
          // PartialBackTracking(time_local_, fni_, partial_cnt_);
          start_offset_ = (last_offset_ / frame_samples_) - time_local_;
          time_local_ = 0;
          reallocSLaser(laser());
          resetSLaser(laser());
        }
        partial_cnt_++;
      }
    }
    logger->trace(" result ( {} frames )", k);
    PartialBackTracking(time_local_, partial_cnt_);
    // time_local_ = 0;
    // partial_cnt_++;
  }
  return true;
}

bool SttStream::PartialBackTracking(int t_local, int cnt_local) {
  char *result_p_org = NULL;
  char *result_p = NULL;

  auto logger = REMOTE_LOGGER();

  result_p_org = getWBAdjustedResultSLaser(laser(), t_local, 1, 1);

  // no append silence, 2016.03.16
#if 0
  for (int k = 0; k < 10; k++) {
    stepSARecFrameExt(laser_, t_local + k,
                      p()->feature_dim_,
                      p()->silence_ + k * p()->mfcc_size_);
  }
  t_local += 10;
  result_p = getWBAdjustedResultSLaser(laser(), t_local, 1, 1);
#endif

  string result_out;
  if (result_p == NULL && result_p_org == NULL) {
    // 2016.02.24 yghwang
    logger->error("CHILD = {}, no result block {}", seq(), cnt_local);
    if (!is_real_time_) return false;
    mlf_partials_.push_back("");
  } else if (result_p == NULL && result_p_org != NULL) {
    logger->debug("getWBAdjustedResultSLaser: [{}]", EuckrToUtf8(result_p_org));
    // 2016.03.04 dmkang add org data remain
    mlf_partials_.emplace_back(result_p_org);
    result_out = PostProcessResultExt(result_p_org);
    if (p()->lang_ == maum::common::LangCode::eng && g_var.use_process_sentence_boundary) {
      result_out = process_sentence_boundary(result_out);
    }
  } else {
    mlf_partials_.emplace_back(result_p);
    result_out = PostProcessResultExt(result_p);
    if (p()->lang_ == maum::common::LangCode::eng && g_var.use_process_sentence_boundary) {
      result_out = process_sentence_boundary(result_out);
    }
  }
  // REMOTE_LOGGER()->debug("partial mlf [{}]", mlf);

  char *result_out_t = new char[result_out.size() * 2];
  SPLPostProc((char *) result_out.c_str(), result_out_t);
  logger->debug("SPLPostProc");
  std::string res;
  if (p()->lang_ == maum::common::LangCode::eng) {
    res = result_out_t;
  } else {
    res = EuckrToUtf8(result_out_t);
  }
  delete[] result_out_t;
  logger->debug("RESULT:(UTF8) {}", res);
  results_.push_back(res);

  if (result_p_org) {
    free(result_p_org);
  }
  return true;
}

bool SttStream::PartialBackTrackingInterim(int t_local, int cnt_local) {
  char *result_p = NULL;
  auto logger = REMOTE_LOGGER();
  string result_out;

  result_p = getWBAdjustedResultSLaser(laser(), t_local, 1, 1);

  if (result_p == NULL) {
    logger->error("CHILD = {}, no result block {}", seq(), cnt_local);
    return false;
  }

  interim_mlf_partials_.emplace_back(result_p);
  result_out = PostProcessResultExt(result_p, true);
  if (p()->lang_ == maum::common::LangCode::eng) {
    result_out = process_sentence_boundary(result_out);
  }
  
  char *result_out_t = new char[result_out.size() * 2];
  SPLPostProc((char *) result_out.c_str(), result_out_t);

  std::string res;
  if (p()->lang_ == maum::common::LangCode::eng) {
    res = result_out_t;
  } else {
    res = EuckrToUtf8(result_out_t);
  }
  delete[] result_out_t;
  logger->debug("INTERIM RESULT:(UTF8) {}", res);
  interim_results_.push_back(res);

  if (result_p) {
    free(result_p);
  }
  return true;
}

void SttStream::ResetStream(unsigned int &t_dec) {
  //t_dec = 0;
  //time_local_ = t_dec;

  // 아래 라인을 실행할 경우 여러번 수행시 vad 또는 epd 되는 단위가 바뀐다
  //resetLFrontEnd(front());
  reallocSLaser(laser());
  resetSLaser(laser());
  feat_buf_.Reset();
}

string SttStream::PostProcessResultExt(const char *in, bool is_interim) {
  string sent;
  // string sent_tmp;

  size_t len;
  if (!in)
    len = 0;
  else
    len = strlen(in);

  int cc = 0;
  int lcnt = 0;
  char token[1024];
  token[0] = '\0';
  int prv = -1;
  int word_cnt = 0;

  MasterLabelFragment fragment;
  for (decltype(len) ii = 0; ii < len; ii++) {
    if (prv == 1 && (in[ii] == ' ' || in[ii] == '\n')) {
      token[cc] = '\0';
      if (lcnt == 2) {
        if (p()->lang_ == maum::common::LangCode::eng) {
          fragment.set_txt(token);
        } else {
          fragment.set_txt(EuckrToUtf8(token));
        }
        if (strcmp(token, "<s>") == 0 ||
            strcmp(token, "</s>") == 0 ||
            strcmp(token, "<eps>") == 0) { ;
        } else if (strcmp(token, "#") == 0) { ;
        } else {
          char *pword = token;
          if (token[0] == '#') {
            pword++;
          }
          if (sent.size())
            sent += ' ';
          sent += pword;
          // sent_tmp += pword;
          word_cnt++;
        }
      } else if (lcnt == 0) {
        if (token[0] == '.')
          break;
        try {
          fragment.set_start(stoi(token));
        } catch (const std::invalid_argument &ia) {
          REMOTE_LOGGER()->debug("invalid start token {}", token);
          fragment.set_start(-1);
        }

      } else if (lcnt == 1) {
        try {
          fragment.set_end(stoi(token));
        } catch (const std::invalid_argument &ia) {
          REMOTE_LOGGER()->debug("invalid end token {}", token);
          fragment.set_end(-1);
        }
      } else if (lcnt == 3) {
        try {
          fragment.set_likelihood(stof(token));
        } catch (const std::invalid_argument &ia) {
          fragment.set_likelihood(0);
          REMOTE_LOGGER()->debug("invalid likelihood token {}", token);
        }
      }

      cc = 0;
      prv = 0;
      lcnt++;

      if (in[ii] == '\n') {
        lcnt = 0;
        REMOTE_LOGGER()->trace("mlf fragement {} {} {} {}",
                        fragment.start(), fragment.end(),
                        fragment.txt(), fragment.likelihood());
        if (!is_interim) {
          fragments_.push_back(fragment);
        } else {
          interim_fragments_.push_back(fragment);
        }
      }
    } else if (in[ii] != ' ') {
      token[cc] = in[ii];
      cc++;
      prv = 1;
    } else if (in[ii] == ' ') {
      prv = cc = 0;
    }
  }

  return sent;
}

vector<Segment> SttStream::GetSegments(bool is_interim) {
  auto &c = libmaum::Config::Instance();
  auto path = c.Get("brain-stt.spl.temp.dir");
  mkpath(path);

  auto logger = REMOTE_LOGGER();
  char in_name[PATH_MAX];
  snprintf(in_name, sizeof in_name, "%s/mlf_in_XXXXXX",
           path.c_str());

  segment_lock_.lock();
  // 파일을 생성한다.
  int fd = mkstemp(in_name);
  auto mlf = GetRawMlf(is_interim);
  if (write(fd, mlf.c_str(), mlf.size()) < 0) {
    logger->error("Failed to Write() MLF (reason: {})", strerror(errno));
  }
  close(fd);
  logger->trace("temp file name is {}", in_name);

  char out_name[PATH_MAX];
  snprintf(out_name, sizeof out_name, "%s/seg_out_XXXXXX",
           path.c_str());
  int fd2 = mkstemp(in_name);
  close(fd2);

  SPLPostProcSplitLongPauseMLF(in_name, out_name, pause_ms_/10);

  // 읽어들이면 OK....
  ifstream ifs(out_name);
  string line;
  vector<Segment> ret;
  while (std::getline(ifs, line)) {
    //
    //
    // ts= 0, te= 1197, 이십사원이요 은행 은행으로 돼있어요
    auto pos_s = line.find("ts= ", 0);
    if (pos_s == string::npos)
      continue;
    auto pos_e = line.find(", te= ", pos_s + 4);
    if (pos_e == string::npos)
      continue;
    auto pos_txt = line.find(", ", pos_e + 6);
    if (pos_txt == string::npos)
      continue;
    string start = line.substr(pos_s + 4, pos_e - (pos_s + 4));
    string end = line.substr(pos_e + 6, pos_txt - (pos_e + 6));
    string txt = line.substr(pos_txt + 2);
    Segment segment;
    segment.set_start(std::stoi(start) + start_offset_);
    segment.set_end(std::stoi(end) + start_offset_);
    if (p()->lang_ == maum::common::LangCode::eng) {
      segment.set_txt(txt);
    } else {
      segment.set_txt(EuckrToUtf8(txt)); // 영어일 경우에는 그냥 ??
    }
    ret.emplace_back(segment);
  }
  ifs.close();

  unlink(in_name);
  unlink(out_name);
  segment_lock_.unlock();

  if (is_interim) {
    interim_mlf_partials_.clear();
    interim_fragments_.clear();
    interim_results_.clear();
  } else {
    mlf_partials_.clear();
    fragments_.clear();
    results_.clear();
  }
  return ret;
}

vector<string> SttStream::GetStreamingResult(vector<MasterLabelFragment> *fragments, bool is_final) {
  // For StreamingRecognize

  if (is_final) {
    if (fragments) {
      for (auto it = fragments_.begin(); it != fragments_.end(); it++) {
        it->set_start(it->start() + start_offset_);
        it->set_end(it->end() + start_offset_);
        // REMOTE_LOGGER()->debug("{}, {}, {}", frag.start(), frag.end(), frag.txt());
      }
      (*fragments) = std::move(fragments_);
    }

    mlf_partials_.clear();
    fragments_.clear();
    return std::move(results_);

  } else {
    if (fragments) {
      for (auto it = interim_fragments_.begin(); it != interim_fragments_.end(); it++) {
        it->set_start(it->start() + start_offset_);
        it->set_end(it->end() + start_offset_);
        // REMOTE_LOGGER()->debug("{}, {}, {}", frag.start(), frag.end(), frag.txt());
      }
      (*fragments) = std::move(interim_fragments_);
    }

    interim_mlf_partials_.clear();
    interim_fragments_.clear();
    return std::move(interim_results_);
  }
}
