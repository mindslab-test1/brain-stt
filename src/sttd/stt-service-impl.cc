#include <thread>
#include <libmaum/common/config.h>
#include "stt-service-impl.h"
#include "stt-real-impl.h"
#include "sttd-variable.h"

using maum::brain::stt::Speech;
using maum::brain::stt::Text;
using grpc::Status;
using grpc::ServerContext;
using grpc::ServerReaderWriter;
using grpc::ServerReader;
using maum::common::LangCode;
using std::string;
using grpc::ClientContext;

using namespace maum::brain;

class CancelTimer {
 public:
  CancelTimer(int seconds) {
    done_ = false;
    timeout_ = seconds;
  }

  ~CancelTimer() {
    if (thrd_.joinable()) {
      thrd_.join();
    }
    LOGGER()->trace("grpc stream read() timer is destructed");
  }

  void Start(ServerContext *ctx) {
    thrd_ = std::thread(&CancelTimer::Run, this, ctx);
  }

  void Refresh() {
    cv_.notify_one();
  }

  void Stop() {
    LOGGER()->trace("grpc stream read() stop()");
    {
        std::lock_guard<std::mutex> lk(lock_);
        done_ = true;
    }
    cv_.notify_one();
  }

 private:
  void Run(ServerContext *ctx) {
    LOGGER()->trace("grpc stream read() start");
    std::unique_lock<std::mutex> lk(lock_);
    while (!done_) {
      if (cv_.wait_for(lk, std::chrono::seconds(timeout_)) == std::cv_status::timeout) {
        // timeout 처리
        ctx->TryCancel();
        LOGGER()->error("grpc stream read() timeout");
        break;
      } else {
        if (done_) {
          LOGGER()->debug("grpc stream read() timer stopped");
        }
      }
    }
  }

  bool done_;
  int  timeout_;
  std::mutex lock_;
  std::condition_variable cv_;
  std::thread thrd_;
};

SttServiceImpl::SttServiceImpl() {
  resolver_ = SttModelResolver::GetInstance();
}

SttServiceImpl::~SttServiceImpl() {
}

void SttServiceImpl::GetMetaData(ServerContext *ctx, Model &model,
                                 int32_t &sec, int32_t &epd_timeout, string &need_interim) {
  const auto &client_map = ctx->client_metadata();
  auto item_l = client_map.find("in.lang");
  if (item_l == client_map.end()) {
    model.set_lang(LangCode::kor);
  } else {
    string lang_name(item_l->second.begin(), item_l->second.end());
    LangCode lang;
    if (!maum::common::LangCode_Parse(lang_name, &lang)) {
      LOGGER()->warn("invalid lang {}", lang_name);
      lang = LangCode::kor;
    }
    model.set_lang(lang);
  }
  auto item_q = client_map.find("in.samplerate");
  if (item_q == client_map.end()) {
    model.set_sample_rate(8000);
  } else {
    model.set_sample_rate(std::stoi(string(item_q->second.begin(),
                                           item_q->second.end())));
  }

  auto item_m = client_map.find("in.model");
  if (item_m == client_map.end()) {
    model.set_model("default");
  } else {
    model.set_model(string(item_m->second.begin(), item_m->second.end()));
  }

  auto item_s = client_map.find("in.seconds");
  if (item_s == client_map.end()) {
    sec = 300;
  } else {
    sec = std::stoi(string(item_s->second.begin(), item_s->second.end()));
  }

  auto tmp = client_map.find("in.epd_timeout");
  if (tmp == client_map.end()) {
    epd_timeout = g_var.max_epd_time;
  } else {
    epd_timeout = std::stoi(string(tmp->second.begin(), tmp->second.end()));
  }

  tmp = client_map.find("in.need.interim");
  if (tmp == client_map.end()) {
    need_interim = "false";
  } else {
    need_interim = string(tmp->second.begin(), tmp->second.end());
  }
}


Status SttServiceImpl::SimpleRecognize(
    ServerContext *ctx,
    ServerReader<Speech> *reader,
    Text *text) {
  Model model;
  int32_t sec, epd_timeout;
  string need_interim;
  GetMetaData(ctx, model, sec, epd_timeout, need_interim);
  ServerStatus ss;
  Status st = resolver_->Find(&model, &ss);
  if (ss.invoked_by() == "find&fork") {
    sleep(3);
  }

  if (st.ok()) {
    std::unique_ptr<SttRealService::Stub> stub =
        SttRealService::NewStub(
            grpc::CreateChannel(ss.server_address(),
                                grpc::InsecureChannelCredentials()));
    ClientContext client_context;
    client_context.AddMetadata("in.seconds", std::to_string(sec));
    Text tmp_text;

    auto writer = stub->SimpleRecognize(&client_context, &tmp_text);
    Speech speech;
    while (reader->Read(&speech)) {
      writer->Write(speech);
    }
    writer->WritesDone();
    Status wr_st = writer->Finish();
    if (wr_st.ok()) {
      text->set_txt(tmp_text.txt());
    }
    return Status::OK;
  } else {
    return Status(grpc::StatusCode::NOT_FOUND, "No such model");
  }
}

Status SttServiceImpl::DetailRecognize(ServerContext *ctx,
                                       ServerReader<Speech> *reader,
                                       DetailText *text) {
  Model model;
  int32_t sec, epd_timeout;
  string need_interim;
  GetMetaData(ctx, model, sec, epd_timeout, need_interim);
  ServerStatus ss;
  Status st = resolver_->Find(&model, &ss);
  // TODO, make with ping
  if (ss.invoked_by() == "find&fork") {
    sleep(3);
  }

  if (st.ok()) {
    std::unique_ptr<SttRealService::Stub> stub =
        SttRealService::NewStub(
            grpc::CreateChannel(ss.server_address(),
                                grpc::InsecureChannelCredentials()));
    ClientContext client_context;
    client_context.AddMetadata("in.seconds", std::to_string(sec));
    DetailText tmp_text;

    auto writer = stub->DetailRecognize(&client_context, &tmp_text);
    Speech speech;
    while (reader->Read(&speech)) {
      writer->Write(speech);
    }
    writer->WritesDone();
    Status wr_st = writer->Finish();
    if (wr_st.ok()) {
      text->CopyFrom(tmp_text);
    }
    return Status::OK;
  } else {
    LOGGER()->warn("error message : {}", st.error_message());
    return Status(grpc::StatusCode::NOT_FOUND, "No such model");
  }
}

void SttServiceImpl::ReadRealStream(
    ClientReaderWriter<Speech, Segment>* sub_stream,
    ServerReaderWriter<Segment, Speech>* stream) {
  Segment segment;
  while (sub_stream->Read(&segment)) {
    stream->Write(segment);
  }
}

void SttServiceImpl::ReadRealStream2(
    ClientReaderWriter<Speech, DetailSegment>* sub_stream,
    ServerReaderWriter<DetailSegment, Speech>* stream) {
  DetailSegment segment;
  while (sub_stream->Read(&segment)) {
    stream->Write(segment);
  }
}

void SttServiceImpl::ReadRealStreaming(
    ClientReaderWriter<StreamingRecognizeRequest, StreamingRecognizeResponse>* sub_stream,
    ServerReaderWriter<StreamingRecognizeResponse, StreamingRecognizeRequest>* stream) {
  StreamingRecognizeResponse resp;
  while (sub_stream->Read(&resp)) {
    stream->Write(resp);
  }
}

Status SttServiceImpl::StreamRecognize(
    ServerContext *ctx,
    ServerReaderWriter<Segment, Speech> *stream) {
  Model model;
  int sec, epd_timeout;
  string need_interim;
  GetMetaData(ctx, model, sec, epd_timeout, need_interim);

  ServerStatus ss;
  Status st = resolver_->Find(&model, &ss);
  // TODO, make with ping
  if (ss.invoked_by() == "find&fork") {
    sleep(3);
  }

  if (st.ok()) {
    std::unique_ptr<SttRealService::Stub> stub =
        SttRealService::NewStub(
            grpc::CreateChannel(ss.server_address(),
                                grpc::InsecureChannelCredentials()));
    ClientContext client_context;
    client_context.AddMetadata("in.seconds", std::to_string(sec));
    client_context.AddMetadata("in.epd_timeout", std::to_string(epd_timeout));
    client_context.AddMetadata("in.need.interim", need_interim);

    auto real_stream = stub->StreamRecognize(&client_context);
    Speech speech;
    std::thread result_thrd(&SttServiceImpl::ReadRealStream, this,
                            real_stream.get(), stream);

    CancelTimer timer(g_var.grpc_recv_timeout);
    timer.Start(ctx);
    while (stream->Read(&speech)) {
      timer.Refresh();
      real_stream->Write(speech);
    }
    real_stream->WritesDone();
    result_thrd.join();
    Status wr_st = real_stream->Finish();

    timer.Stop();
    if (ctx->IsCancelled()) {
      return  Status::CANCELLED;
    }
    return Status::OK;
  } else {
    LOGGER()->warn("error message : {}", st.error_message());
    return Status(grpc::StatusCode::NOT_FOUND, "No such model");
  }
}

Status SttServiceImpl::DetailStreamRecognize(
    ServerContext *ctx,
    ServerReaderWriter<DetailSegment, Speech> *stream)
{
  Model model;
  int sec, epd_timeout;
  string need_interim;
  GetMetaData(ctx, model, sec, epd_timeout, need_interim);

  ServerStatus ss;
  Status st = resolver_->Find(&model, &ss);
  // TODO, make with ping
  if (ss.invoked_by() == "find&fork") {
    sleep(3);
  }

  if (st.ok()) {
    std::unique_ptr<SttRealService::Stub> stub =
        SttRealService::NewStub(
            grpc::CreateChannel(ss.server_address(),
                                grpc::InsecureChannelCredentials()));
    ClientContext client_context;
    client_context.AddMetadata("in.seconds", std::to_string(sec));
    client_context.AddMetadata("in.epd_timeout", std::to_string(epd_timeout));
    client_context.AddMetadata("in.need.interim", need_interim);

    auto real_stream = stub->DetailStreamRecognize(&client_context);
    Speech speech;
    std::thread result_thrd(&SttServiceImpl::ReadRealStream2, this,
                            real_stream.get(), stream);

    CancelTimer timer(g_var.grpc_recv_timeout);
    timer.Start(ctx);
    while (stream->Read(&speech)) {
      timer.Refresh();
      real_stream->Write(speech);
    }
    real_stream->WritesDone();
    result_thrd.join();
    Status wr_st = real_stream->Finish();

    timer.Stop();
    if (ctx->IsCancelled()) {
      return  Status::CANCELLED;
    }
    return Status::OK;
  } else {
    LOGGER()->warn("error message : {}", st.error_message());
    return Status(grpc::StatusCode::NOT_FOUND, "No such model");
  }
}

bool SttServiceImpl::GetModelData(RWStream* stream, Model &model) {
  StreamingRecognizeRequest request;
  stream->Read(&request);
  StreamingRecognizeRequest::StreamingRequestCase oneof = request.streaming_request_case();

  if (oneof == StreamingRecognizeRequest::kSpeechParam) {
    stt::SpeechRecognitionParam param = request.speech_param();
    model.set_sample_rate(param.sample_rate());
    if (param.lang() == maum::common::ko_KR) {
      model.set_lang(maum::common::kor);
    } else {
      model.set_lang(maum::common::eng);
    }
    model.set_model(param.model());
    return true;
  } else if (oneof == StreamingRecognizeRequest::kAudioContent) {
    LOGGER()->warn("Need Model information first");
  }
  
  return false;
}

Status SttServiceImpl::StreamingRecognize(ServerContext *ctx, RWStream *stream) {

  // UNUSED Parameter
  (void)ctx;
  
  Model model;
  int sec = 300;
  if (GetModelData(stream, model) == false) {
    return Status(grpc::StatusCode::FAILED_PRECONDITION, "Need Model Information First");
  }

  ServerStatus ss;
  Status st = resolver_->Find(&model, &ss);
  // TODO, make with ping
  if (ss.invoked_by() == "find&fork") {
    sleep(3);
  }

  if (st.ok()) {
    std::unique_ptr<SttRealService::Stub> stub =
        SttRealService::NewStub(
            grpc::CreateChannel(ss.server_address(),
                                grpc::InsecureChannelCredentials()));
    ClientContext client_context;
    client_context.AddMetadata("in.seconds", std::to_string(sec));

    auto real_stream = stub->StreamingRecognize(&client_context);
    std::thread result_thrd(&SttServiceImpl::ReadRealStreaming, this,
                            real_stream.get(), stream);

    StreamingRecognizeRequest request;
    while (stream->Read(&request)) {
      real_stream->Write(request);
    }
    real_stream->WritesDone();
    result_thrd.join();
    Status wr_st = real_stream->Finish();
    return Status::OK;
  } else {
    LOGGER()->warn("error message : {}", st.error_message());
    return Status(grpc::StatusCode::NOT_FOUND, "No such model");
  }
}
