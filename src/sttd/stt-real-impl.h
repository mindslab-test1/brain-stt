#ifndef _TA_CL_KOR_CLASSIFIER_H
#define _TA_CL_KOR_CLASSIFIER_H

#include <memory>
#include <string>
#include <vector>
#include <mutex>
#include <maum/brain/stt/stt.grpc.pb.h>
#include "stt-master.h"
#include "stt-history.h"

using std::string;

using grpc::ServerContext;
using grpc::Status;
using grpc::ServerReaderWriter;
using ::grpc::ServerReader;


using maum::brain::stt::SttRealService;
using maum::brain::stt::Model;
using maum::brain::stt::ServerStatus;
using maum::common::LangCode;
using ::maum::brain::stt::Speech;
using ::maum::brain::stt::Text;
using ::maum::brain::stt::Segment;
using ::maum::brain::stt::DetailText;
using ::maum::brain::stt::DetailSegment;
using ::maum::brain::stt::Model;
using ::maum::brain::stt::StreamingRecognizeRequest;
using ::maum::brain::stt::StreamingRecognizeResponse;

using RWStream = ServerReaderWriter<StreamingRecognizeResponse, StreamingRecognizeRequest>;

class SttRealServiceImpl : public SttRealService::Service {
 public:
  explicit SttRealServiceImpl(const string &model, LangCode lang,
                              int32_t sample_rate,
                              const string &endpoint,
                              int gpu_idx);
  ~SttRealServiceImpl() override;

  // 서버가 살아있고, 자기가 그 서버인지를 반환한다.
  Status Ping(ServerContext *context,
              const Model *m, ServerStatus *s) override;
  Status SimpleRecognize(ServerContext *context,
                         ServerReader<::maum::brain::stt::Speech> *reader,
                         Text *response) override;
  Status DetailRecognize(ServerContext* context,
                         ServerReader<Speech>* reader,
                         DetailText *response) override;
  Status StreamRecognize(ServerContext *context,
                         ServerReaderWriter<Segment, Speech> *stream) override;
  Status DetailStreamRecognize(ServerContext *context,
                               ServerReaderWriter<DetailSegment, Speech> *stream) override;
  Status StreamingRecognize(ServerContext *context,
                            RWStream *stream) override;

 private:
  SttMaster *get_stt();
  void MakeResponse(SttStream *stt_str, StreamingRecognizeResponse &resp, bool is_final);
  void SendStreamResultRaw(SttStream *stt_str, ServerReaderWriter<Segment, Speech> *stream);
  void SendStreamResult(SttStream *stt_str, ServerReaderWriter<Segment, Speech> *stream);
  void SendStreamResult2(SttStream *stt_str, ServerReaderWriter<DetailSegment, Speech> *stream);
  
  void SendInterimResult(SttStream *stt_str, ServerReaderWriter<Segment, Speech> *stream);

  // AS SERVER
  std::string model_;
  LangCode lang_;
  int32_t sample_rate_;
  std::string endpoint_;
  std::vector<unique_ptr<SttMaster> > stt_list_;
  size_t laser_idx_;
  std::mutex index_lock_;

  std::unique_ptr<SttHistory> stt_history_;
  std::atomic<int64_t> history_id_;
};

#endif // _TA_CL_KOR_CLASSIFIER_H
