#include "sttd-variable.h"
#include <memory>
#include "remote-logger.h"
#include "libmaum/common/config.h"

#define SEND_LOG_SIZE 1024

RemoteLogger::RemoteLogger() :
  is_done_(false) {
}

RemoteLogger::~RemoteLogger() {
  if (thrd_.joinable()) {
    int log_level = -1;
    Connect("this");
    zmq_send(publisher_, &log_level, sizeof(int), 0);
    thrd_.join();
  }
}

void RemoteLogger::Connect(std::string name) {
  int rc;
  name_ = name;
  pub_ctx_ = zmq_ctx_new();
  publisher_ = zmq_socket(pub_ctx_, ZMQ_PUSH);

  // Connect Address
  auto &c = libmaum::Config::Instance();
  string ipc_path = "ipc://" + c.Get("run.dir") + "/sttlog.zmq";

  // Connect
  if ( (rc = zmq_connect(publisher_, ipc_path.c_str())) != 0) {
    LOGGER()->error("zmq bind error");
  }
}

void RemoteLogger::Start() {
  int rc;
  sub_ctx_ = zmq_ctx_new();
  subscriber_ = zmq_socket(sub_ctx_, ZMQ_PULL);

  // Bind Address
  auto &c = libmaum::Config::Instance();
  string ipc_path = "ipc://" + c.Get("run.dir") + "/sttlog.zmq";

  // Bind
  if ( (rc = zmq_bind(subscriber_, ipc_path.c_str())) != 0) {
    LOGGER()->error("zmq bind error");
  }

  // Do not filter anything
  // zmq_setsockopt(subscriber_, ZMQ_SUBSCRIBE, "", 0);

  thrd_ = std::thread(&RemoteLogger::Run, this);
}

void RemoteLogger::Run() {
  int rc;
  int more;
  size_t size = sizeof(int);
  int log_level;

  LOGGER()->info("Start RemoteLogger...");
  while (is_done_ == false) {
    // Recv Log Level
    zmq_recv(subscriber_, &log_level, sizeof(int), 0);

    if (log_level == -1)
      break;

    // Recv Logger Name
    char name[256];
    rc = zmq_recv(subscriber_, name, sizeof(name), 0);
    name[rc] = 0x00;

    auto child_logger = spdlog::get(name);
    if (!child_logger) {
      LOGGER()->info("Make Child Logger [Name: {}]", name);
      child_logger = std::make_shared<spdlog::logger>(name, LOGGER()->sinks()[0]);
      child_logger->set_level(spdlog::level::from_str(g_var.log_stt_sub_level));
      child_logger->flush_on(spdlog::level::debug);
      child_logger->set_pattern("[%m-%d %H:%M:%S.%e] %n[%P %t] %L: %v");
      spdlog::register_logger(child_logger);
    }

    // Recv Log Message
    std::string result;
    zmq_getsockopt(subscriber_, ZMQ_RCVMORE, &more, &size);

    while (more) {
      char buf[SEND_LOG_SIZE];
      int rc = zmq_recv(subscriber_, buf, SEND_LOG_SIZE, 0);
      zmq_getsockopt(subscriber_, ZMQ_RCVMORE, &more, &size);
      result.append(buf, rc);
    }

    child_logger->log((spdlog::level::level_enum)log_level, result);
  }
  LOGGER()->info("Stop RemoteLogger...");
}
