#ifndef STT_MASTER_H
#define STT_MASTER_H

#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include "frontend_api.h"
#include "Laser.h"
#include <fstream>
#include <atomic>
#include <mutex>
#include <memory>
#include <iostream>
#include <stdio.h>
#include <libmaum/common/encoding.h>
#include <maum/common/lang.pb.h>
#include <maum/brain/stt/stt.pb.h>
#include "feat-buffer.h"

using std::string;
using std::vector;
using std::unordered_map;
using std::ifstream;
using std::ofstream;
using std::shared_ptr;
using std::unique_ptr;
using maum::common::LangCode;
using maum::brain::stt::MasterLabelFragment;
using maum::brain::stt::Segment;

// const int kFeatureDim = 53;
const int kMaxMiniBatch = 1024;
// const int kNBEST = 1;

class SttMaster;
class SttSlave;

class SttStream;

using SttStreamPtr = shared_ptr<SttStream>;

/**
 * 특정 음성인식 학습 모델에 기반하여 관련 데이터를 초기화하도록 한다.
 *
 * 내부적으로 Stream 요청에 맞춰서 적절한 개수의 `SttSlave` 객체를 유지하도록 한다.
 * 필요한 설정파일, 각종 옵션에 대한 설정을 유지한다.
 *
 * GPU 개수에 맞게, masterLaser를 유지하고 활성화 한다.
 *
 * 궁극적으로 `CreateStream()` 함수를 호출하면서 음성인식 실행 단위를 반환한다.
 * GetStream()의 과정에서 비어있는 SttSlave 슬롯을 채워나가고 이들 중에서
 * 사용중이지 않은 Slave를 발견하여 새로운 SttSlave가 동작할 수 있도록 처리한다.
 */
class SttMaster {
 public:
  SttMaster(const string &path, LangCode lang,
            int32_t sample_rate, int gpu_idx);
  virtual ~SttMaster();
#if 0
  bool IsValid() {
    return laser_initialized_ && spl_proc_created_;
  }
#endif
  SttStream* CreateStream(int32_t second = 300, int32_t pause_ms = 0, bool do_epd = false, int32_t epd_timeout = 12000 /* 0.01초 */);
  bool LoadConfig();

  /**
   * @brief ETRI 디코더에서 DNN-VAD 사용 여부
   *
   */
  bool use_vad;

  int vad_reset_period;
  int vad_min_reset_period;

  /**
   * @brief ETRI DNN-VAD 디코더 사용 시 필요 파일
   * epdstatinfo.txt
   *
   */
  std::string epdstat_info;

  std::string vad_cfg;

  /**
   * @brief ETRI 엔진 LSTM 모델 사용 여부
   *
   * LSTM: DNN = 0, UniLSTM = 1, BiLSTM = 2
   *
   */
  int useLSTM;

 private:
  //                         thread core dnn gpucard batch weight
  // gpu.exe '+sys.argv[1]+' 1      1    1   1       128    0.8')
  LangCode  lang_;
  int32_t sample_rate_;
  int core_count_ = 1;
  int gpu_idx_ = 0;
  float prior_weight_ = 0.8;

  /**
   * @brief ETRI 엔진 minibatch 값
   *
   * DNN의 경우 보통 128 or 200, LSTM 일 경우는 200
   */
  int mini_batch_;

  /**
   * @brief ETRI 엔진 nlookahead 값
   *
   * unidirectional LSTM : 0, bidirectional LSTM : 40
   */
  int nlookahead_;

  // 음성인식에 필요한 학습 데이터 및 파일 경로
  string model_dirname_;
  string vam_filename_;
  string fsm_filename_;
  string sym_filename_;
  string dnn_filename_;
  string prior_filename_;
  string norm_filename_;

  // 설정파일 경로
  string laser_config_filename_;

  int read_size_ = 2560; // 32frame
  int mfcc_size_ = 600;
  int feature_dim_;
  bool laser_initialized_ = false;
  bool spl_proc_created_ = false;

  float *silence_ = NULL;
  int silence_len_ = 90;

  // Laser
  vector<Laser *> master_lasers_;
  vector<shared_ptr<SttSlave>> slaves_;
  decltype(slaves_.size()) stream_max_ = 0;
  std::mutex child_lock_;

  int InitLaser();
  int InitSPL();
  void OpenSilenceFile();
  int is_use_gpu();

  friend class SttStream;
  friend class SttSlave;
  friend class Decoder;

  static std::atomic<int32_t> seq_;
  static std::atomic<int32_t> stream_cnt_;
};

#endif /* STT_MASTER_H */
