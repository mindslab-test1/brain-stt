#include <sys/time.h>
#include <grpc++/grpc++.h>
#include <libmaum/common/config.h>
#include "stt-real-impl.h"
#include "stt-model-resolver.h"
#include "util.h"
#include "sent-boundary.h"
#include "stt-stream.h"
#include <cuda_runtime.h>
#include "remote-logger.h"
#include "sttd-variable.h"

using std::string;
using grpc::ServerBuilder;
using grpc::Server;
using grpc::Service;
using std::unique_ptr;
using maum::brain::stt::Model;

using namespace maum::brain::stt;

#define IF_RECORD(X) do { if (g_var.history_record) X } while (0);

/**
 * STT Real 서비스 구현
 *
 * @param model STT 서버 모델
 * @param lang 언어
 * @param sample_rate 샘플링 비율
 * @param endpoint 서버가 대기 중인 주소
 */
SttRealServiceImpl::SttRealServiceImpl(const string &model,
                                       LangCode lang,
                                       int32_t sample_rate,
                                       const string &endpoint,
                                       int gpu_idx)
    : model_(model),
      lang_(lang),
      sample_rate_(sample_rate),
      endpoint_(endpoint),
      laser_idx_(0) {
  Model m;
  m.set_lang(lang);
  m.set_model(model);
  m.set_sample_rate(sample_rate);
  string path = GetModelPath(m);
  REMOTE_LOGGER()->debug("new stt real server path {}", path);

  int cnt = 1;
  if (libmaum::Config::Instance().Get("brain-stt.sttd.use.gpu") != "false") {
    cudaGetDeviceCount(&cnt);
  }

  for (int i = 0; i < cnt; i++) {
    auto stt = std::unique_ptr<SttMaster>(
        new SttMaster(path, lang, sample_rate, i));
    stt_list_.push_back(std::move(stt));
  }

  if (!g_var.history_db.empty()) {
    stt_history_.reset(new SttHistory(g_var.history_db));
  }
  history_id_ = time(NULL) << 32;
}

/**
 * 소멸자
 */
SttRealServiceImpl::~SttRealServiceImpl() {
}

SttMaster *SttRealServiceImpl::get_stt() {
  lock_guard<mutex> guard(index_lock_);
  SttMaster *stt;
  stt = stt_list_[laser_idx_++].get();
  if (laser_idx_ >= stt_list_.size()) {
    laser_idx_ = 0;
  }
  return stt;
}

/**
 * 서버에서 음성을 인식한 이후에 텍스트를 반환한다.
 *
 * @param ctx 컨텍스트
 * @param reader 클라이언트에서 보낸 음성을 읽을 객체
 * @param text 반환할 텍스트
 * @return 정상적으로 처리한 경우에는 Status::OK,
 * 만일 모든 작업 Stream이 꽉 차서 더 이상 새로운 것을 할당할 수 없을 경우에는
 * RESOUCE_EXHAUSTED 를 반환한다.
 * 내부에서 오류가 발생한 경우에는 INTERNAL을 반환한다.
 */
Status SttRealServiceImpl::SimpleRecognize(
    ServerContext *ctx,
    ServerReader<Speech> *reader,
    Text *text) {
  int32_t sec = GetIntValueFromContext(ctx, "in.seconds", 300);
  int32_t seg_ms = GetIntValueFromContext(ctx, "in.unsegment.pause.ms", 1000);
  unique_ptr<SttStream> stt_str(get_stt()->CreateStream(sec, seg_ms));
  Speech speech;
  if (!stt_str) {
    return Status(grpc::StatusCode::RESOURCE_EXHAUSTED, "BUSY");
  }

  while (reader->Read(&speech)) {
    const string &u = speech.bin();
    stt_str->AddData(u);
  }
  if (!stt_str->Finish()) {
    return Status(grpc::StatusCode::INTERNAL,
                  "STT 8K failed.");
  }

  if (lang_ == maum::common::LangCode::eng) {
    // process_sentence_boundary(stt_str->GetResult())
    // text->set_txt(process_sentence_boundary(stt_str->GetResult()));
    text->set_txt(stt_str->GetResult());
  } else
    text->set_txt(stt_str->GetResult());

  return Status::OK;
}

/**
 * 서버에서 음성을 인식한 이후에 텍스트 정보 외에
 * 추가적인 데이터, 즉, MLF 까지 반환한다.
 *
 * @param ctx 컨텍스트
 * @param reader 클라이언트에서 보낸 음성을 읽을 객체
 * @param text MLF를 포함한 상세한 text 정보
 * @return 정상적으로 처리한 경우에는 Status::OK,
 * 만일 모든 작업 Stream이 꽉 차서 더 이상 새로운 것을 할당할 수 없을 경우에는
 * RESOUCE_EXHAUSTED 를 반환한다.
 * 내부에서 오류가 발생한 경우에는 INTERNAL을 반환한다.
 */
Status SttRealServiceImpl::DetailRecognize(ServerContext *ctx,
                                           ServerReader<Speech> *reader,
                                           DetailText *text) {
  Speech speech;
  int32_t sec = GetIntValueFromContext(ctx, "in.seconds", 300);
  int32_t seg_ms = GetIntValueFromContext(ctx, "in.unsegment.pause.ms", 1000);
  unique_ptr<SttStream> stt_str(get_stt()->CreateStream(sec, seg_ms));
  if (!stt_str) {
    return Status(grpc::StatusCode::RESOURCE_EXHAUSTED, "BUSY");
  }

  while (reader->Read(&speech)) {
    const string &u = speech.bin();
    stt_str->AddData(u);
  }
  if (!stt_str->Finish()) {
    return Status(grpc::StatusCode::INTERNAL,
                  "STT 8K failed.");
  }
  if (lang_ == maum::common::LangCode::eng) {
    text->set_txt(process_sentence_boundary(stt_str->GetResult()));
  } else
    text->set_txt(stt_str->GetResult());

  for (auto fr : stt_str->GetFragments()) {
    auto frag = text->add_fragments();
    frag->CopyFrom(fr);
  }
  text->set_raw_mlf(stt_str->GetRawMlf());

  for (auto seg : stt_str->GetSegments()) {
    auto segment = text->add_segments();
    segment->CopyFrom(seg);
  }

  return Status::OK;
}

void SttRealServiceImpl::SendStreamResultRaw(SttStream *stt_str,
                                             ServerReaderWriter<Segment, Speech> *stream) {
  if (!g_var.resp_per_segment) {
    Segment final_segment;
    std::string final_sentence;
    vector<Segment> segment_list = stt_str->GetSegments();
    if (!segment_list.empty()) {
      final_segment.set_start(segment_list.front().start());
      final_segment.set_end(segment_list.back().end());
      for (auto seg : segment_list) {
        if (!final_sentence.empty()) {
          final_sentence += ". ";
        }
        final_sentence += seg.txt();
      }
      final_segment.set_txt(final_sentence);
      stream->Write(final_segment);
    }
  } else {
    for (auto seg : stt_str->GetSegments()) {
      stream->Write(seg);
    }
  }
}

void SttRealServiceImpl::SendStreamResult(SttStream *stt_str,
                                          ServerReaderWriter<Segment, Speech> *stream) {
  // 한국어는 process_sentence_boundary를 사용하지 않음
  if (lang_ != maum::common::LangCode::eng || !g_var.use_process_sentence_boundary) {
      SendStreamResultRaw(stt_str, stream);
      return;
  }
  
  // 영어 결과 후처리
  if (!g_var.resp_per_segment) {  // sentence를 라인별로 나누지 않고 보냄
    for (auto seg : stt_str->GetSegments()) {
      std::string sentence = process_sentence_boundary(Utf8ToEuckr(seg.txt()));
      seg.set_txt(EuckrToUtf8(sentence));
      stream->Write(seg);
    }
  } else { // 영어 결과 후처리 (문장 별 Response)
    // process_sentence_boundary에서 문장이 분리될 경우 사용 할 시간 정보
    auto fragments = stt_str->GetFragments();
    vector<MasterLabelFragment> word_fragments;

    int offset = 0;  // segment에 이미 반영되어 있는 시작 offset을 다시 빼야 한다
    if (!fragments.empty()) {
      offset = fragments[0].end();
    }

    // 단어가 아닌 정보 제외
    for (auto frag : fragments) {
      if (frag.txt().size() > 0 && frag.txt()[0] != '<') {
        word_fragments.emplace_back(frag);
      }
      REMOTE_LOGGER()->trace("frag: {} , {}, {}", frag.start(), frag.txt(), frag.end());
    }

    size_t idx = 0;
    for (auto seg : stt_str->GetSegments()) {
      std::string phrase = process_sentence_boundary(Utf8ToEuckr(seg.txt()));
      for (auto sentence : split(phrase, "\n")) {
        auto words = split(sentence, " ");
        REMOTE_LOGGER()->trace("sentence: [{}], words count is {}", sentence, words.size());
        Segment segment;
        segment.set_start(seg.start() + word_fragments[idx].start() - offset);
        idx += words.size() - 1;
        if (idx > (word_fragments.size() - 1)) {
          idx = word_fragments.size() - 1;
          REMOTE_LOGGER()->error("words count({}) is more than fragments' size({})",
                                 words.size(), word_fragments.size());
        }
        segment.set_end(seg.start() + word_fragments[idx++].end() - offset);
        segment.set_txt(EuckrToUtf8(sentence));
        stream->Write(segment);
      }
    }  // end of for
  }
}

void SttRealServiceImpl::SendStreamResult2(SttStream *stt_str,
                                           ServerReaderWriter<DetailSegment, Speech> *stream) {
  DetailSegment final_segment;
  std::string final_sentence;

  for (auto fr : stt_str->GetFragments()) {
    auto frag = final_segment.add_fragments();
    frag->CopyFrom(fr);
  }

  vector<Segment> segment_list = stt_str->GetSegments();
  if (!segment_list.empty()) {
    final_segment.set_start(segment_list.front().start());
    final_segment.set_end(segment_list.back().end());
    for (auto seg : segment_list) {
      if (!final_sentence.empty()) {
        final_sentence += ". ";
      }
      final_sentence += seg.txt();
    }
    final_segment.set_txt(final_sentence);

    stream->Write(final_segment);
  }
}

void SttRealServiceImpl::SendInterimResult(SttStream *stt_str,
                                           ServerReaderWriter<Segment, Speech> *stream) {
  // 영어 결과 후처리
  if (lang_ == maum::common::LangCode::eng) {
    for (auto seg : stt_str->GetSegments(true)) {
      std::string sentence = process_sentence_boundary(Utf8ToEuckr(seg.txt()));
      seg.set_txt(EuckrToUtf8(sentence));
      stream->Write(seg);
    }
  } else { // 한국어의 경우
    if (!g_var.resp_per_segment) {
      Segment final_segment;
      std::string final_sentence;
      vector<Segment> segment_list = stt_str->GetSegments(true);
      if (!segment_list.empty()) {
        final_segment.set_start(segment_list.front().start());
        final_segment.set_end(segment_list.back().end());
        if (final_segment.start() > final_segment.end())
          return;
        for (auto seg : segment_list) {
          if (!final_sentence.empty()) {
            final_sentence += ". ";
          }
          final_sentence += seg.txt();
        }
        final_segment.set_txt("##" + final_sentence);
        stream->Write(final_segment);
      }
    } else {
      for (auto seg : stt_str->GetSegments(true)) {
        seg.set_txt("##" + seg.txt());
        REMOTE_LOGGER()->debug("seg - {}", seg.txt());
        stream->Write(seg);
      }
    }
  }
}

/**
 * 서버에서 Stream 방식으로 음성을 처리한다.
 *
 * @param ctx 컨텍스트
 * @param stream 입력테스트와 스트림 텍스트를 통해서 처리한다.
 * @return 정상적으로 처리한 경우에는 Status::OK,
 * 만일 모든 작업 Stream이 꽉 차서 더 이상 새로운 것을 할당할 수 없을 경우에는
 * RESOUCE_EXHAUSTED 를 반환한다.
 * 내부에서 오류가 발생한 경우에는 INTERNAL을 반환한다.
 */
Status SttRealServiceImpl::StreamRecognize(
    ServerContext *ctx,
    ServerReaderWriter<Segment, Speech> *stream) {
  // exception이 발생하는 경우 grpc내부에서 catch되므로 발생 사실을 인지하지 못할 수 있다
  Speech speech;
  int32_t sec = GetIntValueFromContext(ctx, "in.seconds", 300);
  int32_t seg_ms = GetIntValueFromContext(ctx, "in.unsegment.pause.ms", 10 * 1000);
  int32_t epd_timeout = GetIntValueFromContext(ctx, "in.epd_timeout", 0);
  // int32_t need_interim = GetIntValueFromContext(ctx, "in.need.interim", 0);
  bool need_interim = false;

  timeval start_tv, stop_tv, end_tv;

  const auto &client_map = ctx->client_metadata();
  auto item = client_map.find("in.need.interim");
  if (item != client_map.end()) {
    need_interim = item->second == "true";
  }

  std::string id = model_ + std::to_string(history_id_++);
  if (stt_history_) {
    gettimeofday(&start_tv, NULL);
    stt_history_->InsertHistory(id, start_tv);
  }

  unique_ptr<SttStream> stt_str(get_stt()->CreateStream(sec, seg_ms, true, epd_timeout));
  if (!stt_str) {
    REMOTE_LOGGER()->debug("RESOURCE_EXHAUSTED");
    if (stt_history_) {
      gettimeofday(&end_tv, NULL);
      stop_tv = end_tv;
      stt_history_->UpdateEndEvent(id, start_tv, stop_tv, end_tv);
    }
    return Status(grpc::StatusCode::RESOURCE_EXHAUSTED, "BUSY");
  }
  stt_str->EnableRealTime();
  stt_str->EnableInterim(need_interim);

  int pcm_fd;
  std::string file_name = id + ".pcm";
  REMOTE_LOGGER()->info("STT Session ID: {}", id);

  IF_RECORD( pcm_fd = open(file_name.c_str(), O_WRONLY | O_CREAT | O_APPEND, 0644); )

  while (stream->Read(&speech)) {
    const string &u = speech.bin();
    IF_RECORD(  write(pcm_fd, u.c_str(), u.length()); )
    stt_str->AddData(u);
    if (stt_str->HasResult()) {
      SendStreamResult(stt_str.get(), stream);
    } else if (need_interim && stt_str->HasInterimResult()) {
      SendInterimResult(stt_str.get(), stream);
    }
  }
  IF_RECORD( close(pcm_fd); )

  if (stt_history_) {
    gettimeofday(&stop_tv, NULL);
    stt_history_->UpdateStopEvent(id, stop_tv);
  }

  if (!stt_str->FinishEPD()) {
    return Status(grpc::StatusCode::INTERNAL,
                  "STT 8K failed.");
  }

  SendStreamResult(stt_str.get(), stream);

  if (stt_history_) {
    gettimeofday(&end_tv, NULL);
    stt_history_->UpdateEndEvent(id, start_tv, stop_tv, end_tv);
  }
  return Status::OK;
}

Status SttRealServiceImpl::DetailStreamRecognize(
    ServerContext *ctx,
    ServerReaderWriter<DetailSegment, Speech> *stream) {
  // exception이 발생하는 경우 grpc내부에서 catch되므로 발생 사실을 인지하지 못할 수 있다
  Speech speech;
  int32_t sec = GetIntValueFromContext(ctx, "in.seconds", 300);
  int32_t seg_ms = GetIntValueFromContext(ctx, "in.unsegment.pause.ms", 10 * 1000);
  int32_t epd_timeout = GetIntValueFromContext(ctx, "in.epd_timeout", 0);
  // int32_t need_interim = GetIntValueFromContext(ctx, "in.need.interim", 0);
  bool need_interim = false;

  timeval start_tv, stop_tv, end_tv;

  const auto &client_map = ctx->client_metadata();
  auto item = client_map.find("in.need.interim");
  if (item != client_map.end()) {
    need_interim = item->second == "true";
  }

  std::string id = model_ + std::to_string(history_id_++);
  if (stt_history_) {
    gettimeofday(&start_tv, NULL);
    stt_history_->InsertHistory(id, start_tv);
  }

  unique_ptr<SttStream> stt_str(get_stt()->CreateStream(sec, seg_ms, true, epd_timeout));
  if (!stt_str) {
    REMOTE_LOGGER()->debug("RESOURCE_EXHAUSTED");
    if (stt_history_) {
      gettimeofday(&end_tv, NULL);
      stop_tv = end_tv;
      stt_history_->UpdateEndEvent(id, start_tv, stop_tv, end_tv);
    }
    return Status(grpc::StatusCode::RESOURCE_EXHAUSTED, "BUSY");
  }
  stt_str->EnableRealTime();
  stt_str->EnableInterim(need_interim);

  while (stream->Read(&speech)) {
    const string &u = speech.bin();
    stt_str->AddData(u);
    if (stt_str->HasResult()) {
      SendStreamResult2(stt_str.get(), stream);
    } else if (need_interim && stt_str->HasInterimResult()) {
      // SendInterimResult(stt_str.get(), stream);
    }
  }

  if (stt_history_) {
    gettimeofday(&stop_tv, NULL);
    stt_history_->UpdateStopEvent(id, stop_tv);
  }

  if (!stt_str->FinishEPD()) {
    return Status(grpc::StatusCode::INTERNAL,
                  "STT 8K failed.");
  }

  SendStreamResult2(stt_str.get(), stream);

  if (stt_history_) {
    gettimeofday(&end_tv, NULL);
    stt_history_->UpdateEndEvent(id, start_tv, stop_tv, end_tv);
  }
  return Status::OK;
}

void SttRealServiceImpl::MakeResponse(SttStream *stt_str,
                                      StreamingRecognizeResponse &resp,
                                      bool is_final) {
  // Temp variable for output
  vector<MasterLabelFragment> fragments;

  for (auto text : stt_str->GetStreamingResult(&fragments, is_final)) {
    if (lang_ == maum::common::LangCode::eng && g_var.use_process_sentence_boundary) {
      text = process_sentence_boundary(text);
    }

    // Set One Result
    ::maum::brain::stt::SpeechRecognitionResult* result = resp.add_results();
    result->set_transcript(text);
    result->set_final(is_final);

    // Set Word from fragment (fragment는 MLF 데이터를 담고 있다)
    for (auto frag : fragments) {
      ::maum::brain::stt::WordInfo* word = result->add_words();
      word->mutable_start_time()->set_seconds(frag.start() / 100);
      word->mutable_start_time()->set_nanos(frag.start() % 100 * 1000000 * 10);
      word->mutable_end_time()->set_seconds(frag.end() / 100);
      word->mutable_end_time()->set_nanos(frag.end() % 100 * 1000000 * 10);
      word->set_word(frag.txt());
    }
    fragments.clear();
  }

  // Set SpeechEventType - SPEECH_EVENT_UNSPECIFIED, END_OF_SINGLE_UTTERANCE

#define ee StreamingRecognizeResponse_SpeechEventType_END_OF_SINGLE_UTTERANCE
#define eu StreamingRecognizeResponse_SpeechEventType_SPEECH_EVENT_UNSPECIFIED

  if (is_final)
    resp.set_speech_event_type(ee);
  else
    resp.set_speech_event_type(eu);
}

Status SttRealServiceImpl::StreamingRecognize(ServerContext *ctx,
                                              RWStream *stream) {
  int32_t sec = GetIntValueFromContext(ctx, "in.seconds", 300);
  int32_t seg_ms = GetIntValueFromContext(ctx, "in.unsegment.pause.ms", 10 * 1000);
  bool need_interim = g_var.partial_result_default;

  unique_ptr<SttStream> stt_str(get_stt()->CreateStream(sec, seg_ms, true));

  if (!stt_str) {
    REMOTE_LOGGER()->debug("RESOURCE_EXHAUSTED");
    return Status(grpc::StatusCode::RESOURCE_EXHAUSTED, "BUSY");
  }
  stt_str->EnableRealTime();
  stt_str->EnableInterim(need_interim);

  StreamingRecognizeRequest request;
  while (stream->Read(&request)) {
    if (request.has_speech_param()) {
      const SpeechRecognitionParam &p = request.speech_param();
      // BRAIN-STT IMPL do not use, SpeechRecognitionParam
      REMOTE_LOGGER()->debug("param {} {} {} contexts.size {}",
                             p.sample_rate(),
                             p.model(),
                             p.lang(),
                             p.speech_contexts_size());
      if (p.sample_rate() != sample_rate_ ||
          p.model() != model_ ||
          (int) p.lang() != (int) lang_) {
        REMOTE_LOGGER()->error("speech param is not matched {}:{}, {}:{}, {}:{}",
                               p.sample_rate(), sample_rate_,
                               p.model(), model_,
                               p.lang(), lang_);
        break;
      }
    } else /*if (request.has_audio_content()) */ {
      const string &u = request.audio_content();
      stt_str->AddData(u);
      if (stt_str->HasResult()) {
        StreamingRecognizeResponse resp;
        MakeResponse(stt_str.get(), resp, true);
        stream->Write(resp);
      } else if (need_interim && stt_str->HasInterimResult()) {
        StreamingRecognizeResponse resp;
        MakeResponse(stt_str.get(), resp, false);
        stream->Write(resp);
      }
    }
  }
  if (!stt_str->FinishEPD()) {
    return Status(grpc::StatusCode::INTERNAL, "STT 8K failed.");
  }

  // 음성 데이터 마지막 남은 부분 처리
  StreamingRecognizeResponse last_resp;
  MakeResponse(stt_str.get(), last_resp, true);
  stream->Write(last_resp);

  return Status::OK;
}

/**
 * 서버가 살아있는지 점검한다.
 *
 * @param context 서버 컨텍스트
 * @param model 서버 접속 기본 정보
 * @param ss 서버 접속 엔드포인트
 * @return 호출 성공 여부
 */
Status SttRealServiceImpl::Ping(ServerContext *context,
                                const Model *model,
                                ServerStatus *ss) {
  if (model->model() == model_ &&
      model->lang() == lang_ &&
      model->sample_rate() == sample_rate_) {
    ss->set_lang(lang_);
    ss->set_model(model_);
    ss->set_sample_rate(sample_rate_);

    ss->set_running(true);
    ss->set_server_address(endpoint_);
    ss->set_invoked_by("pong");
  } else {
    REMOTE_LOGGER()->debug("invalid model and lang called [{} {} {}]",
                    model->model(),
                    model->lang(),
                    model->sample_rate());
  }
  return Status::OK;
}

