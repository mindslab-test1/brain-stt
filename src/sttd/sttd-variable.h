#ifndef STTD_VARIABLE_H
#define STTD_VARIABLE_H

#include <string>
#include <map>

// variable, config value, etc... for this application
struct STTD_VARIABLE {
  /**
   * @brief 초기 STTD_VARIABLE값을 설정하는 함수, g_var값을 사용하기 전에 호출되어야 한다.
   * 
   */
  void Initialize();

  bool Verify();

  /**
   * @brief STTD_VARIABLE값을 로그에 표시
   *
   */
  void PrintConfig();

  void LoadModelConfig();

  std::string log_stt_level;
  std::string log_stt_sub_level;

  std::string export_ip;
  int port;
  int timeout;
  int min_port;
  int max_port;
  std::string state_file;

  int max_stream;
  std::string use_gpu;
  int num_cores;
  int max_epd_time;

  bool resp_per_segment;
  bool use_process_sentence_boundary;
  bool partial_result_default;

  /**
   * @brief frontend_api.h에 정의된 FSTATUS ENUM 값의 문자형 표현
   * 
   */
  std::map<int, std::string> FSTATUS_ENUM_MAP;

  /**
   * @brief ETRI 엔진에서 저장하는 파일 디렉토리
   *
   */
  std::string save_input_dir;

  // GRPC
  int grpc_handshake_timeout_ms;

  /**
   * @brief Maximum time that a channel may exist.
   * Int valued, milliseconds. INT_MAX means unlimited.
   */ 
  int grpc_max_conn_age_ms;

  bool grpc_use_ssl;

  int grpc_recv_timeout;

  /**
   * @brief stt_history 테이블 관련 설정
   *
   */ 
  bool        history_record;
  std::string history_db;
  int         history_rollover_hour;
  int         history_max_days;
};

extern STTD_VARIABLE g_var;

#endif /* STTD_VARIABLE_H */
