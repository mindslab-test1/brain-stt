* openssl CSR fields 설명
Field	Meaning	Example
/C=	Country	GB
/ST=	State	London
/L=	Location	London
/O=	Organization	Global Security
/OU=	Organizational Unit	IT Department
/CN=	Common Name	example.com

C  : ISO 국가 코드 KR, US, CN, JP (대문자)
ST : 시,도
L  : 구,군
O  : 기관명, 회사명
OU : 조직명
CN : 도메인명, 일반이름. IP 주소는 CN 으로 사용할수 없습니다.

위 항목은 모두 영문입력을 해야 합니다. 특수문자를 사용하면 안됩니다.
(예제에 사용된 이름은 예제용이므로 해당 도메인 정보로 지정하시기 바랍니다)

* 인증서 테스트
  - gen_key.sh 를 실행하여 인증서 생성
  - client에 server.crt를 배포

