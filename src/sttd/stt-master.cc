#include <unistd.h>
#include <cuda_runtime_api.h>
#include <libmaum/common/config.h>

#include "ETRIPP.h"

#include "const.h"
#include <libmaum/common/util.h>
#include <libmaum/common/fileutils.h>
#include "sttd-variable.h"
#include "stt-master.h"
#include "stt-stream.h"
#include "remote-logger.h"
#include "cpptoml.h"

using std::unique_ptr;
using std::endl;

extern "C" {
int getDLNetNumInNodeLASER(Laser *pLaser);
int getDLNetKindLASER(Laser *pLaser);
}

int HandleSttError(int err, const char *errmsg) {
  REMOTE_LOGGER()->error("stt error: {} {}", err, errmsg);
  return 1;
}

std::atomic<int32_t> SttMaster::seq_;
std::atomic<int32_t> SttMaster::stream_cnt_;

SttMaster::SttMaster(const string &path,
                     LangCode lang,
                     int32_t sample_rate,
                     int gpu_idx)
    : lang_(lang),
      sample_rate_(sample_rate),
      gpu_idx_(gpu_idx) {
  core_count_ = (int) sysconf(_SC_NPROCESSORS_ONLN);

  const int kGpuCardCount = 1;

  libmaum::Config &c = libmaum::Config::Instance();

  // TODO : change stream_max_
  stream_max_ = c.GetAsInt("brain-stt.sttd.max.stream");
  if (stream_max_ <= 0) {
    stream_max_ = unsigned(core_count_ * kGpuCardCount);
  }

  REMOTE_LOGGER()->debug("cpu cores {}, gpu cards {} stream_max {}",
                  core_count_, kGpuCardCount, stream_max_);

  if (lang == maum::common::LangCode::kor) {
    if (sample_rate == 16000) {
      vam_filename_ = c.Get("brain-stt.resource.dnn-image.kor.vam.16k");
      norm_filename_ = c.Get("brain-stt.resource.dnn-image.kor.norm.16k");
    } else if (path == kBaselineKor8000) {
      vam_filename_ = c.Get("brain-stt.resource.dnn-image.kor.vam.sh");
      norm_filename_ = c.Get("brain-stt.resource.dnn-image.kor.norm.sh");
    } else {
      vam_filename_ = c.Get("brain-stt.resource.dnn-image.kor.vam.lg");
      norm_filename_ = c.Get("brain-stt.resource.dnn-image.kor.norm.lg");
    }
  } else if (lang == maum::common::LangCode::eng) {
    if (sample_rate == 8000) {
      vam_filename_ = c.Get("brain-stt.resource.dnn-image.eng.vam.8k");
      norm_filename_ = c.Get("brain-stt.resource.dnn-image.eng.norm.8k");
    } else {
      vam_filename_ = c.Get("brain-stt.resource.dnn-image.eng.vam.16k");
      norm_filename_ = c.Get("brain-stt.resource.dnn-image.eng.norm.16k");
    }
  } else {
    REMOTE_LOGGER()->error("invalid lang code! {}", maum::common::LangCode_Name(lang));
  }

  auto prefix = c.Get("brain-stt.trained.root");
  prefix += '/';
  prefix += path;
  model_dirname_ = prefix;

  if (LoadConfig() == false) {
    exit(-1);
  }

  REMOTE_LOGGER()->debug("model directory : {}", model_dirname_);
  REMOTE_LOGGER()->debug("path baseline : {} {}", vam_filename_, norm_filename_);
  REMOTE_LOGGER()->debug("path lm adapt : {} {}", fsm_filename_, sym_filename_);
  REMOTE_LOGGER()->debug("path am adapt : {} {}", dnn_filename_, prior_filename_);

  const char *laser_cfg = sample_rate == 8000 ?
                          "brain-stt.config.8k.laser" :
                          "brain-stt.config.16k.laser";

  laser_config_filename_ = c.Get(laser_cfg);
  REMOTE_LOGGER()->debug("path config : {}",
                  laser_config_filename_ );

  // FIXME
  // 32의 의미는 상수 또는 enum으로
  if (sample_rate == 8000)
    read_size_ = 2560; // 32 frames
  if (sample_rate == 16000)
    read_size_ = 3200; // 20 frames

  if (mini_batch_ >= 32) {
    if (sample_rate_ == 8000)
      read_size_ = 80 * mini_batch_;
    else
      read_size_ = 160 * mini_batch_;
  }

  //setLaserErrorHandleProc(NULL, (void *) HandleSttError);

  // ETRI API에 관련 설정을 넣어준다.
  // CPU 개수를 지정해준다.
  int num_cores = c.GetAsInt("brain-stt.sttd.num.cores");
  if (num_cores <= 0) {
    num_cores = core_count_;
  }
  setSLaserLBCores(num_cores);
  REMOTE_LOGGER()->debug("setSLaserLBCores : {}", num_cores);

  InitLaser();
  REMOTE_LOGGER()->debug("InitSPL() is called");
  if (gpu_idx_ == 0)
    InitSPL();
  REMOTE_LOGGER()->debug("OpenSilenceFile() is called");
  OpenSilenceFile();
  REMOTE_LOGGER()->debug("SttMaster::ctor() is ended");
}

bool SttMaster::LoadConfig() {
  auto conf = model_dirname_ + "/model.cfg";

  if (IsFile(conf.c_str(), 0)) {
    auto config = cpptoml::parse_file(conf);

    auto get_path = [&](std::string key) -> std::string {
      std::string file_path = config->get_qualified_as<std::string>(key).value_or("");
      if (file_path.front() != '/') {
        file_path = model_dirname_ + "/" + file_path;
      }
      return file_path;
    };

    vam_filename_   = get_path("stt.vam_file");
    fsm_filename_   = get_path("stt.fsm_file");
    sym_filename_   = get_path("stt.sym_file");
    dnn_filename_   = get_path("stt.dnn_file");
    norm_filename_  = get_path("stt.norm_file");
    prior_filename_ = get_path("stt.pri_file");

    mini_batch_ = config->get_qualified_as<int>("stt.minibatch").value_or(600);
    if (mini_batch_ > kMaxMiniBatch)
      mini_batch_ = kMaxMiniBatch;
    nlookahead_ = config->get_qualified_as<int>("stt.nlookahead").value_or(0);

    // VAD Settings
    use_vad          = config->get_qualified_as<bool>("dnnvad.use").value_or(false);
    epdstat_info     = get_path("dnnvad.info_file");
    vad_cfg          = get_path("dnnvad.cfg_file");
    vad_reset_period = config->get_qualified_as<int>("dnnvad.reset_period").value_or(3000);
    vad_min_reset_period =
        config->get_qualified_as<int>("dnnvad.min_reset_period").value_or(300);

    // segment
    g_var.resp_per_segment = config->get_qualified_as<bool>("stt.resp_per_segment").value_or(true);
    // use_process_sentence_boundary
    g_var.use_process_sentence_boundary =
        config->get_qualified_as<bool>("stt.use_process_sentence_boundary").value_or(false);
    // 중간 결과 생성 여부
    g_var.partial_result_default = config->get_qualified_as<bool>("default.partial_result").value_or(false);

    return true;
  }
  return false;
}

int SttMaster::InitLaser() {
  int ret = 0;
  int failure = 0;
  Laser *laser = createMasterLaserDLNet((char *) vam_filename_.c_str(),
                                      (char *) dnn_filename_.c_str(),
                                      prior_weight_,
                                      (char *) prior_filename_.c_str(),
                                      (char *) norm_filename_.c_str(),
                                      mini_batch_ + nlookahead_,
                                      is_use_gpu(),
                                      gpu_idx_,
                                      (char *) fsm_filename_.c_str(),
                                      (char *) sym_filename_.c_str());
  REMOTE_LOGGER()->debug("createMasterLaserDLNet() is called");
  mfcc_size_ = getDLNetNumInNodeLASER(laser);
  auto useLSTM = getDLNetKindLASER(laser);
  REMOTE_LOGGER()->debug("getDLNetKindLASER() is {} (DNN: 0, UniLSTM: 1, BiLSTM: 2)", useLSTM);
  REMOTE_LOGGER()->debug("splice_win = {}", (mfcc_size_ / 40 - 1) / 2);

  feature_dim_ = mfcc_size_ * (mini_batch_ + nlookahead_);
  REMOTE_LOGGER()->debug("getDLNetNumInNodeLASER() is {}", mfcc_size_);
  if (laser) {
    ret = readSLaserConfig(laser, (char *) laser_config_filename_.c_str());
    // 하나라도 실패하면 실패한 것임.
    if (!ret)
      failure |= (gpu_idx_ << 1);
    master_lasers_.push_back(laser);
  }

  laser_initialized_ = !failure;
  return ret;
}

int SttMaster::InitSPL() {
  libmaum::Config &c = libmaum::Config::Instance();

  REMOTE_LOGGER()->info("brain-stt.spl.postproc.instance = {}",
                        c.GetDefaultAsInt("brain-stt.spl.postproc.instance", "1"));

  // FIXME, unsegment 리소스를 처리하는 과정에 매우 애매한 상태임.
  if (!Lat2cnWordNbestOutInit(c.Get("brain-stt.spl.postproc.tagging").c_str(),
                              c.Get("brain-stt.spl.postproc.chunking").c_str(),
                              c.Get("brain-stt.spl.postproc.userdic").c_str(),
                              c.GetDefaultAsInt("brain-stt.spl.postproc.instance", "1")
                              )) {
    REMOTE_LOGGER()->error("Failure: no unsegment resource...");
    spl_proc_created_ = false;
    return -1;
  }
  spl_proc_created_ = true;
  return 0;
}

SttMaster::~SttMaster() {
  for (auto laser : master_lasers_) {
    freeMasterLaserDLNet(laser);
  }
  closeSPLPostProc();
}

void SttMaster::OpenSilenceFile() {
  auto &c = libmaum::Config::Instance();
  string fname;
  fname = c.Get("brain-stt.silence.dnn.data");
  FILE *fp = fopen(fname.c_str(), "rb");
  if (fp == NULL) {
    REMOTE_LOGGER()->error("Failed to open silence file ({}), errno: {}", fname, strerror(errno));
  }

  /*
    20564  sil_53.data
    240000 sil_dnn.data
  */

  int mfcc_size = mfcc_size_;
  if (mini_batch_ > silence_len_) {
    silence_len_ = kMaxMiniBatch;
    REMOTE_LOGGER()->debug("silence_len = {}", silence_len_);
    silence_ = (float *) malloc(sizeof(float) * (silence_len_ + 100) * mfcc_size);
    fread(silence_, sizeof(float), size_t(mfcc_size * 100), fp);
    fclose(fp);
    // 한번 읽은 데이터를 10번 복제해야 합니다.
    for (int i = 1; i <= 10; i++) {
      memcpy(silence_ + i * (mfcc_size * 100), silence_,
             sizeof(float) * mfcc_size * 100);
    }

  } else {
    silence_ = (float *) malloc(sizeof(float) * silence_len_ * mfcc_size);
    fread(silence_, sizeof(float), size_t(mfcc_size * 90), fp); // 90 이라는 상수의 의미는?
    fclose(fp);
  }
}

SttStream* SttMaster::CreateStream(int32_t second, int32_t pause_ms, bool do_epd, int32_t epd_timeout) {
  int32_t global_seq = ++seq_;
  // std::unique_lock<std::mutex> lock(child_lock_);

  uint32_t stream_cnt = ++stream_cnt_;
  if (stream_cnt > stream_max_) {
    // ALL CHILD STREAM IS BUSY, NO ROOM DO PROCESSING!
    --stream_cnt_;
    return NULL;
  }

  // FIXME: seq 값은 사용하지 않고 있음
  int32_t seq = int32_t(slaves_.size());
  // lock.unlock();
  SttStream *stream = NULL;
  SttSlave *one = new SttSlave(this, sample_rate_, seq, do_epd);
  if (one != NULL) {
    one->model_dirname_ = model_dirname_;
    stream = new SttStream(this, one, global_seq, second, pause_ms, epd_timeout);
  } else {
    REMOTE_LOGGER()->error("Failed to new SttSlave");
  }
  return stream;
}

int SttMaster::is_use_gpu() {
  if (libmaum::Config::Instance().Get("brain-stt.sttd.use.gpu") != "false") {
    return true;
  }
  return false;
}
