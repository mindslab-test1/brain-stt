#ifndef STT_HISTORY_H
#define STT_HISTORY_H

#include <string>
#include <sqlite3.h>
#include <time.h>
#include <thread>
#include <condition_variable>
#include <mutex>

// CREATE TABLE "stt_history" (
// 	`id`	TEXT NOT NULL,
// 	`start_date`	date,
// 	`start_time`	datetime,
// 	`stop_time`	datetime,
// 	`finish_time`	datetime,
// 	PRIMARY KEY(id)
// );
// CREATE INDEX stt_history_idx ON stt_history (start_date);

class SttHistory {
 public:
  SttHistory(std::string file_path);
  ~SttHistory();

  bool InsertHistory(std::string id, timeval start_tv = {0, 0});
  bool UpdateStopEvent(std::string id, timeval stop_tv);
  bool UpdateEndEvent(std::string id, timeval start_tv, timeval stop_tv, timeval end_tv);

  void RollOver();
  void DeleteHistory(int max_days);
  void Start(int hour, int max_days);

 private:

  void reopen();
  void create_table();
  void show_rollover_time(std::chrono::system_clock::time_point when);

  static int callback(void *userData, int argc, char **argv, char **azColName);

  std::mutex lock_;
  sqlite3 *db_;
  std::string db_path_;
  std::condition_variable cv_;
  std::thread thrd_;
  bool is_done_;
};

#endif /* STT_HISTORY_H */
