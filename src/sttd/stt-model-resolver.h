#ifndef STT_MODEL_RESOLVER_H
#define STT_MODEL_RESOLVER_H

#include <atomic>
#include <queue>
#include <grpc++/grpc++.h>
#include <json/json.h>
#include <atomic>
#include <maum/brain/stt/stt.grpc.pb.h>

using std::string;
using std::atomic;

using grpc::Status;
using grpc::ServerContext;
using google::protobuf::Empty;
using maum::brain::stt::Model;
using maum::brain::stt::ModelList;
using maum::brain::stt::ServerStatus;
using maum::brain::stt::ServerStatusList;
using maum::brain::stt::SetModelResponse;
using grpc::ServerReader;


class SttModelResolver {
  SttModelResolver();
 public:
  static SttModelResolver *GetInstance();
  virtual ~SttModelResolver();
  Status Find(const Model* request, ServerStatus* status);
  Status GetModels(ModelList *models);
  Status GetServers(ServerStatusList *servers);
  Status Stop(const Model* model, ServerStatus* status);
  Status Restart(const Model* model, ServerStatus* status);
  Status Ping(const Model* model, ServerStatus * status);
  Status SetModel(const Model *model,
                  ServerReader<::maum::common::FilePart> *reader,
                  SetModelResponse * resp);
  Status DeleteModel(const Model *model, ServerStatus *status);
  int ClearModel(pid_t pid);

 private:
  string model_root_;
  Json::Value root_;
  std::mutex root_lock_;
  string export_ip_;
  std::mutex port_lock_;
  std::queue<int> port_list_;
  string state_file_;
  int gpu_count_ = 1;
  std::atomic<int> fork_count_;

  void WriteStatus();
  void LoadStatus();
  enum ForkResult {
    FORK_SUCCESS = 0,
    FORK_NOT_FOUND = 1,
    FORK_FAILED = 2
  };

  bool PingServer(const Json::Value &item);
  ForkResult ForkServer(const Model *model, Json::Value &item);
  bool IsRunning(const Model &m, Json::Value &item);
  bool Stop(const Model &m, const Json::Value &item);

  // singleton
  static std::mutex mutex_;
  static atomic<SttModelResolver *> instance_;
};

void RunSttChild(const string &name, const string &lang, int32_t sample_rate,
                 const string &endpoint, int port);

class SttModelResolverImpl final :
    public maum::brain::stt::SttModelResolver::Service {
 public:
  SttModelResolverImpl() {
    resolver_ = SttModelResolver::GetInstance();
  }
  virtual ~SttModelResolverImpl() {
  }
  const char *Name() {
    return "STT Model resolver";
  }

  Status Find(ServerContext* context,
              const Model* model, ServerStatus* status) override {
    return resolver_->Find(model, status);
  }

  Status GetModels(ServerContext* context,
                   const Empty* request, ModelList *models) override {
    return resolver_->GetModels(models);
  }
  Status GetServers(ServerContext* context,
                    const Empty* empty, ServerStatusList *list) override  {
    return resolver_->GetServers(list);
  }
  Status Stop(ServerContext* context,
              const Model* model, ServerStatus* status) override {
    return resolver_->Stop(model, status);
  }
  Status Restart(ServerContext* context,
                 const Model* model, ServerStatus* status) override {
    return resolver_->Restart(model, status);
  }
  Status Ping(ServerContext* context,
              const Model* model, ServerStatus * status) override {
    return resolver_->Ping(model, status);
  }
  Status SetModel(ServerContext* context,
                  ServerReader<::maum::common::FilePart> *reader,
                  SetModelResponse* response);
  Status DeleteModel(ServerContext* context,
                     const Model* model, ServerStatus* status) {
    return resolver_->DeleteModel(model, status);
  }

 private:
  SttModelResolver * resolver_;
};

#endif // STT_MODEL_RESOLVER_H
