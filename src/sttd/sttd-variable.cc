#include <arpa/inet.h>
#include "sttd-variable.h"
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>

STTD_VARIABLE g_var;

void STTD_VARIABLE::Initialize() {
  auto &c = libmaum::Config::Instance();

  // log config
  log_stt_level     = c.GetDefault("logs.level.brain-sttd", "debug");
  log_stt_sub_level = c.GetDefault("logs.level.brain-stt-subd", "debug");

  // server config
  export_ip    = c.Get("brain-stt.sttd.front.export.ip");
  port         = c.GetAsInt("brain-stt.sttd.front.port");
  timeout      = c.GetAsInt("brain-stt.sttd.front.timeout");
  min_port     = c.GetAsInt("brain-stt.sttd.child.min.port");
  max_port     = c.GetAsInt("brain-stt.sttd.child.max.port");
  state_file   = c.Get("brain-stt.sttd.state.file");

  // for stt config
  max_stream   = c.GetAsInt("brain-stt.sttd.max.stream");
  use_gpu      = c.Get("brain-stt.sttd.use.gpu");
  num_cores    = c.GetAsInt("brain-stt.sttd.num.cores");

  max_epd_time = c.GetAsInt("brain-stt.sttd.max.epd.time");
  save_input_dir = c.GetDefault("brain-stt.sttd.save.input.dir", "");

  resp_per_segment = c.GetDefault("brain-stt.sttd.resp.per.sentence", "true") == "true";
  use_process_sentence_boundary = false;  // default
  // for grpc
  grpc_handshake_timeout_ms = c.GetDefaultAsInt("brain-stt.sttd.handshake.timeout", "120000");
  grpc_max_conn_age_ms      = c.GetDefaultAsInt("brain-stt.sttd.max.connection.age", "2147483647");
  grpc_use_ssl              = c.GetDefault("brain-stt.sttd.grpc.use.ssl", "false") == "true";
  grpc_recv_timeout         = c.GetDefaultAsInt("brain-stt.sttd.grpc.recv.timeout", "86400");

  // sqlite path
  history_record        = c.GetDefault("brain-stt.sttd.history.record", "false") == "true";
  history_db            = c.GetDefault("brain-stt.sttd.history.path", "");
  history_rollover_hour = c.GetDefaultAsInt("brain-stt.sttd.history.rollover.hour", "3");
  history_max_days      = c.GetDefaultAsInt("brain-stt.sttd.history.max.days", "7");

  FSTATUS_ENUM_MAP[0x00] = "noise    ";
  FSTATUS_ENUM_MAP[0x01] = "detecting";
  FSTATUS_ENUM_MAP[0x02] = "detected ";
  FSTATUS_ENUM_MAP[0x04] = "reset    ";
  FSTATUS_ENUM_MAP[0x08] = "onset    ";
  FSTATUS_ENUM_MAP[0x10] = "offset   ";
}

bool STTD_VARIABLE::Verify() {
  // Verify
  struct sockaddr_in sa;
  if (inet_pton(AF_INET, export_ip.c_str(), &(sa.sin_addr)) != 1) {
    LOGGER()->error("INVALID brain-stt.sttd.front.export.ip = {}", export_ip);
    exit(-1);
  }

  if ((port > 1024 && port < 65536) == false) {
    LOGGER()->error("INVALID brain-stt.sttd.front.port = {}", port);
    exit(-1);
  }
  if ((min_port > 1024 && min_port < 65536) == false) {
    LOGGER()->error("INVALID brain-stt.sttd.child.min.port = {}", min_port);
    exit(-1);
  }
  if ((max_port > 1024 && max_port < 65536) == false) {
    LOGGER()->error("INVALID brain-stt.sttd.child.max.port = {}", max_port);
    exit(-1);
  }
  if (min_port > max_port) {
    LOGGER()->error("child.min.port is more than child.max.port");
    exit(-1);
  }

  return true;
}

void STTD_VARIABLE::PrintConfig() {
  LOGGER()->info("brain-stt.sttd.front.export.ip = {}", export_ip);
  LOGGER()->info("brain-stt.sttd.front.port = {}", port);

  LOGGER()->info("brain-stt.sttd.front.timeout = {}", timeout);
  LOGGER()->debug("brain-stt.sttd.max.connection.age = {}", grpc_max_conn_age_ms);
  LOGGER()->debug("brain-stt.sttd.handshake.timeout = {}", grpc_handshake_timeout_ms);

  LOGGER()->info("brain-stt.sttd.child.min.port = {}", min_port);
  LOGGER()->info("brain-stt.sttd.child.max.port = {}", max_port);
  LOGGER()->info("brain-stt.sttd.state.file = {}", state_file);

  LOGGER()->info("brain-stt.sttd.max.stream = {}", max_stream);
  LOGGER()->info("brain-stt.sttd.use.gpu = {}", use_gpu);
  LOGGER()->info("brain-stt.sttd.num.cores = {}", num_cores);
  LOGGER()->info("brain-stt.sttd.max.epd.time = {}", max_epd_time);

  LOGGER()->debug("brain-stt.sttd.save.input.dir = {}", save_input_dir);
  LOGGER()->debug("brain-stt.sttd.grpc.use.ssl = {}", grpc_use_ssl);

  LOGGER()->debug("brain-stt.sttd.history.record        = {}", history_record);
  LOGGER()->debug("brain-stt.sttd.history.path          = {}", history_db);
  LOGGER()->debug("brain-stt.sttd.history.rollover.hour = {}", history_rollover_hour);
  LOGGER()->debug("brain-stt.sttd.history.max.days      = {}", history_max_days);
}
