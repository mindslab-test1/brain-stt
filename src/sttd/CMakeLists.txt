cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_MODULE_PATH "${CMAKE_INSTALL_PREFIX}/share/cmake/modules")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

project(brain-sttd)
set(BINOUT brain-sttd)
set(BINOUT_SUB brain-stt-subd)


include(import)
include(gitversion/cmake)

set(STT_API_DIR     "${CMAKE_CURRENT_SOURCE_DIR}/import_modules/etri-stt-engine-191017")
set(STT_API_TGZ     "${STT_API_DIR}.tar.gz")
set(STT_API_INCLUDE "${STT_API_DIR}/include")
set(STT_API_LIB     "${STT_API_DIR}/lib")

find_package(Protobuf REQUIRED)
find_package(CUDA REQUIRED)
find_package(OpenMP)
if (OPENMP_FOUND)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif ()

add_custom_target(stt-engine ALL
  DEPENDS ${STT_API_DIR}
)
add_custom_command(OUTPUT ${STT_API_DIR}
  COMMAND tar xfzm ${STT_API_TGZ}
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/import_modules
  DEPENDS ${STT_API_TGZ}
  COMMENT "Unpacking ${STT_API_TGZ}"
  VERBATIM
)

include_directories(../../proto)
include_directories(${PROTOBUF_INCLUDE_DIRS})
include_directories(${STT_API_INCLUDE})
include_directories(${CUDA_INCLUDE_DIRS})
include_directories(/usr/include/atlas)

link_directories(${STT_API_LIB})
link_directories(${CUDA_TOOLKIT_ROOT_DIR}/lib64)
link_directories(/usr/lib64/atlas)

set(CMAKE_CXX_STANDARD 11)
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -ggdb")
endif ()

### common

set(SOURCE_FILES
  sttd-variable.cc
  main.cc
  remote-logger.cc
  stt-service-impl.cc
  stt-model-resolver.cc
  util.cc
  const.c
  feat-buffer.cc
  stt-history.cc
)

set(SUB_SOURCE_FILES
  sttd-variable.cc
  main-sub.cc
  remote-logger.cc
  stt-real-impl.cc
  stt-master.cc
  stt-slave.cc
  stt-stream.cc
  util.cc
  const.c
  feat-buffer.cc
  decoder.cc
  vad-decoder.cc
  stt-history.cc
)


set(LIB_STT_GPU dnn.gpu)

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 5.0)
    SET(ESENT_LIB esent5)
  else()
    ## if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 5.0)
    SET(ESENT_LIB esent)
  endif()
else()
  message(WARNING "You are using an unsupported compiler! Compilation has only been tested with GCC.")
endif()


set(LIB_STT_3
    ${STT_API_LIB}/english_sent_split.a
    ${STT_API_LIB}/libEverIsam.a
    )

set(LIB_STT_1
    dnnvad
    asearch
    msearch
    base
    laserdlnet
    esdlapi
    esdl.gpu
    esdl4userdef
    frontend
    splproc
    )
set(LIB_STT_2
    pthread
#    rt
#    splproc_rule
    )

if(EXISTS /etc/redhat-release)
  set(LIB_STT_ATLAS
      lapack
      satlas
      stdc++
      )
else()
  set(LIB_STT_ATLAS
      lapack
      atlas
      lapack_atlas
      stdc++
      )
endif()


### sttd
include_directories("${CMAKE_INSTALL_PREFIX}/include")
link_directories("${CMAKE_INSTALL_PREFIX}/lib")

add_executable(${BINOUT} ${SOURCE_FILES} ${STT_API_MADE})
add_executable(${BINOUT_SUB} ${SUB_SOURCE_FILES} ${STT_API_MADE})
add_dependencies(${BINOUT} stt-engine)
add_dependencies(${BINOUT_SUB} stt-engine)
target_git_version_init(${BINOUT})

set(LIB_STT_CUDA libcudart_static.a libcublas_static.a libculibos.a)

target_link_libraries(${BINOUT}
    maum-common
    maum-json
    brain-stt-pb
    maum-pb
    grpc++ grpc protobuf
    libcudart_static.a
    rt
    dl
    archive
    zmq
    sqlite3
    )

target_link_libraries(${BINOUT_SUB}
    ${LIB_STT_2} ${LIB_STT_3}
    ${LIB_STT_ATLAS}
    m
    pthread
    dl
    ${LIB_STT_CUDA}
    maum-common
    maum-json
    brain-stt-pb
    maum-pb
    grpc++ grpc protobuf
    zmq
    ${LIB_STT_1}
    rt
    sqlite3
    )

### install

install(TARGETS ${BINOUT} RUNTIME DESTINATION bin)
install(TARGETS ${BINOUT_SUB} RUNTIME DESTINATION bin)
install(FILES
    resources/dnn-vad/epdstatinfo.8k
    resources/dnn-vad/epdstatinfo.16k
    DESTINATION etc/stt/config
    COMPONENT config
)
install(FILES model.cfg.sample
    DESTINATION etc/stt/config
    COMPONENT config
)
