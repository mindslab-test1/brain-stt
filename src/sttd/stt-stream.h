#ifndef STT_STREAM_H
#define STT_STREAM_H

#include <mutex>
#include "stt-master.h"
#include "stt-slave.h"
#include "decoder.h"
#include "vad-decoder.h"

/**
 * 반복된 STT 음성 데이터에서 텍스트를 떨구는 음성인식을 실행한다.
 *
 * 반복된 음성 데이터를 처리하는 동안에 임시적인 데이터들을 유지한다.
 * 내부적으로 2개의 global 객체에 대한 참조를 유지한다.
 *
 * `parent_`는 글로벌 객체로서 모든 실행 옵션에 대한 정보를 유지한다.
 * `slave_`는 각기 쓰레드 단위 또는 CPU, GPU 옵션별로 설정된 정보을 유지한다.
 */
class SttStream {
 public:
  SttStream(SttMaster *master, SttSlave *slave, int32_t seq,
            int32_t second = 300, int32_t unseg_ms = 0, int32_t epd_timeout = 0);
  virtual ~SttStream();
  bool AddData(const string &data);
  bool Finish();
  bool FinishEPD();
  vector<Segment> GetSegments(bool is_interim = false);
  vector<string> GetStreamingResult(vector<MasterLabelFragment> *fragments = nullptr, bool is_final = true);
  string GetResult();
  bool HasResult();
  string GetInterimResult();
  bool HasInterimResult();
  vector<MasterLabelFragment> GetFragments() {
    return fragments_;
  }
  string GetRawMlf(bool is_interim = false);

  void EnableRealTime();
  void EnableInterim(bool on);
  void ResetStream(unsigned int &t_dec);

 private:
  void StepFrame(size_t n_read, int16_t * buf, int &t);
  void StepFrameEPD(size_t n_read, int16_t * buf, int &t);
  void StepFeatFrame(int &t, int min_frame_count);
  bool PartialBackTracking(int t_local, int cnt_local);
  
  /**
   * @brief 최종 인식 결과 생성전에 현재까지 인식된 결과 생성
   * 
   * @param t_local int
   * @param cnt_local int
   * @return bool
   */
  bool PartialBackTrackingInterim(int t_local, int cnt_local);

  string PostProcessResultExt(const char *in, bool is_interim = false);

  SttMaster *p() {
    return parent_;
  }
  SttSlave *s() {
    return slave_.get();
  }
  LFrontEnd *front() {
    return slave_->front_;
  }
  Laser *laser() {
    return slave_->laser_;
  }
  int seq() {
    return slave_->seq_;
  }

  // FOR STREAM ITERATION
  int32_t frames_ = 0;
  int32_t out_len_ = 0;
  int32_t time_local_ = 0;
  int32_t start_offset_ = -1;
  int32_t last_offset_ = 0;
  int32_t next_reset_time_;
  int32_t partial_cnt_ = 0;
  string buffer_;

  vector<string> mlf_partials_;
  vector<MasterLabelFragment> fragments_;
  vector<string> results_;

  size_t fragments_offset_ = 0;
  size_t segments_offset_ = 0;
  float *output_ = nullptr;

  SttMaster *parent_;
  unique_ptr<SttSlave> slave_;
  int32_t str_seq_;
  int32_t second_;
  int32_t pause_ms_;
  int32_t frame_samples_;

  int max_epd_time_;

  bool is_real_time_ = false;
  bool is_reset_laser_ = false;
  bool is_epd_ = false;
  
  
  /**
   * @brief 중간 결과를 리턴하기 위한 설정들...
   * 
   */
  bool need_interim_result_ = true;
  int32_t interim_time_local_ = 0;
  vector<string> interim_results_;
  vector<string> interim_mlf_partials_;
  vector<MasterLabelFragment> interim_fragments_;

  FeatBuffer feat_buf_;
  std::unique_ptr<Decoder> decoder_;

  static std::mutex segment_lock_;

  friend class SttSlave;
  friend class SttMaster;
  friend class Decoder;
  friend class VadDecoder;
};

#endif /* STT_STREAM_H */
