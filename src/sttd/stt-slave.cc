#include "sttd-variable.h"
#include <libmaum/common/config.h>

#include "stt-slave.h"
#include "remote-logger.h"
#include "Laser.h"
#include "frontend_api.h"
#include "ETRIPP.h"

extern "C" APITYPE void getSLaserConfig( Laser *a_laserP, char *, char * );

SttSlave::SttSlave(SttMaster *conv, int sample_rate, int seq, bool do_epd)
    : parent_(conv),
      seq_(seq),
      sample_rate_(sample_rate),
      do_epd_(do_epd) {
  REMOTE_LOGGER()->debug("do_epd is {}", do_epd_);
  LoadFrontEnd();

  REMOTE_LOGGER()->debug(R"(read_size: {}
  use gpu : {}
  miniBatch: {}
  prior weight: {}
  pb silence len: {}
  FEATURE SIZE: {}
  read dim: {}
  miniBatch: {}
  feature size: {})",
                  p()->read_size_,
                  p()->is_use_gpu(),
                  p()->mini_batch_,
                  p()->prior_weight_,
                  p()->silence_len_,
                  p()->mfcc_size_,
                  p()->feature_dim_,
                  p()->mini_batch_,
                  p()->feature_dim_);
}

SttSlave::~SttSlave() {
  if (laser_) {
    freeChildLaserDLNet(laser_);
    laser_ = nullptr;
  }
  if (front_) {
    closeLFrontEnd(front_);
  }
}

void SttSlave::LoadFrontEnd() {
  auto &c = libmaum::Config::Instance();
  const string &dnn_config =
      do_epd_ ? "brain-stt.config.frontend.dnn.epd" :
      sample_rate_ == 8000 ? "brain-stt.config.frontend.dnn.8k" : "brain-stt.config.frontend.dnn.16k";

  if (sample_rate_ == 8000)
    front_ = createLFrontEndExt(FRONTEND_OPTION_8KHZFRONTEND | FRONTEND_OPTION_DNNFBFRONTEND);
  else
    front_ = createLFrontEndExt(FRONTEND_OPTION_DNNFBFRONTEND);

  auto frontend_config_filename = c.Get(dnn_config);

  readOptionLFrontEnd(front_, (char *) frontend_config_filename.c_str());
  setOptionLFrontEnd(front_,
                     (char *) "CMS_LEN_BLOCK",
                     (char *) "0"); // set global CMS

  // VAD 버전에서는 EPD를 0으로 해야 정상동작
  if (p()->use_vad) {
    setOptionLFrontEnd(front_, (char *)"FRONTEND_OPTION_DOEPD", (char *)"0");
  }

  char res[64];
  if (!getOptionLFrontEnd(front_, (char *) "EPD_LEN_FRONTMARGIN", res)) {
    epd_len_front_margin_ = std::stoi(res);
    REMOTE_LOGGER()->debug("epd_len_front_margin = {}", epd_len_front_margin_);
  }

  // print all option to stdout
  getAllOptionsLFrontEnd(front_);

  int num_gpu = int(seq_ % p()->master_lasers_.size());
  Laser *master = p()->master_lasers_[num_gpu];
  REMOTE_LOGGER()->info("using master {}", num_gpu);
  laser_ = createChildLaserDLNet(master,
                               (char *) p()->vam_filename_.c_str(),
                               (char *) p()->dnn_filename_.c_str(),
                               p()->prior_weight_,
                               (char *) p()->prior_filename_.c_str(),
                               (char *) p()->norm_filename_.c_str(),
                               p()->mini_batch_ + p()->nlookahead_,
                               p()->is_use_gpu(),
                               p()->gpu_idx_,
                               (char *) p()->fsm_filename_.c_str(),
                               (char *) p()->sym_filename_.c_str());

  {
    auto logger = REMOTE_LOGGER();
    getSLaserConfig(laser_, (char *) "SPARAM_FORCED_OUTPUT", res);
    logger->debug(" Forced Output: {}", res);
    getSLaserConfig(laser_, (char *) "SPARAM_LM_WEIGHT", res);
    logger->debug(" LM weight: {}", res);

    logger->debug(" Forced Output: {}", res);

    logger->debug("FEATURE SIZE = {}, read dim = {},"
                  " mini-batch = {}, READ_SIZE = {}",
                  p()->mfcc_size_,
                  p()->feature_dim_,
                  p()->mini_batch_,
                  p()->read_size_);

  }
}

void SttSlave::ReloadOption(bool do_epd) {
  do_epd_ = do_epd;
  auto &c = libmaum::Config::Instance();
  const string &dnn_config =
      do_epd_ ? "brain-stt.config.frontend.dnn.epd" :
      sample_rate_ == 8000 ? "brain-stt.config.frontend.dnn.8k" : "brain-stt.config.frontend.dnn.16k";

  if (sample_rate_ == 8000)
    front_ = createLFrontEndExt(FRONTEND_OPTION_8KHZFRONTEND | FRONTEND_OPTION_DNNFBFRONTEND);
  else
    front_ = createLFrontEndExt(FRONTEND_OPTION_DNNFBFRONTEND);

  auto frontend_config_filename = c.Get(dnn_config);

  readOptionLFrontEnd(front_, (char *) frontend_config_filename.c_str());
  setOptionLFrontEnd(front_,
                     (char *) "CMS_LEN_BLOCK",
                     (char *) "0"); // set global CMS

  if (p()->use_vad) {
    setOptionLFrontEnd(front_, (char *)"FRONTEND_OPTION_DOEPD", (char *)"0");
  }

  char res[64];
  if (!getOptionLFrontEnd(front_, (char *) "EPD_LEN_FRONTMARGIN", res)) {
    epd_len_front_margin_ = std::stoi(res);
    REMOTE_LOGGER()->debug("epd_len_front_margin = {}", epd_len_front_margin_);
  }

  // print all option to stdout
  getAllOptionsLFrontEnd(front_);
}

void SttSlave::Reset(int32_t seq) {
  REMOTE_LOGGER()->debug("now reset, start new job {}, SEQ: {}", seq, seq_);
  resetSLaser(laser_);
  resetLFrontEnd(front_);
  int featdim;
  if ( getIntValueLFrontEnd( front_, (char *)"FRONTEND_OUTDIM", &featdim ) == 0)
    REMOTE_LOGGER()->debug("featdim is {}", featdim);
  busy_ = true;
}

void SttSlave::Clear() {
  REMOTE_LOGGER()->debug("now clear SEQ: {}", seq_);
  busy_ = false;
}
