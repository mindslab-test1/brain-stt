#ifndef STTD_STT_SERVICE_H
#define STTD_STT_SERVICE_H

#include <grpc++/grpc++.h>
#include <maum/brain/stt/stt.grpc.pb.h>
#include "stt-model-resolver.h"

using maum::brain::stt::SpeechToTextService;
using ::grpc::ServerContext;
using ::grpc::ServerReader;
using ::grpc::Status;
using ::grpc::ServerReaderWriter;
using ::grpc::ClientReaderWriter;
using ::maum::brain::stt::Speech;
using ::maum::brain::stt::Text;
using ::maum::brain::stt::Segment;
using ::maum::brain::stt::DetailText;
using ::maum::brain::stt::DetailSegment;
using ::maum::brain::stt::Model;
using ::maum::brain::stt::StreamingRecognizeRequest;
using ::maum::brain::stt::StreamingRecognizeResponse;

using RWStream = ServerReaderWriter<StreamingRecognizeResponse, StreamingRecognizeRequest>;

class SttServiceImpl final: public SpeechToTextService::Service {
 public:
  SttServiceImpl();
  virtual ~SttServiceImpl();
 public:
  Status SimpleRecognize(ServerContext *context,
                         ServerReader<::maum::brain::stt::Speech> *reader,
                         Text *response) override;
  Status DetailRecognize(ServerContext* context,
                         ServerReader<Speech>* reader,
                         DetailText* response) override;
  Status StreamRecognize(ServerContext *context,
                         ServerReaderWriter<Segment, Speech> *stream) override;
  Status DetailStreamRecognize(ServerContext *context,
                               ServerReaderWriter<DetailSegment, Speech> *stream) override;
  Status StreamingRecognize(ServerContext *context, RWStream *stream) override;

  const char *name() {
    return "Stt Front Service";
  }
 private:
  SttModelResolver * resolver_;
  void GetMetaData(ServerContext *ctx, Model &model, int32_t &sec, int32_t &epd_timeout, string &need_interim);
  bool GetModelData(RWStream* stream, Model &model);
  void ReadRealStream(ClientReaderWriter<Speech, Segment>* sub_stream,
                      ServerReaderWriter<Segment, Speech>* stream);
  void ReadRealStream2(ClientReaderWriter<Speech, DetailSegment>* sub_stream,
                       ServerReaderWriter<DetailSegment, Speech>* stream);
  void ReadRealStreaming(ClientReaderWriter<StreamingRecognizeRequest, StreamingRecognizeResponse>* sub_stream,
                         ServerReaderWriter<StreamingRecognizeResponse, StreamingRecognizeRequest>* stream);
};

#endif
