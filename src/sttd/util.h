#ifndef __STT_UTIL_H
#define __STT_UTIL_H

#include <string>
#include <vector>
#include <grpc++/grpc++.h>
#include <maum/brain/stt/stt.pb.h>

using grpc::ServerContext;
using maum::brain::stt::Model;

void GetModelFromContext(const ServerContext *ctx, Model &model);
int32_t GetSecondFromContext(const ServerContext *ctx);
int32_t GetIntValueFromContext(const ServerContext *ctx, const std::string &key, int32_t def_val);
std::string GetFilenameFromContext(const ServerContext *ctx);

/**
 * 모델 이름과 언어, 샘플 비트를 합쳐서 처리해서 결과를 반환한다.
 *
 * `model`-`lang`-`16000`
 *
 * 언어모델의 디렉토리는 "model-kor-16000", "model-eng-8000" 형태로 생성된다.
 * 참고로 lang의 이름은 ISO-639를 따라서 사용된다.
 * 같은 모델 이름으로 한국어 영어가 동시에 서비스되는 경우가 존재할 수 있다고
 * 가정한다.
 *
* @param model 모델 정보
* @return 모델 경로
 */
inline std::string GetModelPath(const Model &model) {
  std::string model_path = model.model();
  model_path += '-';
  model_path += maum::common::LangCode_Name(model.lang());
  model_path += '-';
  model_path += std::to_string(model.sample_rate());
  return model_path;
}

std::vector<std::string> split(const std::string &str, const std::string &delim);

#endif // __STT_UTIL_H
