#ifndef DECODER_H
#define DECODER_H

#include <stdio.h>
#include <string>

#include "feat-buffer.h"
#include "Laser.h"

class SttStream;

class Decoder {
 public:
  // Constructor & Destructor
  Decoder(SttStream *stream);
  virtual ~Decoder();

  // virtual bool Initialize(SttStream *stream);
  virtual bool Initialize();
  virtual void StepFeatFrame(int &t, int min_frame_count) = 0;
  virtual void StepFeatFrameLast(int &t, int min_frame_count) = 0;

  // From SttStream
  bool PartialBackTracking(int t_dec);

  int SampleRate;
  std::string ModelDname;

 protected:
  SttStream *stream_;
  FeatBuffer *feat_buf_;

  // ETRI 소스 비교를 위하여 Naming Rule은 무시하고 원본 소스를 그대로 사용
  Laser *pLaser;
  unsigned int reset_period;
  unsigned int featdim;
  unsigned int nminibatch;
};

#endif /* DECODER_H */
