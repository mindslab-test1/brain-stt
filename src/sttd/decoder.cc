#include "decoder.h"
#include "stt-stream.h"

Decoder::Decoder(SttStream *stream) {
  stream_ = stream;
  feat_buf_ = &stream_->feat_buf_;

  pLaser = stream_->laser();

  featdim = 600;
  nminibatch = stream_->p()->mini_batch_;
}

Decoder::~Decoder() {
}

bool Decoder::Initialize() {
  return true;
}

bool Decoder::PartialBackTracking(int t_dec) {
  return stream_->PartialBackTracking(t_dec, stream_->partial_cnt_);
}
