#include "util.h"
#include <libmaum/common/config.h>
#include <libmaum/common/base64.h>
#include "remote-logger.h"

using grpc::ServerContext;
using maum::brain::stt::Model;
using maum::common::LangCode;
using std::string;

void GetModelFromContext(const ServerContext * ctx, Model &model) {
  const auto &client_map = ctx->client_metadata();
  auto item_l = client_map.find("in.lang");
  if (item_l == client_map.end()) {
    model.set_lang(LangCode::kor);
  } else {
    string lang_name(item_l->second.begin(), item_l->second.end());
    LangCode lang;
    if (!maum::common::LangCode_Parse(lang_name, &lang)) {
      REMOTE_LOGGER()->warn("invalid lang {}", lang_name);
      lang = LangCode::kor;
    }
    model.set_lang(lang);
  }
  auto item_q = client_map.find("in.samplerate");
  if (item_q == client_map.end()) {
    model.set_sample_rate(8000);
  } else {
    model.set_sample_rate(std::stoi(string(item_q->second.begin(),
                                           item_q->second.end())));
  }

  auto item_m = client_map.find("in.model");
  if (item_m == client_map.end()) {
    model.set_model("default");
  } else {
    model.set_model(string(item_m->second.begin(), item_m->second.end()));
  }
}

int32_t GetSecondFromContext(const ServerContext * ctx) {
  const auto &client_map = ctx->client_metadata();
  auto item_s = client_map.find("in.seconds");
  if (item_s == client_map.end()) {
    return 300;
  } else {
    return std::stoi(string(item_s->second.begin(), item_s->second.end()));
  }
}

int32_t GetIntValueFromContext(const ServerContext * ctx,
                               const std::string &key,
                               int32_t def_val) {
  const auto &client_map = ctx->client_metadata();
  auto item = client_map.find(key);
  if (item == client_map.end()) {
    return def_val;
  } else {
    try {
      return std::stoi(string(item->second.begin(), item->second.end()));
    } catch (const std::invalid_argument &ia) {
      return def_val;
    }
  }
}


std::string GetFilenameFromContext(const ServerContext * ctx) {
  const auto &client_map = ctx->client_metadata();
  auto item_s = client_map.find("in.filename");
  if (item_s == client_map.end()) {
    return "";
  } else {
    return Base64Decode(string(item_s->second.begin(), item_s->second.end()));
  }
}

std::vector<string> split(const string &str, const string &delim) {
  std::vector<string> tokens;

  string::size_type prev = 0, pos = 0;
  do {
    pos = str.find(delim, prev);
    if (pos == string::npos)
      pos = str.length();
    string token = str.substr(prev, pos - prev);
    if (!token.empty())
      tokens.push_back(token);
    prev = pos + delim.length();
  } while (pos < str.length() && prev < str.length());
  return tokens;
}
