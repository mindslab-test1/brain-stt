#ifndef FEAT_BUFFER_H
#define FEAT_BUFFER_H

class FeatBuffer {
 public:
  FeatBuffer();
  ~FeatBuffer();

  bool   Create(int featdim /* 차원벡터크기 */, int max_frame_count /* 최대 프레임 크기 */);
  void   Reset();
  void   MoveWritePtr(int len);
  int    GetFrameCount(int min_frame_count);
  float *GetBuffer();
  float *GetFeatBuffer(int min_frame_count = 1);

 private:
  int featdim_;
  int max_frame_count_;
  float *buf_;
  float *rd_ptr_;
  float *wr_ptr_;
};

#endif /* FEAT_BUFFER_H */
