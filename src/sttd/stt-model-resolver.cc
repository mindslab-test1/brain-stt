#include <sys/socket.h>
#include <wait.h>
#include "stt-model-resolver.h"
#include <libmaum/common/config.h>
#include <libmaum/common/util.h>
#include <libmaum/common/fileutils.h>
#include <fstream>
#include <sys/resource.h>

#include "stt-real-impl.h"
#include "util.h"
#include "const.h"
#include <archive.h>
#include <archive_entry.h>
#include <cuda_runtime.h>

using grpc::StatusCode;
using grpc::ChannelInterface;
using grpc::ServerBuilder;
using grpc::Server;

using maum::brain::stt::SetModelResult;
using maum::common::FilePart;
using maum::brain::stt::SttRealService;

using namespace std;

atomic<SttModelResolver *> SttModelResolver::instance_;
mutex SttModelResolver::mutex_;

SttModelResolver *SttModelResolver::GetInstance() {
  SttModelResolver *tmp = instance_.load(std::memory_order_relaxed);
  std::atomic_thread_fence(std::memory_order_acquire);
  if (tmp == nullptr) {
    std::lock_guard<std::mutex> lock(mutex_);
    tmp = instance_.load(std::memory_order_relaxed);
    if (tmp == nullptr) {
      tmp = new SttModelResolver;
      std::atomic_thread_fence(std::memory_order_release);
      instance_.store(tmp, std::memory_order_relaxed);
    }
  }
  return tmp;
}

SttModelResolver::SttModelResolver() {
  // 기본 정보 초기화
  // 현재 서비스에 대한 정보 "JSON" 으로 덤프하기
  auto &c = libmaum::Config::Instance();
  model_root_ = c.Get("brain-stt.trained.root");

  export_ip_ = c.Get("brain-stt.sttd.front.export.ip");
  int min_port = c.GetAsInt("brain-stt.sttd.child.min.port");
  int max_port = c.GetAsInt("brain-stt.sttd.child.max.port");
  for (int i = min_port; i <= max_port; i++) {
    port_list_.push(i);
  }

  string json_dir = c.Get("run.dir");
  mkpath(json_dir);
  string json_file = c.Get("brain-stt.sttd.state.file");
  state_file_ = json_dir + '/' + json_file;

  // CHECK kBaselineKor8000 is prepared.
  // symlink kBaelineKor8000 to resources/stt/baseline-kor-8000
  auto res_root = c.Get("brain-stt.resource.root");
  auto baseline(model_root_);
  baseline += '/';
  baseline += kBaselineKor8000;
  if (access(baseline.c_str(), R_OK | W_OK | X_OK) == 0) {
    struct stat st;
    lstat(baseline.c_str(), &st);
    if (S_ISLNK(st.st_mode)) {
      return;
    }
    if (S_ISDIR(st.st_mode)) {
      LOGGER()->warn("{} is should be symlink to {}", baseline, res_root);
    }
  } else {
    auto baseline_org(res_root);
    baseline_org += '/';
    baseline_org += kBaselineKor8000;
    if (symlink(baseline_org.c_str(), baseline.c_str()) < 0) {
      LOGGER()->error("cannot symlink {} from: {} to: {}",
                      errno, baseline_org, baseline);
    }
  }

  cudaGetDeviceCount(&gpu_count_);

  LoadStatus();
}

SttModelResolver::~SttModelResolver() {
  LOGGER()->warn<const char *>("oops! dying");
}

// ### PRIVATE ###

bool SttModelResolver::IsRunning(const Model &model, Json::Value &found) {
  auto logger = LOGGER();
  string model_path = GetModelPath(model);
  Json::Value empty;

  lock_guard<mutex> guard(root_lock_);
  found = root_.get(model_path, empty);
  guard.~lock_guard();

  if (!found.isObject()) {
    logger->debug("not found {}", model_path);
    return false;
  }

  logger->debug("found {}", found.toStyledString());
  if (PingServer(found)) {
    return true;
  }
  logger->debug("Remove Member, {}", model_path);

  lock_guard<mutex> guard2(root_lock_);
  auto item = root_[model_path];
  port_list_.push(item["port"].asInt());
  root_[model_path].clear();
  root_.removeMember(model_path);
  WriteStatus();
  return false;
}


/**
 * 서버는 `fork`로 같은 프로세스를 다시 실행한다.
 *
 * 기존에 실행 중인 서버에 대한 정보를 가져온다. 없으면 새로 실행한다.
 * 실행 후에 `PingServer()`를 호출해서 그 결과를 받아 온다.
 *
 * 이미 서버가 실행 중일 경우에도 PingServer()를 호출해서 동작 여부를 확인한다.
 * 동작이 확인 후 서버가 동작하고 있지 않으면 다시 실행한다.
 * 그래도 정상적이지 않으면 서버 레코드를 삭제한다.
 *
 * @param ctx 호출하는 클라이언트 정보
 * @param query 요청하는 DM 서버 정보
 * @param location 실행중인 서버의 주소
 * @return 서비스 처리 상태, 요청하는 서버가 존재하지 않으면 NOT_FOUND를 반환한다.
 */
Status SttModelResolver::Find(const Model *model,
                              ServerStatus *ss) {
  auto logger = LOGGER();

  Json::Value found;
  bool running = IsRunning(*model, found);
  if (running) {
    Json::Value endpoint = found["endpoint"];
    ss->set_server_address(endpoint.asString());
    ss->set_model(model->model());
    ss->set_running(true);
    ss->set_invoked_by("find");
    return Status::OK;
  }

  // 서버를 새로 띄워야 합니다.
  Status status = Status::OK;
  Json::Value item;
  ForkResult res = ForkServer(model, item);
  logger->debug("[res] {}", res);
  switch (res) {
    case FORK_SUCCESS: {
      Json::Value endpoint = item["endpoint"];
      logger->debug("[server endpoint] {}", endpoint.asString());
      ss->set_server_address(endpoint.asString());
      ss->set_model(model->model());
      ss->set_running(true);
      ss->set_invoked_by("find&fork");
      status = Status::OK;
      break;
    }
    case FORK_NOT_FOUND: {
      logger->debug("cannot find {} : {}", model->model(), model->lang());
      status = Status(StatusCode::NOT_FOUND,
                      "Cannot find requested path or name");
      break;
    }
    case FORK_FAILED: {
      logger->debug("fork failed  errno: {}", errno);
      status = Status(StatusCode::INTERNAL, "Cannot fork");
      break;
    }
  }
  return status;
}

void ChildHandler(int sig) {
  int status;
  pid_t pid;
  while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
    LOGGER()->error("CHILD EXIT : {}  sig:{}", pid, sig);

    auto resolver = SttModelResolver::GetInstance();
    resolver->ClearModel(pid);
  }
}

extern char *g_program_dir;

/**
 *
 * @param query
 * @return
 */
SttModelResolver::ForkResult SttModelResolver::ForkServer(
    const Model *model, Json::Value &item) {
  auto logger = LOGGER();

  string model_path = GetModelPath(*model);
  string full_path = model_root_;
  full_path += '/';
  full_path += model_path;

  int path_found = access(full_path.c_str(), 0);
  if (path_found != 0) {
    logger->debug("{} is not found", full_path);
    return FORK_NOT_FOUND;
  }
  logger->debug("{} is found", full_path);

  // 순차적으로 PORT Range에 있는 Number를 할당
  std::unique_lock<std::mutex> port_lock(root_lock_);
  int port = 0;
  if (port_list_.empty()) {
    logger->error("Available Child port is exhausted");
    return FORK_FAILED;
  }
  port = port_list_.front();
  port_list_.pop();
  port_lock.unlock();

  int pid = fork();
  if (pid == -1) {
    return FORK_FAILED;
  }

  string endpoint = export_ip_ + ":" + std::to_string(port);
  if (pid == 0) {
    rlimit rl;
    getrlimit(RLIMIT_NOFILE, &rl);
    for (unsigned int fd = 3; fd < rl.rlim_cur; fd++)
      (void) close(fd);

    std::cout << "model: " << model->model() << std::endl;
    std::cout << "lang: " << model->lang() << std::endl;
    std::cout << "sample_rate: " << model->sample_rate() << std::endl;
    std::cout << "port: " << port << std::endl;

    char option_n[3] = "-n";
    char option_l[3] = "-l";
    char option_e[3] = "-e";
    char option_r[3] = "-r";
    char option_p[3] = "-p";
    char option_g[3] = "-g";
    char full_exe[PATH_MAX];
    snprintf(full_exe, sizeof(full_exe), "%s/%s",
             g_program_dir, "brain-stt-subd");

    char gpu_idx[10] = {0};

    fork_count_.fetch_add(1);
    snprintf(gpu_idx, sizeof gpu_idx, "%d",
             fork_count_ % gpu_count_);

    auto sample_rate_str = to_string(model->sample_rate());
    auto port_str = to_string(port);

    char *argv[] = {full_exe,
                    option_n, (char *) model->model().c_str(),
                    option_l, (char *) maum::common::LangCode_Name(model->lang()).c_str(),
                    option_r, (char *) sample_rate_str.c_str(),
                    option_e, (char *) endpoint.c_str(),
                    option_p, (char *) port_str.c_str(),
                    option_g, (char *) gpu_idx,
                    NULL};
    execv(full_exe, argv);

    // NOT REACH
    return FORK_SUCCESS;
  } else {
    logger->debug("insert forked object {} {} {}",
                  pid, model->model(), model->lang());
    logger->debug(
        "pid={}, path={}, lang={} model={} samplerate={}, endpoint={}",
        pid,
        model_path,
        maum::common::LangCode_Name(model->lang()),
        model->model(),
        model->sample_rate(),
        endpoint);
    item["pid"] = pid;
    item["path"] = model_path;
    item["lang"] = maum::common::LangCode_Name(model->lang());
    item["model"] = model->model();
    item["sample_rate"] = model->sample_rate();
    item["endpoint"] = endpoint;
    item["port"] = port;
    {
      lock_guard<mutex> guard(root_lock_);
      root_[model_path] = item;
      WriteStatus();
    }
    return FORK_SUCCESS;
  }
}

/**
 * 현재 메모리에 있는 상태를 모두 DB에 저장한다.
 *
 * DB는 JSON 파일이다.
 * 현재 메모리에 있는 모든 정보를 다 기록한다.
 *
 * 이 함수는 변경이 발생하면 매번 호출되어야 한다.
 *
 * 이 함수는 반드시 LOCK 환경에서만 호출되어야 합니다.
 */
void SttModelResolver::WriteStatus() {
  std::ofstream ofs(state_file_, std::ios_base::trunc | std::ios_base::out);
  Json::StreamWriterBuilder builder;
  builder["commentStyle"] = "None";
  builder["indentation"] = "  ";
  std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
  writer->write(root_, &ofs);
  ofs << std::endl;

  LOGGER()->debug("json {}", root_.toStyledString());
}

/**
 * 기존의 서버 등록 상태를 로딩한다.
 *
 * maum/run/"sds-svcd.dump.json" 파일이 존재하면 파일의 내용을 로딩한다.
 * 하나의 레코드를 로딩하면서 바로바로 서버의 존재여부를 체크한다.
 *
 * ```
 * [
 *    "model-kor-8000":
 *    {
 *      "pid": 1234,
 *      "lang": "kor",
 *      "name": "model",
 *      "path": "model-kor",
 *      "sample_rate" : 8000,
 *      "endpoint": "x.x.x.x:nnnn"
 *    }
 * ]
 * ```
 * 체크할 때에는 `PingServer()` 함수를 호출하도록 한다.
 */
void SttModelResolver::LoadStatus() {
  std::ifstream ifs(state_file_);
  auto logger = LOGGER();
  if (ifs.fail()) {
    logger->warn("cannot load state json {}: not exist", state_file_);
    return;
  }

  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;

  Json::Value temp;
  string errs;
  bool ok = Json::parseFromStream(builder, ifs, &temp, &errs);
  if (!ok) {
    logger->error("cannot parsing state json {} {}", state_file_, errs);
    return;
  }

  logger->debug("loaded status: {}", temp.toStyledString());

  for (auto item: temp) {
    if (!PingServer(item)) {
      port_list_.push(item["port"].asInt());
      temp.removeMember(item["path"].asString());
    }
  }

  lock_guard<mutex> guard(root_lock_);
  root_ = temp;
}

/**
 * 개별 서버가 동작하고 있는지를 점검한다.
 *
 * 먼저 서버의 PID가 동작중인지 점검한다.
 * 해당 서버로 Ping을 호출해서 그 결과를 확인하고 정상적이면 처리한다.
 *
 * @param item 개별 서버의 정보
 */
bool SttModelResolver::PingServer(const Json::Value &item) {
  pid_t pid = item["pid"].asInt();
  auto logger = LOGGER();
  logger->debug("Check pid : {}", pid);
  int rc = kill(pid, 0);
  if (rc < 0) {
    if (errno == ESRCH) {
      logger->error("kill error : {}", errno);
      return false;
    }
  }

  string endpoint = item["endpoint"].asString();
  auto stt_stub = SttRealService::NewStub(
      grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials()));
  grpc::ClientContext ctx;
  Model query;
  query.set_model(item["model"].asString());
  maum::common::LangCode langCode;
  maum::common::LangCode_Parse(item["lang"].asString(), &langCode);
  query.set_lang(langCode);
  query.set_sample_rate(item["sample_rate"].asInt());

  ServerStatus ss;
  Status status = stt_stub->Ping(&ctx, query, &ss);

  logger->debug("ping result {}", ss.running());
  if (status.ok()) {
    return !ss.server_address().empty();
  } else {
    logger->warn("grpc error code:{}, message:{}", status.error_code(), status.error_message());
    if (status.error_code() == StatusCode::UNAVAILABLE) {
      LOGGER()->debug("child process ({}-{}-{}) is preparing...",
                      query.model(), item["lang"].asString(), query.sample_rate());
      return true;
    }
    return false;
  }
}

int SttModelResolver::ClearModel(pid_t pid) {
  lock_guard<mutex> guard(root_lock_);
  for (auto item : root_) {
    auto run_pid = item["pid"].asInt();
    if (pid == run_pid) {
      auto path = item["path"].asString();
      port_list_.push(item["port"].asInt());

      root_[path].clear();
      root_.removeMember(path);
      WriteStatus();
      LOGGER()->debug("pid {} removed in root_", pid);
      return 0;
    }
  }
  LOGGER()->debug("pid {} not found in root_", pid);
  return -1;
}

static int IsHypen(int c) {
  return c == '-';
}

Status SttModelResolver::GetModels(ModelList *list) {
  struct dirent dirent;
  struct dirent *result;
  DIR *dir = nullptr;
  dir = opendir(model_root_.c_str());
  auto logger = LOGGER();
  if (dir == NULL) {
    logger->error("cannot open directory {}", model_root_);
    return Status(grpc::StatusCode::NOT_FOUND, "cannot open directory!");
  }

  while (readdir_r(dir, &dirent, &result) == 0 && result != NULL) {
    if (strncmp(dirent.d_name, ".", 1) == 0 ||
        strncmp(dirent.d_name, "..", 2) == 0)
      continue;
    // FIXME
    // Symbolic link인 경우, 원본파일의 Type이 Directory인 경우도 Model로 추가해야 한다.
    if (dirent.d_type == DT_DIR || dirent.d_type == DT_LNK) {
      logger->debug("sub directory found {}", dirent.d_name);
      vector<string> tokens = split(dirent.d_name, IsHypen);
      if (tokens.size() == 3) {
        LangCode lang;
        if (!maum::common::LangCode_Parse(tokens[1], &lang)) {
          logger->debug("invalid lang {}", tokens[1]);
          continue;
        }
        int32_t sample_rate = std::stoi(tokens[2]);
        if (sample_rate != 16000 && sample_rate != 8000) {
          logger->warn("invalid sample rate {}", tokens[2]);
        }
        auto m = list->add_models();
        m->set_lang(lang);
        m->set_sample_rate(sample_rate);
        m->set_model(tokens[0]);
      }
    }
  }
  closedir(dir);

  return Status::OK;
}

Status SttModelResolver::GetServers(ServerStatusList *list) {
  lock_guard<mutex> guard(root_lock_);
  int sync = 0;
  for (auto item : root_) {
    auto server = list->add_servers();
    LangCode lc;
    maum::common::LangCode_Parse(item["lang"].asString(), &lc);
    server->set_model(item["model"].asString());
    server->set_sample_rate(item["sample_rate"].asInt());
    server->set_lang(lc);
    server->set_server_address(item["endpoint"].asString());
    server->set_invoked_by("get servers");
    if (PingServer(item)) {
      server->set_running(true);
    } else {
      port_list_.push(item["port"].asInt());
      root_.removeMember(item["path"].asString());
      server->set_running(false);
      sync++;
    }
  }
  if (sync)
    WriteStatus();
  return Status::OK;
}


bool SttModelResolver::Stop(const Model &model, const Json::Value &found) {
  auto logger = LOGGER();
  logger->debug("found {}", found.toStyledString());
  pid_t pid = found["pid"].asInt();
  logger->debug("killing pid : {}", pid);

  bool ret = false;
  // TODO
  // 정상적으로 죽도록 처리해야 한다!!
  int rc = kill(pid, SIGTERM);
  if (rc < 0) {
    if (errno == ESRCH) {
      logger->warn("kill ESRCH error : {}", errno);
      ret = true;
    } else if(errno == EPERM) {
      logger->error("kill EPERM error : {}", errno);
      ret = false;
    }
  } else
    ret = true;
  if (ret) {
    auto model_path = GetModelPath(model);
    lock_guard<mutex> guard(root_lock_);
    auto item = root_[model_path];
    port_list_.push(item["port"].asInt());
    root_[model_path].clear();
    root_.removeMember(model_path);
    WriteStatus();
  }
  return ret;
}

/**
 * 동작 중인 서버를 종료시킨다.
 *
 * @param context 호출 컨텍스트
 * @param m 모델
 * @param server 서버 상태
 * @return grpc status
 */
Status SttModelResolver::Stop(const Model *m,
                              ServerStatus *server) {
  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_sample_rate(m->sample_rate());
  server->set_invoked_by("stop");

  Json::Value found;
  auto running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
    server->set_running(false);
  } else {
    server->set_running(false);
    server->set_invoked_by("stop&not_running");
  }
  return Status::OK;
}

Status SttModelResolver::Restart(const Model *m, ServerStatus *server) {
  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_sample_rate(m->sample_rate());
  server->set_invoked_by("restart");

  Json::Value found;
  auto running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
    server->set_running(false);
  } else {
    server->set_running(false);
    server->set_invoked_by("stop&not_running");
  }

  Status status = Status::OK;

  Json::Value item;
  ForkResult res = ForkServer(m, item);
  auto logger = LOGGER();
  logger->debug("[res] fork res {}", res);
  switch (res) {
    case FORK_SUCCESS: {
      Json::Value endpoint = item["endpoint"];
      logger->debug("[server endpoint] {}", endpoint.asString());
      server->set_server_address(endpoint.asString());
      server->set_running(true);
      status = Status::OK;
      break;
    }
    case FORK_NOT_FOUND: {
      logger->debug("cannot find {} : {}", m->model(), m->lang());
      status = Status(StatusCode::NOT_FOUND,
                      "Cannot find requested path or name");
      break;
    }
    case FORK_FAILED: {
      logger->debug("fork failed  errno: {}", errno);
      status = Status(StatusCode::INTERNAL, "Cannot fork");
      break;
    }
  }
  return status;
}

Status SttModelResolver::Ping(const Model *m,
                              ServerStatus *server) {
  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_sample_rate(m->sample_rate());
  server->set_invoked_by("ping");

  Json::Value found;
  auto running = IsRunning(*m, found);
  server->set_running(running);
  if (!running) {
    server->clear_server_address();
  }

  return Status::OK;
}

struct ServerReaderContext {
  ServerReaderContext(ServerReader<FilePart> * a_reader)
      : reader(a_reader) {}
  ServerReader<FilePart> * reader;
  FilePart part;
};

static ssize_t GrpcStreamReaderRead(archive */*ar*/,
                                    void *d,
                                    const void **buffer) {
  ServerReaderContext * ctx = static_cast<ServerReaderContext *>(d);
  int ret = ctx->reader->Read(&ctx->part);
  if (!ret) {
    return 0;
  } else {
    *buffer = ctx->part.part().c_str();
    return ctx->part.part().size();
  }
}

static int GrpcStreamReaderOpen(archive * /*ar*/, void * /* client_data */) {
  return ARCHIVE_OK;
}

static int GrpcStreamReaderClose(archive * /* ar */, void * /* client_data */) {
  return ARCHIVE_OK;
}

static int CopyData(archive *ar, struct archive *aw) {
  const void *buff;
  size_t size;
  int64_t offset;

  for (;;) {
    int r = archive_read_data_block(ar, &buff, &size, &offset);
    if (r == ARCHIVE_EOF)
      return (ARCHIVE_OK);
    if (r < ARCHIVE_OK)
      return (r);
    ssize_t wr = archive_write_data_block(aw, buff, size, offset);
    if (wr < ARCHIVE_OK) {
      LOGGER()->error("write data block failed {}", archive_error_string(aw));
      return int(wr);
    }
  }
}

bool ExtractTar(ServerReader<::maum::common::FilePart> *reader,
                const string &to_dir) {
  int r;

  archive *a = archive_read_new();
  archive_read_support_filter_all(a);
  archive_read_support_format_tar(a);

  archive *ext = archive_write_disk_new();

  /* Select which attributes we want to restore. */
  int flags = ARCHIVE_EXTRACT_TIME;
  flags |= ARCHIVE_EXTRACT_PERM;
  flags |= ARCHIVE_EXTRACT_ACL;
  flags |= ARCHIVE_EXTRACT_FFLAGS;

  archive_write_disk_set_options(ext, flags);
  archive_write_disk_set_standard_lookup(ext);

  ServerReaderContext ctx(reader);
  r = archive_read_open(a, &ctx,
                        GrpcStreamReaderOpen,
                        GrpcStreamReaderRead,
                        GrpcStreamReaderClose);
  archive_entry *entry = nullptr;

  bool ret = false;
  auto logger = LOGGER();
  for(int c = 0; ; c++) {
    logger->debug("before next header {}", c);
    r = archive_read_next_header(a, &entry);
    if (r == ARCHIVE_EOF) {
      logger->debug("archive eof {}", c);
      break;
    }
    if (r < ARCHIVE_OK)
      logger->error("archive error {}", archive_error_string(a));

    const char *e_path = archive_entry_pathname(entry);
    logger->debug("archive pathname {}", e_path);

    if (r < ARCHIVE_WARN) {
      logger->error("archive warn {}", e_path);
      goto error;
    }
    string new_path(to_dir);
    new_path += '/';
    new_path += e_path;

    archive_entry_set_pathname(entry, new_path.c_str());
    r = archive_write_header(ext, entry);
    if (r < ARCHIVE_OK)
      logger->error("archive write header failed {}",
                    archive_error_string(ext));
    else if (archive_entry_size(entry) > 0) {
      r = CopyData(a, ext);
      if (r < ARCHIVE_OK)
        logger->error("copy data failed, {}", archive_error_string(ext));
      if (r < ARCHIVE_WARN) {
        goto error;
      }
    }
    r = archive_write_finish_entry(ext);
    if (r < ARCHIVE_OK) {
      logger->error("archive write finish entry failed {}",
                    archive_error_string(ext));
    }
    if (r < ARCHIVE_WARN) {
      goto error;
    }
  }

  ret = true;

  error:
  archive_read_close(a);
  archive_read_free(a);
  archive_write_close(ext);
  archive_write_free(ext);
  return ret;
}

Status SttModelResolver::SetModel(const Model *model,
                                  ServerReader<::maum::common::FilePart> *reader,
                                  SetModelResponse *resp) {
  Json::Value found;
  bool running = IsRunning(*model, found);
  if (running) {
    Stop(*model, found);
  }
  resp->set_lang(model->lang());
  resp->set_sample_rate(model->sample_rate());
  resp->set_model(model->model());
  if (!ExtractTar(reader, model_root_)) {
    resp->set_result(SetModelResult::CL_SETMODEL_FAILED);
    resp->set_error("cannot extract tar file");
    return Status::OK;
  }
  resp->set_result(SetModelResult::CL_SETMODEL_SUCCESS);
  if (running) {
    ForkResult res = ForkServer(model, found);
    if (res == FORK_SUCCESS) {
      resp->set_result(SetModelResult::CL_SETMODEL_SUCCESS_RESTART);
    } else {
      resp->set_error("fork failed!");
      resp->set_result(SetModelResult::CL_SETMODEL_SUCCESS_NOTSTARTED);
    }
  }

  return Status::OK;
}

/**
 * 모델을 학습 데이터를 삭제한다.
 *
 * 모델에 관련된 서버가 떠 있으면 이를 중지하고 삭제한다.
 *
 * @param m 모델정보
 * @param server 서버 상태
 * @return GRPC 리턴
 */
Status SttModelResolver::DeleteModel(const Model *m,
                                     ServerStatus *server) {
  server->set_model(m->model());
  server->set_lang(m->lang());
  server->set_sample_rate(m->sample_rate());
  server->set_invoked_by("delete");

  Json::Value found;
  bool running = IsRunning(*m, found);
  if (running) {
    Stop(*m, found);
  }
  server->set_running(false);
  string path = model_root_;
  path += '/';
  path += GetModelPath(*m);

  RemoveFile(path.c_str(), FILEUTILS_RECUR | FILEUTILS_FORCE);
  return Status::OK;
}

/**
 *
 * @param context grpc 서버 컨텍스트
 * @param reader 리더 정보
 * @param response
 * @return
 */
Status SttModelResolverImpl::SetModel(ServerContext *context,
                                      ServerReader<::maum::common::FilePart> *reader,
                                      SetModelResponse *response) {
  Model model;
  GetModelFromContext(context, model);
  Status st = resolver_->SetModel(&model, reader, response);
  return st;
}


