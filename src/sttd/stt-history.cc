#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <thread>
#include <condition_variable>
#include <unistd.h>  // for sleep
#include "stt-history.h"
#include "remote-logger.h"
#include <libmaum/common/config.h>

#ifndef QUOTE
#define QUOTE(...) #__VA_ARGS__
#endif

using namespace std::chrono;

SttHistory::SttHistory(std::string file_path) {
  int rc;
  db_path_ = file_path;

  /* Open database */
  rc = sqlite3_open(db_path_.c_str(), &db_);
   
  if (rc) {
    REMOTE_LOGGER()->error("Can't open database: {}", sqlite3_errmsg(db_));
  } else {
    REMOTE_LOGGER()->info("Opened database successfully");
    sqlite3_busy_timeout(db_, 1000);
  }

  is_done_ = false;
}

SttHistory::~SttHistory() {
  if (db_) {
    sqlite3_close(db_);
  }

  {
    std::lock_guard<std::mutex> lk(lock_);
    is_done_ = true;
  }
  cv_.notify_one();

  if (thrd_.joinable()) {
    thrd_.join();
  }
}

void to_str_date(timeval time_tv, char *buf)
{
  struct tm tm_info;
  localtime_r(&time_tv.tv_sec, &tm_info);
  sprintf(buf, "%04d-%02d-%02d",
          1900 + tm_info.tm_year, tm_info.tm_mon + 1, tm_info.tm_mday);
}

void to_str_time(timeval time_tv, char *buf)
{
  struct tm tm_info;
  localtime_r(&time_tv.tv_sec, &tm_info);
  sprintf(buf, "%02d:%02d:%02d.%03ld",
          tm_info.tm_hour, tm_info.tm_min, tm_info.tm_sec,
          time_tv.tv_usec / 1000);
}

void to_str_delta(timeval time_tv, char *buf)
{
  sprintf(buf, "%02ld:%02ld:%02ld.%03ld",
          time_tv.tv_sec / 3600,
          time_tv.tv_sec / 60,
          time_tv.tv_sec % 60,
          time_tv.tv_usec / 1000);
}

bool SttHistory::InsertHistory(std::string id, timeval start_tv)
{
  std::unique_lock<std::mutex> lk(lock_);

  int rc;
  char *zErrMsg = 0;
  char start_time[32];
  char start_date[32];

  to_str_date(start_tv, start_date);
  to_str_time(start_tv, start_time);

  char buf[1024];
  const char *sql = QUOTE(
      INSERT INTO stt_history (id, start_date, start_time)
      VALUES ('%s', '%s', '%s')
  );

  sprintf(buf, sql, id.c_str(), start_date, start_time);

  /* Execute SQL statement */
  rc = sqlite3_exec(db_, buf, SttHistory::callback, 0, &zErrMsg);
   
  if (rc != SQLITE_OK) {
    if (zErrMsg != NULL)
      REMOTE_LOGGER()->error("SQL error: {} (id - {})", zErrMsg, id);
    sqlite3_free(zErrMsg);
  } else {
    REMOTE_LOGGER()->debug("Table inserted successfully - {}", id);
  }
  return true;
}

bool SttHistory::UpdateStopEvent(std::string id, timeval stop_tv)
{
  std::unique_lock<std::mutex> lk(lock_);

  char stop_time[64];

  to_str_time(stop_tv, stop_time);

  int rc;
  char *zErrMsg = 0;

  char buf[1024];
  const char *sql = QUOTE(
      UPDATE stt_history
      SET    stop_time = '%s'
      WHERE  id = '%s'
  );

  sprintf(buf, sql, stop_time, id.c_str());

  /* Execute SQL statement */
  rc = sqlite3_exec(db_, buf, SttHistory::callback, 0, &zErrMsg);
   
  if (rc != SQLITE_OK) {
    if (zErrMsg != NULL)
      REMOTE_LOGGER()->error("SQL error: {} (id - {})", zErrMsg, id);
    sqlite3_free(zErrMsg);
  } else {
    REMOTE_LOGGER()->debug("Table updated successfully (id - {})", id);
  }
  return true;
}

bool SttHistory::
UpdateEndEvent(std::string id, timeval start_tv, timeval stop_tv, timeval end_tv)
{
  std::unique_lock<std::mutex> lk(lock_);

  timeval delay_tv, duration_tv;
  timersub(&end_tv, &start_tv, &duration_tv);
  timersub(&end_tv, &stop_tv, &delay_tv);

  char stop_time[64];
  char end_time[64];
  char duration_time[64];
  char delay_time[64];

  to_str_time(stop_tv, stop_time);
  to_str_time(end_tv, end_time);
  to_str_delta(duration_tv, duration_time);
  to_str_delta(delay_tv, delay_time);

  int rc;
  char *zErrMsg = 0;

  char buf[1024];
  const char *sql = QUOTE(
      UPDATE stt_history
      SET    stop_time = '%s', end_time = '%s', delay_time = '%s', duration = '%s'
      WHERE  id = '%s'
  );

  sprintf(buf, sql, stop_time, end_time, delay_time, duration_time, id.c_str());

  /* Execute SQL statement */
  rc = sqlite3_exec(db_, buf, SttHistory::callback, 0, &zErrMsg);
   
  if (rc != SQLITE_OK) {
    if (zErrMsg != NULL)
      REMOTE_LOGGER()->error("SQL error: {} (id - {})", zErrMsg, id);
    sqlite3_free(zErrMsg);
  } else {
    REMOTE_LOGGER()->debug("Table updated successfully (id - {})", id);
  }
  return true;
}

void SttHistory::RollOver() {
  char *zErrMsg = 0;

  const char *sql = QUOTE(
      INSERT INTO stt_history_old
      SELECT * from stt_history;

      DROP TABLE stt_history;

      CREATE TABLE stt_history (
          id	NUMERIC NOT NULL,
          start_date	date,
          start_time	datetime,
          stop_time	datetime,
          end_time	datetime,
          delay_time	datetime,
          duration	datetime,
          PRIMARY KEY(id)
      );
      CREATE INDEX stt_history_idx ON stt_history (start_date);
  );

  /* Execute SQL statement */
  int rc = sqlite3_exec(db_, sql, 0, 0, &zErrMsg);
   
  if (rc != SQLITE_OK) {
    if (zErrMsg != NULL)
      REMOTE_LOGGER()->error("SQL error: {}", zErrMsg);
    sqlite3_free(zErrMsg);
  } else {
    REMOTE_LOGGER()->info("Table rollover successfully");
  }
}

void SttHistory::DeleteHistory(int max_days) {
  char *zErrMsg = 0;

  const char *sql = QUOTE(
      DELETE FROM stt_history_old
      WHERE  start_date < date('now', '-%d day');
  );
  char buf[1024];
  sprintf(buf, sql, max_days);

  /* Execute SQL statement */
  int rc = sqlite3_exec(db_, buf, 0, 0, &zErrMsg);
   
  if (rc != SQLITE_OK) {
    if (zErrMsg != NULL)
      REMOTE_LOGGER()->error("SQL error: {}", zErrMsg);
    sqlite3_free(zErrMsg);
  } else {
    REMOTE_LOGGER()->info("Table deleted successfully");
  }
}

void SttHistory::reopen() {
  if (db_) {
    sqlite3_close(db_);
  }

  /* Open database */
  int rc = sqlite3_open(db_path_.c_str(), &db_);
   
  if (rc) {
    // fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
  } else {
    // fprintf(stdout, "Opened database successfully\n");
  }
}

void SttHistory::create_table() {
  char *zErrMsg = 0;

  std::string query = QUOTE(
      CREATE TABLE IF NOT EXISTS stt_history (
          id	NUMERIC NOT NULL,
          start_date	date,
          start_time	datetime,
          stop_time	datetime,
          end_time	datetime,
          delay_time	datetime,
          duration	datetime,
          PRIMARY KEY(id)
      );
      CREATE INDEX IF NOT EXISTS stt_history_idx ON stt_history (start_date);

      CREATE TABLE IF NOT EXISTS stt_history_old (
          id	NUMERIC NOT NULL,
          start_date	date,
          start_time	datetime,
          stop_time	datetime,
          end_time	datetime,
          delay_time	datetime,
          duration	datetime,
          PRIMARY KEY(id)
      );
      CREATE INDEX IF NOT EXISTS stt_history_old_idx ON stt_history_old (start_date);
  );

  /* Execute SQL statement */
  int rc = sqlite3_exec(db_, query.c_str(), SttHistory::callback, 0, &zErrMsg);

  if (rc != SQLITE_OK) {
    if (zErrMsg != NULL)
      REMOTE_LOGGER()->error("SQL error: {}", zErrMsg);
    sqlite3_free(zErrMsg);
  } else {
    REMOTE_LOGGER()->info("Table created successfully");
  }
}

int SttHistory::callback(void *userData, int argc, char **argv, char **azColName) {
  return 0;
}

void SttHistory::show_rollover_time(std::chrono::system_clock::time_point when) {
  char buf[128];
  tm   date;
  time_t t = std::chrono::system_clock::to_time_t(when);

  localtime_r(&t, &date);
  strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", &date);
  LOGGER()->info("next rollover time: {}", buf);
}

std::chrono::system_clock::time_point get_rollover_time(int hour) {
  auto now = std::chrono::system_clock::now();

  time_t tnow = std::chrono::system_clock::to_time_t(now);
  tm date_buf;
  tm *date = localtime_r(&tnow, &date_buf);
  date->tm_hour  = hour;
  date->tm_min   = 0;
  date->tm_sec   = 0;

  time_t next = std::mktime(date);
  auto expire = std::chrono::system_clock::from_time_t(next);
  expire += std::chrono::hours(24);

  return expire;
}

void SttHistory::Start(int hour, int max_days) {
  create_table();

  thrd_ = std::thread([&, hour, max_days]() {
      auto expire = get_rollover_time(hour);
      std::unique_lock<std::mutex> lk(lock_);
      while (!is_done_) {
        // for log
        show_rollover_time(expire);

        if (cv_.wait_until(lk, expire) ==  std::cv_status::timeout) {
          RollOver();
          DeleteHistory(max_days);
          expire += std::chrono::hours(24);
        } else {
          LOGGER()->debug("history thread waked up");
        }
      }
    });
}

#ifdef TEST
#include <sys/time.h>

int main(int argc, char *argv[])
{
  // timeval start_tv, stop_tv, end_tv, delay, duration;
  timeval start_tv, stop_tv, end_tv;

  gettimeofday(&start_tv, NULL);
  stop_tv = start_tv;
  stop_tv.tv_sec += 3;

  end_tv = stop_tv;
  end_tv.tv_sec += 1;

  SttHistory stats("/home/minds/test.db");
  stats.Start();
  while (true) {
    sleep(1000);
  }

  stats.InsertHistory("1237", start_tv);
  stats.UpdateStopEvent("1237", stop_tv);
  stats.UpdateEndEvent("1237", start_tv, stop_tv, end_tv);
  // stats.ExecuteSQL("select * from stt_history");

  sleep(1000);
  return 0;
}
#endif

// g++ -DTEST -std=c++11 stt-history.cc -lsqlite3 -lpthread
