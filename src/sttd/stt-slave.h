#ifndef STT_SLAVE_H
#define STT_SLAVE_H

#include "stt-master.h"

/**
 * 각 GPU 단위로 개별 CPU 코어에 맞게 할당된 Slave FrontEnd 객체와
 * Laser 객체를 유지하면서 실질적인 Stream을 처리할 수 있도록 유지한다.
 *
 * Slave는 반복적으로 SttStream이 생성되고 소멸될 때마다 상태를 변경한다.
 * 생성될 때, Reset()으로 초기화하고, Clear()로 상태를 정리한다.
 */
class SttSlave {
 public:
  SttSlave(SttMaster *master, int sample_rate, int seq = 0, bool do_epd = false);
  virtual ~SttSlave();
  void Reset(int32_t seq);
  void Clear();
  void ReloadOption(bool do_epd);
 private:
  void LoadFrontEnd();
  /**
   * SttMater의 설정 정보에 접근하기 위해서 SttMaster 를 꺼내온다.
   * @return
   */
  SttMaster *p() {
    return parent_;
  }

  // USED GLOBALLY..
  SttMaster *parent_;
  int seq_;
  int sample_rate_;
  bool do_epd_;
  LFrontEnd *front_ = nullptr;
  Laser *laser_ = nullptr;
  int epd_len_front_margin_ = 40;
  int useLSTM_;
  std::string model_dirname_;

  // LOCK..
  bool busy_ = true;
  friend class SttStream;
  friend class SttMaster;
};

#endif /* STT_SLAVE_H */
