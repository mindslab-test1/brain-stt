#!/usr/bin/python

import grpc
import os
import sys
from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc

def bytes_from_file(filename, chunksize=10000):
    with open(filename, "rb") as f:
        while True:
            chunk = f.read(chunksize)
            if chunk:
                speech = stt_pb2.Speech()
                speech.bin = chunk
                yield speech
            else:
                break

def simple_recognize(channel, metadata):
    stub = stt_pb2_grpc.SpeechToTextServiceStub(channel)
    result = stub.SimpleRecognize(bytes_from_file('./8k.pcm'), metadata=metadata)
    print result.txt

def stream_recognize(channel, metadata):
    stub = stt_pb2_grpc.SpeechToTextServiceStub(channel)
    segments = stub.StreamRecognize(bytes_from_file('./8k.pcm'), metadata=metadata)
    try:
        for seg in segments:
            print '%.2f ~ %.2f : %s' % (seg.start / 100.0, seg.end / 100.0, seg.txt)
    except grpc.RpcError as e:
        print('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details()))

channel = grpc.insecure_channel('127.0.0.1:9801')
metadata={(b'in.lang', b'kor'), (b'in.model', 'baseline'), (b'in.samplerate', '8000') }

simple_recognize(channel, metadata)
stream_recognize(channel, metadata)
