#!/bin/sh
protoc --cpp_out=. --grpc_out=. --plugin=protoc-gen-grpc=/your_grpc_plugin_path/bin/grpc_cpp_plugin stt.proto
g++-4.8 -std=c++11 -I /your_grpc_path/include/ -L /your_grpc_path/lib/ simple-client.cc stt.grpc.pb.cc stt.pb.cc -o simple-client -l grpc++ -l protobuf
g++-4.8 -std=c++11 -I /your_grpc_path/include/ -L /your_grpc_path/lib/ stream-client.cc stt.grpc.pb.cc stt.pb.cc -o stream-client -l pthread -l grpc++ -l protobuf
