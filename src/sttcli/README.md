* grpc library 설치
```
$ go get -u google.golang.org/grpc
$ go get -u github.com/golang/protobuf/protoc-gen-go
```

* PATH 설정 (별도의 GOPATH를 설정하지 않은 경우)
```
export PATH=~/go/bin:$PATH
```

* proto 생성
```
make pb
```

* build
```
make
```

* 실행 or 도움말
./go-stt-test -h 참고
