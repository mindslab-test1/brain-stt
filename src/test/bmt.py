#!/usr/bin/python

import sys
import time
import subprocess
from multiprocessing import Process

def f(i):
    for n in range(1):
        st = time.time()
        subprocess.call("stt-test stream > /dev/null", shell=True)
        print 'idx %d :elapsed time is %.3f' % (i, time.time()-st)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print '%s process_count' % sys.argv[0]
        sys.exit(0)
    plist = []
    process_cnt = int(sys.argv[1])
    for i in range(process_cnt):
        p = Process(target=f, args=(i,))
        p.start()
        plist.append(p)
    for p in plist:
        p.join()
