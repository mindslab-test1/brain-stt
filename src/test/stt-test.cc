#include <memory>
#include <vector>
#include <iostream>
#include <thread>
#include <stdio.h>
#include <math.h>

#include <grpc++/grpc++.h>

#include <libmaum/common/config.h>
#include <spdlog/spdlog.h>

#ifdef SPDLOG_VER_MAJOR
#include <spdlog/sinks/stdout_color_sinks.h>
#endif

#include <maum/brain/stt/stt.grpc.pb.h>

using std::string;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::ClientReaderWriter;

using maum::brain::stt::SpeechToTextService;
using maum::brain::stt::SttRealService;
using maum::brain::stt::SttModelResolver;
using maum::brain::stt::ServerStatus;
using maum::brain::stt::Model;
using maum::brain::stt::Speech;
using maum::brain::stt::Text;
using maum::brain::stt::DetailText;
using maum::brain::stt::Segment;

class SttRealClient {

 public:
  SttRealClient(std::shared_ptr<Channel> channel)
      : stub_(SttRealService::NewStub(channel)) {
  }

  void SimpleRecognize(const string& filename) {
    ClientContext ctx;

    ctx.AddMetadata("in.lang", "kor");
    ctx.AddMetadata("in.samplerate", "8000");
    ctx.AddMetadata("in.model", "baseline");

    Text resp;
    auto writer = stub_->SimpleRecognize(&ctx, &resp);

    int read_cnt;
    int16_t buf[24000];
    FILE *fp = fopen(filename.c_str(), "rb");
    if (fp == NULL) {
      std::cerr << "cannot open file : " << filename << std::endl;
      return;
    }
    while ((read_cnt = fread(buf, 2, 24000, fp)) > 0) {
      Speech speech;
      speech.set_bin(buf, size_t(read_cnt * sizeof(int16_t)));
      writer->Write(speech);
    }
    writer->WritesDone();
    auto status = writer->Finish();
    if (status.ok()) {
      std::cout << "result: " << resp.txt() << std::endl;
    } else {
      std::cout << "failed: " << status.error_message() << std::endl;
    }
  }

 private:
  std::unique_ptr<SttRealService::Stub> stub_;
};


class SttClient {

 public:
  SttClient(std::shared_ptr<Channel> channel)
      : stub_(SpeechToTextService::NewStub(channel)) {
    // Console logger with color
    console_ = spdlog::stdout_color_mt("console");
    console_->set_level(spdlog::level::debug);
  }

  void SetMetadata(std::string model, std::string lang,
                   std::string sample_rate, std::string file_path) {
    model_ = model;
    lang_ = lang;
    sample_rate_ = sample_rate;
    file_path_ = file_path;
  }

  void SimpleRecognize() {
    ClientContext ctx;

    ctx.AddMetadata("in.lang", lang_);
    ctx.AddMetadata("in.samplerate", sample_rate_);
    ctx.AddMetadata("in.model", model_);

    Text resp;
    auto writer = stub_->SimpleRecognize(&ctx, &resp);

    int read_cnt;
    int16_t buf[500];
    FILE *fp = fopen(file_path_.c_str(), "rb");
    if (fp == NULL) {
      std::cerr << "cannot open file : " << file_path_ << std::endl;
      return;
    }
    while ((read_cnt = fread(buf, 2, 500, fp)) > 0) {
      Speech speech;
      speech.set_bin(buf, size_t(read_cnt * sizeof(int16_t)));
      writer->Write(speech);
    }
    writer->WritesDone();
    auto status = writer->Finish();
    if (status.ok()) {
      std::cout << "result: " << resp.txt() << std::endl;
    } else {
      std::cout << "failed: " << status.error_message() << std::endl;
    }
  }

  void DetailRecongnize() {
    ClientContext ctx;

    ctx.AddMetadata("in.lang", lang_);
    ctx.AddMetadata("in.samplerate", sample_rate_);
    ctx.AddMetadata("in.model", model_);

    DetailText resp;
    auto writer = stub_->DetailRecognize(&ctx, &resp);

    int read_cnt;
    int16_t buf[500];
    FILE *fp = fopen(file_path_.c_str(), "rb");
    if (fp == NULL) {
      std::cerr << "cannot open file : " << file_path_ << std::endl;
      return;
    }
    while ((read_cnt = fread(buf, 2, 500, fp)) > 0) {
      Speech speech;
      speech.set_bin(buf, size_t(read_cnt * sizeof(int16_t)));
      writer->Write(speech);
    }
    writer->WritesDone();
    auto status = writer->Finish();
    if (status.ok()) {
      std::cout << "result: " << resp.txt() << std::endl;
      std::cout << "mlf: " << resp.raw_mlf() << std::endl;
    } else {
      std::cout << "failed: " << status.error_message() << std::endl;
    }
  }

  void StreamRecognize() {
    ClientContext ctx;

    ctx.AddMetadata("in.lang", lang_);
    ctx.AddMetadata("in.samplerate", sample_rate_);
    ctx.AddMetadata("in.model", model_);

    Text resp;
    auto stream = stub_->StreamRecognize(&ctx);
    console_->debug("{}, {}", lang_, sample_rate_);

    int read_cnt;
    int16_t buf[500];
    FILE *fp = fopen(file_path_.c_str(), "rb");
    if (fp == NULL) {
      std::cerr << "cannot open file : " << file_path_ << std::endl;
      return;
    }

    std::thread result_thrd(&SttClient::ReadStream, this, stream.get());
    size_t sent_cnt = 0;
    size_t duration = 0;

    int sample_rate = atoi(sample_rate_.c_str());
    while ((read_cnt = fread(buf, 2, 500, fp)) > 0) {
      Speech speech;
      speech.set_bin(buf, size_t(read_cnt * sizeof(int16_t)));
      stream->Write(speech);
      for (int i = 0; i < read_cnt; i++) {
        sample_list_.push_back(buf[i]);
      }

      // delay like real speaking
      usleep(read_cnt * 1000 * 1000 / sample_rate);

      sent_cnt += read_cnt;
      if (sent_cnt % sample_rate == 0) {
        console_->debug("Speaking Time ---> {} seconds", duration);
        duration++;
      }
    }

    stream->WritesDone();

    result_thrd.join();

    auto status = stream->Finish();
    if (status.ok()) {
      std::cout << "result: OK" << std::endl;
    } else {
      std::cout << "failed: " << status.error_message() << std::endl;
    }
  }

  bool WaitServer(const string & addr) {
    auto resolver_channel =
      grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
    auto resolver_stub = SttModelResolver::NewStub(resolver_channel);

    ClientContext ctx;
    ServerStatus ss;
    Model model;
    if (lang_ == "kor") {
      model.set_lang(maum::common::LangCode::kor);
    } else {
      model.set_lang(maum::common::LangCode::eng);
    }
    model.set_sample_rate(atoi(sample_rate_.c_str()));
    model.set_model(model_);

    Status st = resolver_stub->Find(&ctx, model, &ss);

    for (int i = 0; i < 10; i++) {
      auto real_channel =
          grpc::CreateChannel(ss.server_address(), grpc::InsecureChannelCredentials());
      auto real_stub = SttRealService::NewStub(real_channel);
      ClientContext real_ctx;
      st = real_stub->Ping(&real_ctx, model, &ss);

      if (st.ok()) break;
      usleep(1000 * 1000);
    }
    return true;
  }

 private:

  std::unique_ptr<SpeechToTextService::Stub> stub_;
  std::shared_ptr<spdlog::logger> console_;
  std::vector<int16_t> sample_list_;

  // http://stackoverflow.com/questions/12721254/how-to-calculate-sound-frequency-in-android
  int CalculateTone(int sample_rate, int start, int end) {
    int num_samples = end - start;
    int num_crossing = 0;
    for (int p = start; p < end-1; p++) {
      if ((sample_list_[p] > 0 && sample_list_[p + 1] <= 0) ||
          (sample_list_[p] < 0 && sample_list_[p + 1] >= 0)) {
        num_crossing++;
      }
    }

    float num_seconds_recorded = (float)num_samples / (float)sample_rate;
    float num_cycles = num_crossing / 2;
    float frequency = num_cycles / num_seconds_recorded;

    return (int)frequency;
  }

  void ReadStream(ClientReaderWriter<Speech, Segment>* stream) {
    Segment segment;
    int sample_rate = atoi(sample_rate_.c_str());
    while (stream->Read(&segment)) {
      int start_sample;
      int end_sample;

      // segment start unit is 0.01 second, sample rate is 8000
      start_sample = segment.start() * sample_rate / 100;
      end_sample = segment.end() * sample_rate / 100;
      int tone = CalculateTone(sample_rate, start_sample, end_sample);

      console_->info("Channel 0: Range {0: >8.2f} ~ {1: >8.2f}, {2} Tone: {3}hz",
                     (float)segment.start()/100.0,
                     (float)segment.end()/100.0,
                     segment.txt().c_str(),
                     tone
                    );
    }
    std::cout << "stream read end..." << std::endl;
  }

  std::string model_;
  std::string sample_rate_;
  std::string lang_;
  std::string file_path_;
};

int main(int argc, char** argv) {
  char cwd[256];
  if (argc != 2) {
    printf("%s <real | stream | simple | detail>\n", argv[0]);
    return 0;
  }

  getcwd(cwd, sizeof(cwd));
  auto &c = libmaum::Config::Init(argc, argv, "stt-test.conf");
  chdir(cwd);
  std::string model = c.Get("stt.test.model");
  std::string lang = c.Get("stt.test.lang");
  std::string sample_rate = c.Get("stt.test.sample_rate");
  std::string file_path = c.Get("stt.test.file_path");

  string remote_na("127.0.0.1");
  remote_na += ':';
  remote_na += c.Get("brain-stt.sttd.front.port");
  std::shared_ptr<Channel> channel;
  channel = grpc::CreateChannel(remote_na, grpc::InsecureChannelCredentials());

  SttClient client(channel);
  client.SetMetadata(model, lang, sample_rate, file_path);

  if (strncmp(argv[1], "real", 4) == 0 ) {
    SttRealClient real_client(channel);
    real_client.SimpleRecognize("/home/gih2yun/aa.pcm");

  } else if (strncmp(argv[1], "stream", 6) == 0) {
    if (client.WaitServer(remote_na))
      client.StreamRecognize();

  } else if (strncmp(argv[1], "simple", 6) == 0) {
    if (client.WaitServer(remote_na))
      client.SimpleRecognize();

  } else if (strncmp(argv[1], "detail", 6) == 0) {
    if (client.WaitServer(remote_na))
      client.DetailRecongnize();

  } else {
    client.SimpleRecognize();
  }

  return 0;
}

