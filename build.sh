#!/usr/bin/env bash

maum_root=${MAUM_ROOT}
if [ -z "${maum_root}" ]; then
  echo 'MAUM_ROOT is not defined!'
  exit 1
fi

test -d ${maum_root} || mkdir -p ${maum_root}

[ -n $(which nproc) ] && {
  NPROC=$(nproc)
} || {
  NPROC=$(cat /proc/cpuinfo | grep cores | wc -l)
}

repo_root=$(pwd)
echo "[brain-stt]" repo_root: ${repo_root}, NPROC: ${NPROC}, MAUM_ROOT: ${MAUM_ROOT}

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS, use ubuntu or centos"
  exit 1
fi

function get_requirements() {
  if [ "${OS}" = "ubuntu" ]; then
      sudo apt-get install \
           libzmq3-dev \
           libsqlite3-dev \
           libarchive13 \
           libarchive-dev \
           libatlas-base-dev \
           libatlas-dev \
           build-essential \
           python-dev \
           make \
           cmake \
           automake \
           libtool \
           g++-4.8 \
           g++
  else
    sudo yum install -y gcc \
         epel-release \
         zeromq-devel \
         sqlite-devel \
         gcc-c++ \
         libarchive-devel.x86_64 \
         atlas-devel.x86_64 \
         python-devel.x86_64 \
         glibc.x86_64 \
         lapack-devel.x86_64 \
         autoconf \
         automake \
         libtool \
         make
  fi

  curl "https://bootstrap.pypa.io/get-pip.py" | sudo python
  sudo pip install --upgrade pip
  sudo pip install --upgrade virtualenv
  sudo pip install boto3==1.4.7 grpcio==1.9.1 requests numpy theano workerpool
  touch ${maum_root}/.stt-installed
}

GLOB_BUILD_DIR=${HOME}/.maum-build
test -d ${GLOB_BUILD_DIR} || mkdir -p ${GLOB_BUILD_DIR}
sha1=$(git log -n 1 --pretty=format:%H ./build.sh)
echo "Last commit for build.sh: ${sha1}"

# not docker build && update commit
if [ -z "${DOCKER_MAUM_BUILD}" ] && [ ! -f ${GLOB_BUILD_DIR}/${sha1}.done ]; then
  get_requirements
  if [ "$?" = "0" ]; then
    touch ${GLOB_BUILD_DIR}/${sha1}.done
  fi
else
  echo " get_requirements had been done!"
fi

__CC=${CC}
__CXX=${CXX}
if [ "${OS}" = "centos" ]; then
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc
    __CC=/usr/bin/gcc
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++
    __CXX=/usr/bin/g++
  fi

  CMAKE=/usr/bin/cmake3
  OS_VER=`cat /etc/redhat-release`
else
  if [ -z ${__CC} ]; then
    echo CC not defined, use /usr/bin/gcc-4.8
    __CC=/usr/bin/gcc-4.8
  fi
  if [ -z ${__CXX} ]; then
    echo CXX not defined, use /usr/bin/g++-4.8
    __CXX=/usr/bin/g++-4.8
  fi
  CMAKE=/usr/bin/cmake
  OS_VER=`lsb_release -sd`
fi

GCC_VER=$(${__CC} -dumpversion)

build_base="build-debug" && [[ "${MAUM_BUILD_DEPLOY}" == "true" ]] && build_base="build-deploy-debug"
build_dir=${PWD}/${build_base}-${GCC_VER}

__BUILD_TYPE=${BUILD_TYPE}
if [ -z ${BUILD_TYPE} ]; then
  echo BUILD_TYPE not defined, set BUILD_TYPE Debug
  __BUILD_TYPE=Debug
fi

# 다른 프로젝트의 build.sh tar .... 의 명령을 실행할 때 build.sh clean-deploy를 한 적이 있을 경우
# CMakeCache.txt가 예전 MAUM_ROOT(deploy-XXXXX)를 바라보는 문제가 있으므로 빌드 디렉토리를 새로 만든다

function build_main() {
  if [ "$MAUM_BUILD_DEPLOY" = true ]; then
    test -d ${build_dir} && rm -rf ${build_dir}
  fi

  test -d ${build_dir} || mkdir -p ${build_dir}
  cd ${build_dir}

  ${CMAKE} \
    -DCMAKE_PREFIX_PATH=${maum_root} \
    -DCMAKE_BUILD_TYPE=${__BUILD_TYPE} \
    -DCMAKE_C_COMPILER=${__CC} \
    -DCMAKE_CXX_COMPILER=${__CXX} \
    -DCMAKE_INSTALL_PREFIX=${maum_root} ..

  if [ "$1" = "proto" ]; then
    (cd proto && make install -j${NPROC})
    (cd pysrc/proto && make install -j${NPROC})
  else
    if [ "${MAUM_BUILD_DEPLOY}" = "true" ]; then
      make install -j${NPROC}
    else
      make install -j${NPROC}
    fi
  fi
}

function build_libmaum() {
  git clone git@github.com:mindslab-ai/libmaum.git temp-libmaum
  cd temp-libmaum
  ./build.sh ${maum_root}
  cd -
  rm -rf temp-libmaum
}

function check_libmaum() {
  if [ -f ${maum_root}/lib/libmaum-pb.so ]; then
    echo "libmaum has built"
    return 0;
  else
    echo "libmaum has not built, now build libmaum first."
    return 1;
  fi
}

function build_doc() {
  test -d ${maum_root}/doc || mkdir -p ${maum_root}/doc
  cp ${repo_root}/doc/* ${maum_root}/doc/
}

if ! check_libmaum; then
  build_libmaum
fi

build_main $*
build_doc

# DNN to DLNet convert tool
cp ${repo_root}/utils/convDNN2DLNet.2017.1220/cuda8.0/convDNN2DLNet ${maum_root}/bin/convDNN2DLNet-8.0

### VERSION
BIZ_VERSION=`git describe --tags`
BUILD_DATE=`date '+%Y-%m-%d %H:%M:%S'`
echo "BIZ_VERSION: ${BIZ_VERSION}" > ${maum_root}/etc/brain-stt-version
echo "BUILD_DATE: ${BUILD_DATE}" >> ${maum_root}/etc/brain-stt-version
echo "OS_VERSION: ${OS_VER}" >> ${maum_root}/etc/brain-stt-version
