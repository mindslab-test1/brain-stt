#!/bin/sh
#sox $1 -b 16 -s -c 1 -r 8k -t raw $2
# https://stackoverflow.com/questions/4854513/can-ffmpeg-convert-audio-to-raw-pcm-if-so-how
ffmpeg -i $1 -ac 1 -ar 8000 $2
