#!/usr/bin/python

import os
import fnmatch
import subprocess

def find_file(suffix):
    matches = []
    for root, dirnames, filenames in os.walk('.'):
        for filename in fnmatch.filter(filenames, suffix):
            matches.append(os.path.join(root, filename))
    return matches

def main():
    pcm_list = find_file('*.pcm.wav')
    for pcm in pcm_list:
        os.remove(pcm)

    wav_list = find_file('*.wav')
    for wav in wav_list:
        if wav.endswith('pcm.wav'):
            continue
        pcm = wav.replace('.wav', '.pcm.wav')
        cmd = './any_to_wav.sh {} {}'.format(wav, pcm)
        # print cmd
        subprocess.call(cmd, shell=True)
        subprocess.call('./stt_diar.py -m base_unilstm {} > {}.result'.format(pcm, pcm), shell=True)

if __name__ == '__main__':
    main()
