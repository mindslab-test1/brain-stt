#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

import grpc
import time
import argparse

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.getenv('MAUM_ROOT') + '/lib/python'
sys.path.append(lib_path)

from common.config import Config
from maum.brain.stt import stt_version
from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc
from maum.common import lang_pb2
from maum.common import types_pb2
import time
from decimal import *

import google.protobuf.empty_pb2 as empty

from maum.brain.dap.dap_pb2_grpc import DiarizeStub
from maum.brain.dap.dap_pb2 import EmbList
from maum.brain.dap.dap_pb2 import WavBinary
import collections

DIAR = list()

class DiarizeClient(object):
    def __init__(self, remote='172.17.0.2:42001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = DiarizeStub(channel)
        self.chunk_size = chunk_size

    def get_diarize(self, emblist):
        return self.stub.GetDiarization(emblist)

    def get_diarize_from_wav(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.GetDiarizationFromWav(wav_binary)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def get_diarize_config(self):
        return self.stub.GetDiarizeConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavBinary(bin=wav_binary[idx:idx+self.chunk_size])

def do_diarization(file_path):
    diar_client = DiarizeClient()
    with open(file_path, 'rb') as rf:
        diarize_result = diar_client.get_diarize_from_wav(rf.read()).data
    #DIAR = diarize_result
    global DIAR
    n = 0
    #csv_result = list()
    for r in diarize_result:
        print '%.2f,%.2f,%d' % (n*0.4, (n+1)*0.4, r)
        #csv_result.append([n*0.4, (n+1)*0.4, r])
        n += 1
        for i in range(40):
            DIAR.append(r)


class TextData:
    def __init__(self):
        self.txt = ''
        self.start = ''
        self.end = ''


class SttClient:
    conf = Config()
    real_stub = None
    resolver_stub = None

    def __init__(self):
        remote = '127.0.0.1:' + conf.get('brain-stt.sttd.front.port')
        print(remote)
        channel = grpc.insecure_channel(remote)
        self.resolver_stub = stt_pb2_grpc.SttModelResolverStub(channel)
        self.final_result = list()

    def get_servers(self, _name, _lang, _sample_rate):
        """Find & Connect servers"""
        # Define model
        model = stt_pb2.Model()
        if _lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = _name
        model.sample_rate = _sample_rate

        try:
            # Find Server
            self.server_status = self.resolver_stub.Find(model)
        except:
            print(_name + '-' + _lang + '-' + str(_sample_rate) + ' model cannot found')
            return False

        # Remote STT service
        # channel = grpc.insecure_channel(server_status.server_address)
        # self.real_stub = stt_pb2.SttRealServiceStub(channel)

        # Get STT server status
        wait = 0
        while not self.get_stt_status(model):
            print('Wait for server ready...')
            time.sleep(0.5)
            wait += 1
            if wait > 20:
                return False
            continue

        return True

    def get_stt_status(self, model):
        """Return STT server status"""
        try:
            # Remote STT service
            ip, port = self.server_status.server_address.split(':')
            channel = grpc.insecure_channel(self.server_status.server_address)
            #channel = grpc.insecure_channel('10.122.64.152:' + port)
            self.real_stub = stt_pb2_grpc.SttRealServiceStub(channel)

            status = self.real_stub.Ping(model)
            return status.running
            print('Model : ', status.model)
            print('Sample Rate : ', status.sample_rate)
            print('Lang : ', status.lang)
            print('Running : ', status.running)
            print('Server Address : ', status.server_address)
            print('Invoked by : ', status.invoked_by)
            return status.running
        except:
            return False

    def detail_recognize(self, audio_file):
        """Speech to Text function"""
        result = self.real_stub.DetailRecognize(self.bytes_from_file(audio_file))

        for seg in result.segments:
            print('%.2f ~ %.2f : %s' % (seg.start / 100.0, seg.end / 100.0, seg.txt))
        for fragment in result.fragments:
            print('%d, %d, %s, %.2f' % (fragment.start, fragment.end, fragment.txt, fragment.likelihood))
        print(result.raw_mlf.decode('euckr'))

    def stream_recognize(self, audio_file):
        """Speech to Text function"""
        #segments = self.real_stub.StreamRecognize(self.bytes_from_file(audio_file), timeout=3600)
        segments = self.real_stub.StreamRecognize(self.bytes_from_file(audio_file))

        try:
            for seg in segments:
                #diar_start = int(seg.start / 40)
                #diar_end = int(seg.end / 40)
                diar_start = seg.start
                diar_end = seg.end
                diar = DIAR[diar_start:diar_end+1]
                c = collections.Counter(diar)
                speaker_id, count = c.most_common(1)[0]
                #print('(%d, %d) %.2f ~ %.2f : %s' % (diar.count(0), diar.count(1), seg.start / 100.0, seg.end / 100.0, seg.txt))
                ###print '%.2f ~ %.2f : ' % (diar_start * 0.4, diar_end * 0.4), diar
                #print('(%d, %d%%) %.2f ~ %.2f : %s' % (speaker_id, float(count) / len(diar) * 100, seg.start / 100.0, seg.end / 100.0, seg.txt))
                print('spk %d, %.2f ~ %.2f : %s' % (speaker_id, seg.start / 100.0, seg.end / 100.0, seg.txt))
        except grpc.RpcError as e:
            print(('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details())))

    def find_idx(self, ori, frag):
        end = 0
        for ch in frag:
            idx = ori.find(ch)
            if idx > 0:
                end += idx + len(ch)
                ori = ori[end:]
        return end

    def find_speaker(self, info):
        diar_start = info[1]
        diar_end = info[2]
        diar = DIAR[diar_start:diar_end+1]
        if len(diar) <= 0:
            print 'list index out of range', diar_start, diar_end
            return -1, 0
        c = collections.Counter(diar)
        speaker_id, count = c.most_common(1)[0]
        #print speaker_id, diar_start, diar_end, '----------------', info[0], count, len(diar), float(count)/len(diar)
        return speaker_id, float(count)/len(diar)
 
    def make_word_list(self, seg):
        sentence_list = seg.txt.encode('utf8').split('.')
        sent_infos = []
        offset = 0              # fragment idx
        for sent in sentence_list:
            sent = sent.lstrip()
            word_infos = []
            word_list = sent.split()
            idx = 0
            word_start = -1
            word_end = 0
            for frag in seg.fragments:
                if not frag.txt.startswith('<'):
                    audio_start = seg.start - frag.start
                    break

            sent_start = -1
            sent_end = 0

            for word in word_list:
            #    print word, offset
                for frag in seg.fragments[offset:]:
                    frag_utf = frag.txt.encode('utf8').lstrip('#')
                    if frag_utf.startswith('<'):
                        offset += 1
                        continue
                    if word_start == -1 and word.startswith(frag_utf):
                        word_start = audio_start + frag.start
                    if word_end == 0 and word.endswith(frag_utf):
                        word_end = audio_start + frag.end
                        word_infos.append((word, word_start, word_end))
                        if sent_start == -1:
                            sent_start = word_start
                        if word_end > sent_end:
                            sent_end = word_end
                        offset += 1
                        break
                    offset += 1
                word_start = -1
                word_end = 0

            sent_info = (sent, sent_start, sent_end)
            sent_infos.append(sent_info)
            speaker, ratio = self.find_speaker(sent_info)
            if speaker == -1:
                continue
            if ratio >= 0.8:
                #print speaker, sent_start, sent_end, '----------------', sent, '%02d%%' % (ratio * 100)
                print '화자:%d, (%.02f초 ~ %.02f초) ---> %s (구간일치:%02d%%)' % (
                      speaker, sent_start/100.0, sent_end/100.0, sent, ratio * 100)
            else:
                prev_speaker = -1
                new_sent = ''
                new_sent_start = 0
                new_sent_end = 0
                for info in word_infos:
                    speaker, ratio = self.find_speaker(info)
                    if new_sent_start == 0:
                        new_sent_start = info[1]
                    if prev_speaker != -1 and prev_speaker != speaker:
                        print '화자:%d, (%.02f초 ~ %.02f초) ---> %s' % (
                              prev_speaker, new_sent_start/100.0, new_sent_end/100.0, new_sent)
                        #print prev_speaker, new_sent_start, new_sent_end, '----------------', new_sent
                        #print new_sent
                        new_sent = ''
                        new_sent_start = info[1]
                    new_sent += info[0] + ' '
                    new_sent_end = info[2]
                    prev_speaker = speaker
                if len(new_sent) > 0:
                    print '화자:%d, (%.02f초 ~ %.02f초) ---> %s' % (
                          prev_speaker, new_sent_start/100.0, new_sent_end/100.0, new_sent)
                    #print prev_speaker, new_sent_start, new_sent_end, '----------------', new_sent

            # for frag in seg.fragments:
            #     if frag.txt.startswith('<'):
            #         continue
            #     new_frag = frag.txt.encode('utf8').lstrip('#')
            #     print new_frag, idx
            #     offset = word_list[idx].find(new_frag)
            #     if offset < 0:
            #         word_infos.append((word_list[idx],word_start, word_end))
            #         word_start = -1
            #         word_end = 0
            #         idx += 1
            #     if word_start == -1:
            #         word_start = frag.start
            #     word_end = frag.end
            for word in word_infos:
                pass
                #print word[0], word[1], word[2]
                
    def stream_recognize3(self, audio_file):
        '''
        1. 문장(segment)정확도 측정
        2. segment 80? 90% 이하인 경우
        2.1. 단어별로 분리
        2.2. 단어별로 화자 설정
        '''
        #csv_idx = 0
        #global csv_result
        segments = self.real_stub.DetailStreamRecognize(self.bytes_from_file(audio_file))
        try:
            for seg in segments:
                self.make_word_list(seg)
                continue
                audio_start = 0
                for frag in seg.fragments:
                    if not frag.txt.startswith('<'):
                        audio_start = seg.start - frag.start
                        break
                print 'SEG START:%.2f, END:%.2f' % (seg.start / 100.0, seg.end / 100.0)
                #for frag in seg.fragments:
                #    print '%.2f,%.2f,%s' % ((seg.start + frag.start)/100.0, (seg.start + frag.end)/100.0, frag.txt.encode('utf8'))
                    #csv_result[csv_idx].extend([(seg.start + frag.start)/100.0, (seg.start + frag.end)/100.0, frag.txt.encode('utf8')])
                    #csv_idx += 1
                sent = ''
                prev_spk = -1
                ori_sentence = seg.txt.encode('utf8')
                #print 'Original : ' + ori_sentence
                start = 0
                end = 0
                sent_start = seg.start
                sent_end = 0
                for frag in seg.fragments:
                    txt = frag.txt.encode('utf8').lstrip('#')
                    if txt.startswith('<'):
                        print 'ignore non-word %s' % txt
                        continue
                    print '%.2f,%.2f,%s' % ((audio_start + frag.start)/100.0, (audio_start + frag.end)/100.0, frag.txt.encode('utf8'))
                    diar_start = audio_start + frag.start
                    diar_end = audio_start + frag.end
                    diar = DIAR[diar_start:diar_end+1]
                    c = collections.Counter(diar)
                    try:
                        speaker_id, count = c.most_common(1)[0]
                    except:
                        speaker_id = prev_spk
                    if prev_spk != -1 and prev_spk != speaker_id:
                        #print 'spk{}'.format(prev_spk), sent
                        if len(ori_sentence[start:end]) > 0:
                            edited = ori_sentence[start:end].replace('.', '').lstrip()
                            print 'Speaker {}: {}'.format(prev_spk, edited)
                            self.final_result.append('Speaker {} says ({:.2f}~{:.2f}): {}'.format(
                                prev_spk,
                                (sent_start) / 100.0,
                                (sent_end) / 100.0,
                                edited))
                        start = end
                        sent_start = diar_start
                        sent = ''
                    if not txt.startswith('<'):
                        #idx = ori_sentence[start:].find(txt)
                        idx = ori_sentence[end:].find(txt)
                        if idx >= 0:
                            end += idx + len(txt)
                        else:
                            print 'cannot find %d' % end, ori_sentence[end:] + ',' +  txt

                        #sent += txt + ' '
                    #print 'spk {} (count {}) start:{:5d}, end{:5d},\ttxt:{}'.format(speaker_id, count, seg.start + frag.start, seg.start + frag.end, frag.txt.encode('utf8'))
                    prev_spk = speaker_id
                    sent_end = diar_end
                #if len(sent) > 0 and not sent.startswith('<'):
                #if len(sent) > 0:
                #    print 'spk{}'.format(prev_spk), sent
                if end > start:
                    edited = ori_sentence[start:end].replace('.', '').lstrip()
                    print 'Speaker {}: {}'.format(prev_spk, edited)
                    self.final_result.append('Speaker {} says ({:.2f}~{:.2f}): {}'.format(
                        prev_spk,
                        (sent_start) / 100.0,
                        (sent_end) / 100.0,
                        edited))
        except grpc.RpcError as e:
            print(('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details())))

    def pcm_recognize(self, pcm_bytes):
        """Speech to Text function"""
        #segments = self.real_stub.StreamRecognize(self.bytes_from_pcm(pcm_bytes), timeout=3600)
        result = self.real_stub.SimpleRecognize(self.bytes_from_pcm(pcm_bytes), timeout=3600)
        print result.txt
        return result.txt

        try:
            for seg in segments:
                #print('(%d) %.2f ~ %.2f : %s' % (len(pcm_bytes) / 6400, seg.start / 100.0, seg.end / 100.0, seg.txt))
                print seg.txt
        except grpc.RpcError as e:
            print(('StreamRecognize() failed with {0}: {1}'.format(e.code(), e.details())))


    def bytes_from_pcm(self, pcm_bytes):
        speech = stt_pb2.Speech()
        speech.bin = pcm_bytes
        yield speech

    #def bytes_from_file(self, filename, chunksize=10000):
    def bytes_from_file(self, filename, chunksize=160):
        with open(filename, "rb") as f:
            if filename.endswith('.wav'):
                # ignore wav header
                f.read(44)
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    speech = stt_pb2.Speech()
                    speech.bin = chunk
                    # for timeout test, uncomment below line...
                    # time.sleep(3)
                    yield speech
                else:
                    break

    def bytes_from_file2(self, filename, chunksize=1024*1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    part = types_pb2.FilePart()
                    part.part = chunk
                    yield part
                else:
                    break

    def set_model(self, _filename):
        metadata={(b'in.lang', b'kor'), (b'in.model', 'weather'), (b'in.samplerate', '8000') }
        result = self.resolver_stub.SetModel(self.bytes_from_file2(_filename), metadata=metadata)
        print('RESULT', result.lang)
        print('RESULT', result.model)
        print('RESULT', result.sample_rate)
        print('RESULT', result.result)
        print('RESULT', result.error)

    def delete_model(self, name, lang, sample_rate):
        model = stt_pb2.Model()
        if lang == 'eng':
            model.lang = lang_pb2.eng
        else:
            model.lang = lang_pb2.kor

        model.model = name
        model.sample_rate = sample_rate
        status = self.resolver_stub.DeleteModel(model)
        print('Model : ', status.model)
        print('Sample Rate : ', status.sample_rate)
        print('Lang : ', status.lang)
        print('Running : ', status.running)
        print('Server Address : ', status.server_address)
        print('Invoked by : ', status.invoked_by)
        return status.running


def test_recognize2(stt_client, sample_rate, model, filename, service):
    chunksize = 160 * 40
    index = 0
    prev_spk_id = None
    pcm_bytes = ''
    outf = open(filename[:-4] + '.txt', 'wb')
    with open(filename, "rb") as f:
        while True:
            chunk = f.read(chunksize)
            if len(chunk) < chunksize or not index < len(DIAR):
                break
            spk_id = DIAR[index]
            if prev_spk_id != None and prev_spk_id != spk_id:
                print 'Speaker %d said:' % prev_spk_id
                outf.write('Speaker %d said:' % prev_spk_id)
                stt_client.get_servers(_name=model, _lang='kor', _sample_rate=sample_rate)
                result = stt_client.pcm_recognize(pcm_bytes)
                outf.write(result.encode('utf8'))
                outf.write('\n')
                pcm_bytes = ''
            else:
                pcm_bytes += chunk
            prev_spk_id = spk_id
            index += 1
    outf.close()


def test_recognize(stt_client, sample_rate, model, filename, service):
    if stt_client.get_servers(_name=model, _lang='kor', _sample_rate=sample_rate):
        if service == 'simple':
            stt_client.simple_recognize(filename)
        elif service == 'detail':
            #stt_client.detail_recognize(filename)
            stt_client.stream_recognize3(filename)
            for line in stt_client.final_result:
                print line
        elif service == 'stream':
            stt_client.stream_recognize(filename)
    else:
        print('STT Service unavailable')


def test_set_model(_stt_client, f):
    _stt_client.set_model(f)


def test_delete_model(_stt_client):
    _stt_client.delete_model(name='weather', lang='kor', sample_rate=8000)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='STT diarization test')
    parser.add_argument('-m', nargs='?', const='baseline', type=str,
                        metavar='model',
                        help='stt model name')
    parser.add_argument('--lang', default='kor', type=str,
                        metavar='language',
                        help='stt model language')
    parser.add_argument('-r', default=8000, type=int,
                        metavar='samplerate',
                        help='stt model samplerate')
    parser.add_argument('-s', default='detail', type=str,
                        metavar='grpc_service',
                        help='grpc service name')
    parser.add_argument('filename', type=str,
                        help='file name')
    args = parser.parse_args()

    conf = Config()
    conf.init('brain-stt.conf')

    model = args.m
    lang = args.lang
    sample_rate = args.r
    filename = args.filename

    stt_client = SttClient()
    do_diarization(filename)
    # test_recognize(stt_client, sample_rate, model, filename, service)
    # test_recognize2(stt_client, sample_rate, model, filename, service)
    test_recognize(stt_client, sample_rate, model, filename, args.s)

